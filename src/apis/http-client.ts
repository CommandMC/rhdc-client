import { FileLike, Nullable, Weak } from '../util/types';
import { getAuthToken } from '../auth/auth-token';
import { API_HOST } from '../config';
import { RHDC_DEV_KEY } from '../auth/dev-key';

const g_responseHandlers: ((_:XMLHttpRequest) => void)[] = [];

const ContentType = Object.freeze({
	Text: 'text/plain',
	Json: 'application/json',
	Form: 'multipart/form-data',
	Error: 'application/problem+json'
});

type HttpMethod = 'GET' | 'PUT' | 'PATCH' | 'POST' | 'DELETE' | 'HEAD';

export enum HttpErrorType {
	Problem,
	Basic,
	Unknown
}

export interface ProblemResult {
	type: string;
	title: string;
	status: number;
	detail?: string;
}

export interface StatusResponse {
	code: number;
	text: string;
}

export class UploadProgress {

	public readonly bytesUploaded: number;
	public readonly totalBytes: number;

	constructor( uploaded: number, total: number ) {
		this.bytesUploaded = uploaded;
		this.totalBytes = total;
	}

	public get value() : number {
		return this.bytesUploaded / this.totalBytes;
	}

	public get percentage() : number {
		return Math.floor( 100 * this.value );
	}

}

export class HttpError {

	#type: HttpErrorType;
	#error: ProblemResult | StatusResponse | Error;
	#xhr: Weak<XMLHttpRequest>;

	constructor(
		type: HttpErrorType,
		error: ProblemResult | StatusResponse | Error,
		xhr: Weak<XMLHttpRequest> = null
	) {
		this.#type = type;
		this.#error = error;
		this.#xhr = xhr;
	}

	public get type() : HttpErrorType {
		return this.#type;
	}

	public get problem() : ProblemResult {
		return this.#error as ProblemResult;
	}

	public get status() : StatusResponse {
		return this.#error as StatusResponse;
	}

	public get exception() : Error {
		return this.#error as Error;
	}

	public tryGetHeader( headerName: string ) : Nullable<string> {
		return this.#xhr ? this.#xhr.getResponseHeader( headerName ) : null;
	}

}

const _sendAsync = function(
	request: XMLHttpRequest,
	data: Nullable<XMLHttpRequestBodyInit> = null,
	dataType: Nullable<string> = null
) : Promise<XMLHttpRequest> {
	return new Promise( (resolve, reject) => {
		const authToken = getAuthToken();
		if( authToken ) request.setRequestHeader( 'Authorization', `Bearer ${authToken}` );
		if( dataType ) request.setRequestHeader( 'Content-Type', dataType );
		if( RHDC_DEV_KEY ) request.setRequestHeader( 'X-Dev-Key', RHDC_DEV_KEY );
		request.setRequestHeader( 'X-Source-Page', window.location.pathname );

		request.onreadystatechange = function () {
			if( request.readyState !== XMLHttpRequest.DONE ) return;
			g_responseHandlers.forEach( handler => handler( request ) );
			resolve( request );
		};

		request.onerror = function( exception: ProgressEvent ) {
			console.error( exception );
			reject( new HttpError( HttpErrorType.Unknown, new Error(), request ) );
		}

		request.responseType = 'blob';
		request.send( data );
	});
};

const _makeRequest = function(
	method: HttpMethod,
	route: string
) : XMLHttpRequest {
	const request = new XMLHttpRequest();
	request.open( method, API_HOST + route );
	return request;
};

const _errorFromException = function( exception: unknown ) : HttpError {
	if( exception instanceof Error ) {
		return new HttpError( HttpErrorType.Unknown, exception );
	} else if( typeof exception === 'string' ) {
		return new HttpError( HttpErrorType.Unknown, new Error( exception ) );
	} else {
		console.error( exception );
		return new HttpError( HttpErrorType.Unknown, new Error() );
	}
}

const _requireSuccessAsync = async function( response: XMLHttpRequest ) : Promise<void> {
	if( response.status >= 200 && response.status < 400 ) return;

	if( response.getResponseHeader( 'Content-Type' ) === ContentType.Error ) {
		let problem: ProblemResult;
		try {
			problem = JSON.parse( await (response.response as Blob).text() ) as ProblemResult;
		} catch( exception: unknown ) {
			throw _errorFromException( exception );
		}
		
		throw new HttpError( HttpErrorType.Problem, problem, response );
	}

	throw new HttpError( HttpErrorType.Basic, { code: response.status, text: response.statusText }, response );
};

const _readResponseAsync = async function( response: XMLHttpRequest ) : Promise<unknown> {
	try {
		const contentType = response.getResponseHeader( 'Content-Type' );
		if( !contentType ) return null;

		const responseBody = response.response as Blob;
		if( contentType.startsWith( ContentType.Text ) ) {
			return await responseBody.text();
		} else if( contentType.startsWith( ContentType.Json ) || contentType.startsWith( ContentType.Error ) ) {
			return JSON.parse( await responseBody.text() );
		} else {
			return responseBody;
		}
	} catch( exception: unknown ) {
		throw _errorFromException( exception );
	}
};

const _getMimeType = function( file: FileLike ) : string {
	const extension = file.name.substring( file.name.lastIndexOf( '.' ) );
	switch( extension ) {
		case '.bps': return 'application/x-bps-patch';
		case '.zip': return 'application/zip';
		case '.json': case '.jsml': return 'application/json';
		case '.png': return 'image/png';
		case '.bmp': return 'image/bmp';
		case '.gif': return 'image/gif';
		case '.svg': return 'image/svg+xml';
		case '.jpg': case '.jpeg': return 'image/jpeg';
		case '.pbm': return 'image/x-portable-bitmap';
		case '.pgm': return 'image/x-portable-graymap';
		case '.ppm': return 'image/x-portable-pixmap';
		case '.xbm': return 'image/x-xbitmap';
		case '.xpm': return 'image/x-xpixmap';
		default: return file.type || 'application/octet-stream';
	}
}

const _sendEmptyAsync = async function<T>( method: HttpMethod, route: string ) : Promise<T> {
	const xhr = _makeRequest( method, route );
	await _sendAsync( xhr );
	await _requireSuccessAsync( xhr );
	return await _readResponseAsync( xhr ) as T;
};

const _sendJsonAsync = async function<T>( method: HttpMethod, route: string, data: unknown ) : Promise<T> {
	const xhr = _makeRequest( method, route );
	await _sendAsync( xhr, JSON.stringify( data ), ContentType.Json );
	await _requireSuccessAsync( xhr );
	return await _readResponseAsync( xhr ) as T;
};

const _sendTextAsync = async function<T>( method: HttpMethod, route: string, data: string ) : Promise<T> {
	const xhr = _makeRequest( method, route );
	await _sendAsync( xhr, data, ContentType.Text );
	await _requireSuccessAsync( xhr );
	return await _readResponseAsync( xhr ) as T;
};

const _sendFileAsync = async function<T>( method: HttpMethod, route: string, file: FileLike, callback: Weak<(_:UploadProgress) => unknown> ) : Promise<T> {
	const xhr = _makeRequest( method, route );
	xhr.setRequestHeader( 'X-Filename', file.name );
	if( callback ) {
		xhr.upload.onloadstart = () => {
			callback( new UploadProgress( 0, file.size ) );
		};
		xhr.upload.onprogress = (event: ProgressEvent) => {
			callback( new UploadProgress( event.loaded, event.total ) );
		};
		xhr.upload.onload = () => {
			callback( new UploadProgress( file.size, file.size ) );
		};
	}
	await _sendAsync( xhr, file, _getMimeType( file ) );
	await _requireSuccessAsync( xhr );
	return await _readResponseAsync( xhr ) as T;
};

const _sendFormDataAsync = async function<T>( method: HttpMethod, route: string, data: FormData ) : Promise<T> {
	const xhr = _makeRequest( method, route );
	await _sendAsync( xhr, data, ContentType.Form );
	await _requireSuccessAsync( xhr );
	return await _readResponseAsync( xhr ) as T;
};

export class HttpClient {

	public static async getAsync<T>( route: string ) : Promise<T> {
		return _sendEmptyAsync<T>( 'GET', route );
	}

	public static async headAsync( route: string ) : Promise<void> {
		return _sendEmptyAsync<void>( 'HEAD', route );
	}

	public static async postAsync<T>( route: string ) : Promise<T> {
		return _sendEmptyAsync<T>( 'POST', route );
	}

	public static async postTextAsync<T>( route: string, data: string ) : Promise<T> {
		return _sendTextAsync<T>( 'POST', route, data );
	}

	public static async postJsonAsync<T>( route: string, data: unknown ) : Promise<T> {
		return _sendJsonAsync<T>( 'POST', route, data );
	}

	public static async postFileAsync<T>( route: string, file: FileLike, uploadProgressCallback: Weak<(_:UploadProgress) => unknown> = undefined ) : Promise<T> {
		return _sendFileAsync<T>( 'POST', route, file, uploadProgressCallback );
	}

	public static async postFormAsync<T>( route: string, data: FormData ) : Promise<T> {
		return _sendFormDataAsync<T>( 'POST', route, data );
	}

	public static async putTextAsync<T>( route: string, data: string ) : Promise<T> {
		return _sendTextAsync<T>( 'PUT', route, data );
	}

	public static async putJsonAsync<T>( route: string, data: unknown ) : Promise<T> {
		return _sendJsonAsync<T>( 'PUT', route, data );
	}

	public static async putFileAsync<T>( route: string, file: FileLike, uploadProgressCallback: Weak<(_:UploadProgress) => unknown> = undefined ) : Promise<T> {
		return _sendFileAsync<T>( 'PUT', route, file, uploadProgressCallback );
	}

	public static async patchJsonAsync<T>( route: string, data: unknown ) : Promise<T> {
		return _sendJsonAsync<T>( 'PATCH', route, data );
	}

	public static async deleteAsync<T>( route: string ) : Promise<T> {
		return _sendEmptyAsync<T>( 'DELETE', route );
	}

	public static registerGlobalResponseHandler( handler: (_:XMLHttpRequest) => void ) : void {
		g_responseHandlers.push( handler );
	}

}
