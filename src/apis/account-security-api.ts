import { HttpClient } from './http-client';
import { Nullable, Weak, Uuid, ConstArray } from '../util/types';
import { UserAuth, setUserContext } from '../decorators/user-context';

export interface SecurityCredentials {
	password: string;
	mfaCode?: Weak<number>;
}

export enum KnownBrowser {
	Firefox = 'Firefox',
	Seamonkey = 'Seamonkey',
	Chromium = 'Chromium',
	Chrome = 'Chrome',
	OperaGX = 'Opera GX',
	Opera = 'Opera',
	Vivaldi = 'Vivaldi',
	Edge = 'Edge',
	Safari = 'Safari',
	ParallelLauncher = 'Parallel Launcher'
}

interface ActiveLoginData {
	sessionId: Uuid;
	application: 'Browser' | 'ParallelLauncher';
	loginDate: string;
	refreshDate: string;
	expiryDate: string;
	country: Nullable<string>;
	city: Nullable<string>;
	browser: Nullable<KnownBrowser>;
	browserVersion: Nullable<string>;
	operatingSystem: 'Linux' | 'Windows' | 'MacOS' | null;
	mobile: boolean;
	actions: string[];
}

export class ActiveLogin {

	public readonly sessionId: Uuid;
	public readonly application: 'Browser' | 'ParallelLauncher';
	public readonly loginDate: Date;
	public readonly refreshData: Date;
	public readonly expiryDate: Date;
	public readonly country: Nullable<string>;
	public readonly city: Nullable<string>;
	public readonly browser: Nullable<KnownBrowser>;
	public readonly browserVersion: Nullable<string>;
	public readonly operatingSystem: 'Linux' | 'Windows' | 'MacOS' | null;
	public readonly mobile: boolean;
	readonly #actions: string[];

	public constructor( data: ActiveLoginData ) {
		this.sessionId = data.sessionId;
		this.application = data.application;
		this.loginDate = new Date( data.loginDate );
		this.refreshData = new Date( data.refreshDate );
		this.expiryDate = new Date( data.expiryDate );
		this.country = data.country;
		this.city = data.city;
		this.browser = data.browser;
		this.browserVersion = data.browserVersion;
		this.operatingSystem = data.operatingSystem;
		this.mobile = data.mobile;
		this.#actions = data.actions;
		Object.freeze( this );
	}

	public get steamDeck() : boolean {
		return this.mobile && this.application === 'ParallelLauncher' && this.browser === KnownBrowser.ParallelLauncher && this.operatingSystem === 'Linux';
	}

	public get canLogout() : boolean {
		return this.#actions.includes( 'logout' );
	}

}

export class AccountSecurityApi {

	static async requestPageAccessAsync( credentials: SecurityCredentials ) : Promise<Uuid> {
		const ast = await HttpClient.postJsonAsync<{ token: Uuid }>( '/v3/security/begin', credentials );
		return ast.token;
	}

	static revokePageAccessAsync( accountSecurityToken: Uuid ) : Promise<void> {
		return HttpClient.postAsync<void>( `/v3/security/finish?ast=${accountSecurityToken}` );
	}

	static changePasswordAsync( accountSecurityToken: Uuid, newPassword: string ) : Promise<void> {
		return HttpClient.putJsonAsync<void>( `/v3/security/password?ast=${accountSecurityToken}`, { password: newPassword } );
	}

	static async generateMfaQrCodeAsync( accountSecurityToken: Uuid ) : Promise<string> {
		const token = await HttpClient.postAsync<{ secret: string }>( `/v3/security/mfa/request?ast=${accountSecurityToken}` );
		return token.secret;
	}

	static async enableMfaAsync( accountSecurityToken: Uuid, mfaCode: number ) : Promise<UserAuth> {
		const userAuth = await HttpClient.postAsync<UserAuth>( `/v3/security/mfa/enable?ast=${accountSecurityToken}&code=${mfaCode}` );
		setUserContext( Object.freeze( userAuth ) );
		return userAuth;
	}

	static async disableMfaAsync( accountSecurityToken: Uuid ) : Promise<UserAuth> {
		const userAuth = await HttpClient.postAsync<UserAuth>( `/v3/security/mfa/disable?ast=${accountSecurityToken}` );
		setUserContext( Object.freeze( userAuth ) );
		return userAuth;
	}

	static async getActiveLoginsAsync( accountSecurityToken: Uuid ) : Promise<ConstArray<ActiveLogin>> {
		const sessions = await HttpClient.getAsync<ActiveLoginData[]>( `/v3/security/logins?ast=${accountSecurityToken}` );
		return Object.freeze( sessions.map( data => new ActiveLogin( data ) ) );
	}

	static terminateSessionAsync( accountSecurityToken: Uuid, sessionId: Uuid ) : Promise<void> {
		return HttpClient.deleteAsync( `/v3/security/login/${sessionId}?ast=${accountSecurityToken}` );
	}

}
