import { RichUserRef } from "./users-api";
import { HttpClient } from './http-client';
import { Page } from './pagination';

interface LeaderboardEntryData {
	user: RichUserRef;
	starPoints: number;
	kaizoStarPoints: number;
}

export class LeaderboardEntry {

	public readonly user: Readonly<RichUserRef>;
	public readonly starPoints: number;
	public readonly kaizoStarPoints: number;

	constructor( data: LeaderboardEntryData ) {
		this.user = Object.freeze( data.user );
		this.starPoints = data.starPoints;
		this.kaizoStarPoints = data.kaizoStarPoints;
		Object.freeze( this );
	}

}

export class LeaderboardApi {

	static getMainLeaderboardAsync( pageSize = 30 ) : Promise<Page<LeaderboardEntry>> {
		return Page.fetchFirstAsync(
			`/v3/leaderboard/main?pageSize=${pageSize}`,
			(data: LeaderboardEntryData) => new LeaderboardEntry( data )
		);
	}

	static getKaizoLeaderboardAsync( pageSize = 30 ) : Promise<Page<LeaderboardEntry>> {
		return Page.fetchFirstAsync(
			`/v3/leaderboard/kaizo?pageSize=${pageSize}`,
			(data: LeaderboardEntryData) => new LeaderboardEntry( data )
		);
	}

	static getMainRankAsync() : Promise<number> {
		return HttpClient.getAsync<number>( '/v3/leaderboard/main/rank' );
	}

	static getKaizoRankAsync() : Promise<number> {
		return HttpClient.getAsync<number>( '/v3/leaderboard/kaizo/rank' );
	}

}
