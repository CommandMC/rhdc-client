import { Uuid, Nullable, Weak } from '../util/types';
import { HttpClient } from './http-client';
import { Page } from './pagination';
import { RichUserRef } from './users-api';

export class NewsPost {

	public readonly postId: Uuid;
	public readonly author: Readonly<RichUserRef>;
	public readonly datePosted: Readonly<Date>;
	public readonly dateEdited: Nullable<Readonly<Date>>;
	public readonly title: string;
	public readonly message: string;
	public readonly pollId: Nullable<Uuid>;
	readonly #actions: string[];

	constructor( data: NewsPostData ) {
		this.postId = data.postId;
		this.author = Object.freeze( data.author );
		this.datePosted = Object.freeze( new Date( data.datePosted ) );
		this.dateEdited = data.dateEdited ? Object.freeze( new Date( data.dateEdited ) ) : null;
		this.title = data.title;
		this.message = data.message;
		this.pollId = data.pollId;
		this.#actions = data.actions;
		Object.freeze( this );
	}

	public get canEdit() : boolean {
		return this.#actions.includes( 'edit' );
	}

}

interface NewsPostData {
	postId: Uuid;
	author: RichUserRef;
	datePosted: string;
	dateEdited: Nullable<string>;
	title: string;
	message: string;
	pollId: Nullable<Uuid>;
	actions: string[];
}

export class NewsApi {

	static getNewsPostsPagedAsync() : Promise<Page<NewsPost>> {
		return Page.fetchFirstAsync( '/v3/news', (x: NewsPostData) => new NewsPost( x ) );
	}

	static async createNewsPostAsync( title: string, content: string, pollId: Weak<Uuid> = undefined ) : Promise<NewsPost> {
		return new NewsPost( await HttpClient.postJsonAsync<NewsPostData>( '/v3/news', {
			title: title,
			message: content,
			pollId: pollId
		}));
	}

	static async updateNewsPostAsync( postId: Uuid, title: string, content: string ) : Promise<NewsPost> {
		return new NewsPost( await HttpClient.patchJsonAsync<NewsPostData>( `/v3/news/${postId}`, {
			title: title,
			message: content
		}));
	}

	static async removePollAsync( postId: Uuid ) : Promise<NewsPost> {
		return new NewsPost( await HttpClient.patchJsonAsync<NewsPostData>( `/v3/news/${postId}`, { pollId: null } ) );
	}

	static deleteNewsPostAsync( postId: Uuid ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/news/${postId}` );
	}

}
