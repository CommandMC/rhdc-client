import { HttpClient } from "./http-client";
import { Nullable } from "../util/types";

interface DownloadTokenData {
	url: string;
	mimeType: string;
	expiresAt: Nullable<string>;
}

export class DownloadToken {

	public readonly href: string;
	public readonly mimeType: string;
	public readonly expiresAt: Nullable<Date>;

	public get isExpired() : boolean {
		return !!this.expiresAt && new Date() >= this.expiresAt;
	}

	constructor( data: DownloadTokenData ) {
		this.href = data.url;
		this.mimeType = data.mimeType;
		this.expiresAt = data.expiresAt ? new Date( data.expiresAt ) : null;
	}

}

export interface DownloadInfo {

	readonly fileName: string;
	readonly mimeType: string;
	readonly directHref: string;
	readonly webHref: string;
	readonly secured: boolean;

}

export class FilesApi {

	static async getDownloadTokenAsync( url: string ) : Promise<DownloadToken> {
		return new DownloadToken( await HttpClient.getAsync<DownloadTokenData>( url ) );
	}

}
