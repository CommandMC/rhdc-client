import { ConstArray, HexId, Nullable, Optional } from '../util/types';
import { DownloadInfo } from './files';
import { HttpClient } from './http-client';
import { Page } from './pagination';
import { RichUserRef, WeakRichUserRef } from './users-api';

const DAYS = 1000 * 60 * 60 * 24;

interface QueueStatusData {
	count: number;
	oldest?: Optional<string>;
}

interface ModqueueStatusData {
	hacks: QueueStatusData;
	versions: QueueStatusData;
	claims: QueueStatusData;
	starpower: QueueStatusData;
}

interface PendingHackData {
	hackId: HexId;
	title: string;
	slug: string;
	thumbnail: Nullable<DownloadInfo>;
	uploader: Nullable<RichUserRef>;
	authors: WeakRichUserRef[];
	uploadedDate: string;
	lastVersionUploadTime: string;
	pendingVersions: number;
}

interface PendingClaimData {
	hackId: HexId;
	hackTitle: string;
	hackSlug: string;
	hackThumbnail: Nullable<DownloadInfo>;
	claimant: RichUserRef;
	targetAuthor: Nullable<string>;
	explanation: Nullable<string>;
	dateSubmitted: string;
}

interface PendingStarpowerData {
	hackId: HexId;
	hackTitle: string;
	hackSlug: string;
	user: RichUserRef;
	starPoints: number;
	claimedStarPoints: number;
	hackComplete: boolean;
	claimedHackComplete: boolean;
	proofLink: string;
	claimedStarCountLastUpdated: Nullable<string>;
}

export class PendingHack {

	public readonly hackId: HexId;
	public readonly title: string;
	public readonly slug: string;
	public readonly thumbnail: Nullable<DownloadInfo>;
	public readonly uploader: Nullable<RichUserRef>;
	public readonly authors: ConstArray<WeakRichUserRef>;
	public readonly uploadedDate: Readonly<Date>;
	public readonly lastVersionUploadTime: Readonly<Date>;
	public readonly pendingVersions: number;

	constructor( data: PendingHackData ) {
		this.hackId = data.hackId;
		this.title = data.title;
		this.slug = data.slug;
		this.thumbnail = Object.freeze( data.thumbnail );
		this.uploader = Object.freeze( data.uploader );
		this.authors = Object.freeze( data.authors.map( Object.freeze ) as Readonly<WeakRichUserRef>[] );
		this.uploadedDate = Object.freeze( new Date( data.uploadedDate ) );
		this.lastVersionUploadTime = Object.freeze( new Date( data.lastVersionUploadTime ) );
		this.pendingVersions = data.pendingVersions;
		Object.freeze( this );
	}

}

export class PendingClaim {

	public readonly hackId: HexId;
	public readonly hackTitle: string;
	public readonly hackSlug: string;
	public readonly hackThumbnail: Nullable<DownloadInfo>;
	public readonly claimant: RichUserRef;
	public readonly targetAuthor: Nullable<string>;
	public readonly explanation: Nullable<string>;
	public readonly dateSubmitted: Readonly<Date>;

	constructor( data: PendingClaimData ) {
		this.hackId = data.hackId;
		this.hackTitle = data.hackTitle;
		this.hackSlug = data.hackSlug;
		this.hackThumbnail = Object.freeze( data.hackThumbnail );
		this.claimant = Object.freeze( data.claimant );
		this.targetAuthor = data.targetAuthor;
		this.explanation = data.explanation;
		this.dateSubmitted = Object.freeze( new Date( data.dateSubmitted ) );
		Object.freeze( this );
	}

}

export class PendingStarpower {

	public readonly hackId: HexId;
	public readonly hackTitle: string;
	public readonly hackSlug: string;
	public readonly user: RichUserRef;
	public readonly starPoints: number;
	public readonly claimedStarPoints: number;
	public readonly hackComplete: boolean;
	public readonly claimedHackComplete: boolean;
	public readonly proofLink: string;
	public readonly claimedStarCountLastUpdated: Nullable<Readonly<Date>>;

	constructor( data: PendingStarpowerData ) {
		this.hackId = data.hackId;
		this.hackTitle = data.hackTitle;
		this.hackSlug = data.hackSlug;
		this.user = Object.freeze( data.user );
		this.starPoints = data.starPoints;
		this.claimedStarPoints = data.claimedStarPoints;
		this.hackComplete = data.hackComplete;
		this.claimedHackComplete = data.claimedHackComplete;
		this.proofLink = data.proofLink;
		this.claimedStarCountLastUpdated = data.claimedStarCountLastUpdated ? Object.freeze( new Date( data.claimedStarCountLastUpdated ) ) : null;
		Object.freeze( this );
	}

}

export class ModqueueComponentStatus {

	public readonly count: number;
	public readonly oldest: Nullable<Readonly<Date>>;

	constructor( data: QueueStatusData ) {
		this.count = data.count;
		this.oldest = data.oldest ? Object.freeze( new Date( data.oldest ) ) : null;
		Object.freeze( this );
	}

	public get priority() : 'success' | 'primary' | 'warning' | 'danger' {
		if( this.count === 0 ) return 'success';
		if( !this.oldest ) return 'primary';
		const days = (Date.now() - this.oldest.getTime()) / DAYS;
		if( days < 3 ) {
			return 'primary';
		} else if( days < 7 ) {
			return 'warning';
		} else {
			return 'danger';
		}
	}

	public get important() : boolean {
		return !!this.oldest && (Date.now() - this.oldest.getTime() >= 14 * DAYS);
	}

}

export class ModqueueStatus {

	public readonly hacks: Readonly<ModqueueComponentStatus>;
	public readonly versions: Readonly<ModqueueComponentStatus>;
	public readonly claims: Readonly<ModqueueComponentStatus>;
	public readonly starpower: Readonly<ModqueueComponentStatus>;

	constructor( data: ModqueueStatusData ) {
		this.hacks = new ModqueueComponentStatus( data.hacks );
		this.versions = new ModqueueComponentStatus( data.versions );
		this.claims = new ModqueueComponentStatus( data.claims );
		this.starpower = new ModqueueComponentStatus( data.starpower );
		Object.freeze( this );
	}

	public get overall() : Readonly<ModqueueComponentStatus> {
		let count = 0;
		let oldest: Optional<Readonly<Date>>;

		for( const component of [ this.hacks, this.versions, this.claims, this.starpower ] ) {
			count += component.count;
			if( component.oldest && (!oldest || component.oldest < oldest ) ) {
				oldest = component.oldest;
			}
		}

		return new ModqueueComponentStatus({
			count: count,
			oldest: oldest?.toISOString()
		});
	}

}

export class ModqueueApi {

	static async getQueueStatusAsync() : Promise<ModqueueStatus> {
		return new ModqueueStatus( await HttpClient.getAsync<ModqueueStatusData>( '/v3/modqueue/status' ) );
	}

	static getMissingCompletionBonusesCountAsync() : Promise<number> {
		return HttpClient.getAsync<number>( '/v3/modqueue/bonuses/count' );
	}

	static getPendingHacksAsync() : Promise<Page<PendingHack>> {
		return Page.fetchFirstAsync<PendingHack,PendingHackData>(
			'/v3/modqueue/hacks',
			data => new PendingHack( data )
		);
	}

	static getPendingVersionsAsync() : Promise<Page<PendingHack>> {
		return Page.fetchFirstAsync<PendingHack,PendingHackData>(
			'/v3/modqueue/versions',
			data => new PendingHack( data )
		);
	}

	static getPendingClaimsAsync() : Promise<Page<PendingClaim>> {
		return Page.fetchFirstAsync<PendingClaim, PendingClaimData>(
			'/v3/modqueue/claims',
			data => new PendingClaim( data )
		);
	}

	static getRejectedClaimsAsync() : Promise<Page<PendingClaim>> {
		return Page.fetchFirstAsync<PendingClaim, PendingClaimData>(
			'/v3/modqueue/claims/rejected',
			data => new PendingClaim( data )
		);
	}

	static getPendingStarpowerAsync() : Promise<Page<PendingStarpower>> {
		return Page.fetchFirstAsync<PendingStarpower, PendingStarpowerData>(
			'/v3/modqueue/starpower',
			data => new PendingStarpower( data )
		);
	}

	static getPendingCompletionBonusesAsync() : Promise<Page<PendingHack>> {
		return Page.fetchFirstAsync<PendingHack,PendingHackData>(
			'/v3/modqueue/bonuses',
			data => new PendingHack( data )
		);
	}

	static getPendingCompletionBonusCountAsync() : Promise<number> {
		return HttpClient.getAsync<number>( '/v3/modqueue/bonuses/count' );
	}

}
