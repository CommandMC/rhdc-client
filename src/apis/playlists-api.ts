import { HttpClient } from './http-client';
import { ConstArray, HexId, UserId, Optional } from '../util/types';
import { HackData, Hack, HackSortField } from './hacks-api';
import { Page } from './pagination';

interface PlaylistDto {
	name: string;
	public: boolean;
	hackCount: number;
	actions: string[];
}

interface PlaylistCheckDto {
	name: string;
	hasHack: boolean;
}

export class Playlist {

	public readonly name: string;
	public readonly public: boolean;
	public readonly hackCount: number;
	readonly #actions: string[];

	constructor( data: PlaylistDto ) {
		this.name = data.name;
		this.public = data.public;
		this.hackCount = data.hackCount;
		this.#actions = data.actions;
		Object.freeze( this );
	}

	public get canEdit() : boolean {
		return this.#actions.includes( 'edit' );
	}

}

export class PlaylistCheck {

	public readonly name: string;
	public readonly hasHack: boolean;

	constructor( data: PlaylistCheckDto ) {
		this.name = data.name;
		this.hasHack = data.hasHack;
		Object.freeze( this );
	}

}

export interface PlaylistUpdate {
	name?: Optional<string>;
	public?: Optional<boolean>;
}

export class PlaylistsApi {

	static async getPlaylistsAndCheckHackAsync( hackId: HexId ) : Promise<ConstArray<PlaylistCheck>> {
		const playlists = await HttpClient.getAsync<PlaylistCheckDto[]>( `/v3/playlists/hack/${hackId}` );
		return Object.freeze( playlists.map( pl => new PlaylistCheck( pl ) ) );
	}

	static async getUserPlaylistsAsync( userSlug: UserId | string ) : Promise<ConstArray<Playlist>> {
		const playlists = await HttpClient.getAsync<PlaylistDto[]>( `/v3/playlists/user/${userSlug}` );
		return Object.freeze( playlists.map( pl => new Playlist( pl ) ) );
	}

	static async movePlaylistAsync( userSlug: string | UserId, sourceIndex: number, moveBefore: number ) : Promise<ConstArray<Playlist>> {
		const playlists = await HttpClient.postAsync<PlaylistDto[]>( `/v3/playlists/user/${userSlug}/move/${sourceIndex}/${moveBefore}` );
		return Object.freeze( playlists.map( pl => new Playlist( pl ) ) );
	}

	static getPlaylistHacksAsync(
		userSlug: UserId | string,
		playlist: string,
		sortBy: HackSortField,
		descending: boolean,
		pageSize = 10
	) : Promise<Page<Hack>> {
		return Page.fetchFirstAsync(
			`/v3/playlists/user/${userSlug}/name/${encodeURIComponent( playlist )}/hacks?sortBy=${sortBy}&sortOrder=${descending ? 'desc' : 'asc'}&pageSize=${pageSize}`,
			(data: HackData) => new Hack( data )
		);
	}

	static async addHackToPlaylistAsync( playlist: string, hackSlug: HexId | string ) : Promise<void> {
		await HttpClient.postAsync<unknown>( `/v3/playlists/name/${encodeURIComponent( playlist )}/hack/${hackSlug}` );
	}

	static removeHackFromPlaylistAsync( playlist: string, hackSlug: HexId | string ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/playlists/name/${encodeURIComponent( playlist )}/hack/${hackSlug}` );
	}

	static async createPlaylistAsync( playlist: string, isPublic = true ) : Promise<Playlist> {
		return new Playlist( await HttpClient.postAsync<PlaylistDto>( `/v3/playlists/name/${encodeURIComponent( playlist )}?isPrivate=${isPublic ? 'false' : 'true'}` ) );
	}

	static async updatePlaylistAsync( playlist: string, update: PlaylistUpdate ) : Promise<Playlist> {
		return new Playlist( await HttpClient.patchJsonAsync<PlaylistDto>( `/v3/playlists/name/${encodeURIComponent( playlist )}`, update ) );
	}

	static deletePlaylistAsync( playlist: string ) : Promise<void> {
		return HttpClient.deleteAsync<void>( `/v3/playlists/name/${encodeURIComponent( playlist )}` );
	}

}
