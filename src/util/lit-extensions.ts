import { asyncAppend, AsyncAppendDirective } from 'lit/directives/async-append.js';
import { asyncReplace, AsyncReplaceDirective } from 'lit/directives/async-replace.js';
import { guard } from 'lit/directives/guard.js';
import { DirectiveResult } from 'lit/directive';
import { nothing } from 'lit';
import { AsyncPipe } from './async-pipe';

export function rhdcAsyncAppend<T>(
	asyncIterable: AsyncIterable<T>,
	renderer: (_:T) => unknown
) : DirectiveResult<typeof AsyncAppendDirective> {
	return guard( [ asyncIterable ], () => asyncAppend( fixPipe( asyncIterable, renderer ) ) );
}

export function rhdcAsyncReplace<T>(
	asyncIterable: AsyncIterable<T>,
	renderer: (_:T) => unknown
) : DirectiveResult<typeof AsyncReplaceDirective> {
	return guard( [ asyncIterable ], () => asyncReplace( fixPipe( asyncIterable, renderer ) ) );
}

/* There is a bug in Lit element where changing iterators won't cause an update
 * until the new iterator returns its first value. Use this adaptor to work around
 * this bug until it is fixed.
 */
function fixPipe<T>(
	asyncIterable: AsyncIterable<T>,
	renderer: (_:T) => unknown
) : AsyncPipe<unknown> {
	const pipeWrapper = new AsyncPipe<unknown>();
	fixPipeHelper( asyncIterable, renderer, pipeWrapper );
	return pipeWrapper;
}

async function fixPipeHelper<T>(
	asyncIterable: AsyncIterable<T>,
	renderer: (_:T) => unknown,
	pipeWrapper: AsyncPipe<unknown>
) : Promise<void> {
	pipeWrapper.push( nothing );
	for await ( const item of asyncIterable ) {
		pipeWrapper.push( renderer( item ) );
	}
	pipeWrapper.end();
}
