import { BasicConditionVariable } from './condition-variable';
import { ConstRef } from './types';

export class AsyncPipe<T> implements AsyncIterable<T> {

	#cvar: BasicConditionVariable = new BasicConditionVariable();
	#values: T[] = [];
	#done = false;

	public [Symbol.asyncIterator]() : AsyncIterator<T> {
		const conditionVariable = this.#cvar;
		const resolvedValues = this.#values;
		const isDone = () => this.#done;

		let i = 0;
		return {
			async next() {
				while( true ) {
					if( i < resolvedValues.length ) {
						return { value: resolvedValues[i++], done: false };
					} else if( isDone() ) {
						return { value: undefined, done: true };
					} else await conditionVariable.wait();
				}
			}
		};
	}

	public push( value: ConstRef<T> ) : void {
		this.#values.push( value );
		this.#cvar.notify();
	}

	public pushMany( values: ConstRef<T[]> ) : void {
		if( values.length === 0 ) return;
		this.#values.push( ...values );
		this.#cvar.notify();
	}

	public end() : void {
		this.#done = true;
		this.#cvar.notify();
	}

	public any() : boolean {
		return this.#values.length > 0;
	}

}
