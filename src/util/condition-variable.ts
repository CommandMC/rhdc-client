export class BasicConditionVariable {

	#notifier!: () => void;
	#promise: Promise<void>;

	constructor() {
		this.#promise = new Promise<void>( resolve => { this.#notifier = resolve; } );
	}

	public wait() : Promise<void> {
		return this.#promise;
	}

	public notify() : void {
		const lastNotifier = this.#notifier;
		this.#promise = new Promise<void>( resolve => { this.#notifier = resolve; } );
		lastNotifier();
	}

}
