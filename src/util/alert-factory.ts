import { html, nothing, HTMLTemplateResult } from 'lit';
import { Optional, Weak } from './types';
import '@shoelace-style/shoelace/dist/components/alert/alert';
import '@shoelace-style/shoelace/dist/components/icon/icon';

export type AlertType = 'primary' | 'success' | 'neutral' | 'warning' | 'danger';

export interface AlertCreationOptions {
	type: AlertType;
	title?: Weak<string>;
	message: string;
	closeable?: Optional<boolean>;
	hidden?: Optional<boolean>;
	icon?: Optional<string>;
}

export class AlertFactory {

	static create( options: AlertCreationOptions ) : HTMLTemplateResult {
		const title = options.title ? html`<strong>${options.title}</strong><br/>` : nothing;
		const icon = options.icon || AlertFactory.#getDefaultIcon( options.type );
		return html`
			<sl-alert variant=${options.type} ?open=${!options.hidden} ?closable=${!!options.closeable}>
				<sl-icon slot="icon" name=${icon}></sl-icon>
				${title}
				<span>${options.message}</span>
			</sl-alert>
		`;
	}

	static #getDefaultIcon( type: AlertType ) : string {
		switch( type ) {
			case 'primary': return 'info-circle';
			case 'success': return 'check2-circle';
			case 'neutral': return 'gear';
			case 'warning': return 'exclamation-triangle';
			case 'danger': return 'exclamation-octagon';
			default: throw 'Invalid alert type';
		}
	}

}
