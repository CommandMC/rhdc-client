import { Gender, User, WeakRichUserRef } from '../apis/users-api';
import { HexId, Nullable, Optional, UserId } from './types';
import { countryMap } from './countries';
import { ConsoleCompatibility, Hack } from '../apis/hacks-api';
import { toApiUrl } from '../util/url';
import { parseYouTubeLink } from './youtube-link';

type LdGender = 'https://schema.org/Male' | 'https://schema.org/Female' | 'nonbinary';

interface LdCountry {
	'@type': 'Country';
	identifier: string;
	name: string;
}

interface LdRating {
	'@type': 'AggregateRating';
	bestRating: 10;
	worstRating: 1;
	ratingValue?: Optional<number>;
	ratingCount?: Optional<0>;
}

interface LdDownloadCount {
	'@type': 'InteractionCounter';
	interactionType: 'https://schema.org/DownloadAction';
	userInteractionCount: number;
}

interface LdVideo {
	'@type': 'VideoObject';
	'@id': string;
	thumbnail: {
		'@type': 'ImageObject',
		contentUrl: string;
	},
	url: string;
	embedUrl: string;
}

export interface LdUser {
	'@type': 'Person';
	'@id'?: Optional<UserId>;
	name: string;
	callSign: string;
	url: string;
	birthDate?: Optional<string>;
	gender?: Optional<LdGender>;
	nationality?: Optional<LdCountry>;
}

export interface LdHack {
	'@type': 'SoftwareApplication';
	'@id': HexId;
	applicationCategory: 'GameApplication';
	applicationSubCategory: 'Romhack';
	offers: {
		'@type': 'Offer';
		price: 0;
	};
	isAccessibleForFree: true;
	isBasedOn?: Optional<'https://romhacking.com/hack/super-mario-64'>;
	aggregateRating: LdRating;
	contentRating?: Optional<'Mature'>;
	availableOnDevice: string[];
	dateCreated?: Optional<string>;
	datePublished: string;
	softwareRequirements?: Optional<'https://parallel-launcher.ca/'>;
	downloadUrl?: Optional<string>;
	interactionStatistic: LdDownloadCount;
	name: string;
	url: string;
	author: LdUser[];
	video: LdVideo[];
	screenshot: string[];
	thumbnailUrl?: Optional<string>;
}

export class LinkedData {

	static #makeGender( gender: Gender ) : LdGender {
		switch( gender ) {
			case Gender.Masculine: return 'https://schema.org/Male';
			case Gender.Feminine: return 'https://schema.org/Female';
			default: return 'nonbinary';
		}
	}

	static #makeCountry( code: string ) : Optional<LdCountry> {
		const country = countryMap.get( code );
		if( !country ) return undefined;
		return {
			'@type': 'Country',
			identifier: country.code,
			name: country.name
		};
	}

	static #makeRating( rating: Nullable<number> ) : LdRating {
		if( rating ) {
			return {
				'@type': 'AggregateRating',
				ratingValue: rating,
				bestRating: 10,
				worstRating: 1
			};
		} else {
			return {
				'@type': 'AggregateRating',
				bestRating: 10,
				worstRating: 1,
				ratingCount: 0
			};
		}
	}

	static #makeDownloadCount( count: number ) : LdDownloadCount {
		return {
			'@type': 'InteractionCounter',
			interactionType: 'https://schema.org/DownloadAction',
			userInteractionCount: count
		};
	}

	static #makeVideo( url: string ) : Optional<LdVideo> {
		const video = parseYouTubeLink( url );
		if( !video ) return undefined;
		return {
			'@type': 'VideoObject',
			'@id': video.videoId,
			thumbnail: {
				'@type': 'ImageObject',
				contentUrl: video.smallThumbnailLink()
			},
			url: video.standardLink(),
			embedUrl: video.embedLink()
		};
	}

	public static fromUser( user: WeakRichUserRef | User ) : LdUser {
		const ldUser : LdUser = {
			'@type': 'Person',
			name: user.username,
			callSign: user.username,
			url: `https://romhacking.com/user/${user.username}`
		};
	
		if( user.userId ) {
			ldUser['@id'] = user.userId;
		}
		
		if( user instanceof User && user.birthday ) {
			ldUser.birthDate = user.birthday.toISOString();
		}
	
		if( user.gender ) {
			ldUser.gender = LinkedData.#makeGender( user.gender );
		}
	
		if( user.country ) {
			const country = LinkedData.#makeCountry( user.country );
			if( country ) ldUser.nationality = country;
		}
	
		return ldUser;
	}

	public static fromHack( hack: Hack ) : LdHack {
		const ldHack : LdHack = {
			'@type': 'SoftwareApplication',
			'@id': hack.hackId,
			applicationCategory: 'GameApplication',
			applicationSubCategory: 'Romhack',
			offers: {
				'@type': 'Offer',
				price: 0,
			},
			isAccessibleForFree: true,
			aggregateRating: LinkedData.#makeRating( hack.rating ),
			availableOnDevice: [ 'Emulator' ],
			datePublished: hack.uploadedDate.toISOString(),
			interactionStatistic: LinkedData.#makeDownloadCount( hack.numDownloads ),
			name: hack.title,
			url: `https://romhacking.com/hack/${hack.urlTitle}`,
			author: hack.authors.map( LinkedData.fromUser ),
			video: hack.videos.map( LinkedData.#makeVideo ).filter( x => x ) as LdVideo[],
			screenshot: hack.screenshots.filter( x => !x.secured ).map( x => toApiUrl( x.directHref ) )
		};

		if( hack.createdDate ) {
			ldHack.dateCreated = hack.createdDate.toISOString();
		}

		for( const version of hack.versions ) {
			if( !version.archived && !version.download.secured ) {
				ldHack.downloadUrl = toApiUrl( version.download.directHref );
				break;
			}
		}

		if(
			hack.consoleCompatibility === ConsoleCompatibility.Playable ||
			hack.consoleCompatibility === ConsoleCompatibility.Good ||
			hack.consoleCompatibility === ConsoleCompatibility.Excellent
		) {
			ldHack.availableOnDevice.push( 'Nintendo 64' );
		} else {
			ldHack.softwareRequirements = 'https://parallel-launcher.ca/';
		}

		if( hack.urlTitle !== 'super-mario-64' ) {
			ldHack.isBasedOn = 'https://romhacking.com/hack/super-mario-64';
		}

		if( ldHack.screenshot.length > 0 ) {
			ldHack.thumbnailUrl = ldHack.screenshot[0];
		}

		if( hack.mature ) {
			ldHack.contentRating = 'Mature';
		}

		return ldHack;
	}

}
