import { ConstArray, Weak } from './types';

type StringProperties = {[_:string]: unknown };
type SymbolProperties = {[_:symbol]: unknown };

export function freezeObjArray<T extends Weak<{}>>( array: T[] ) : ConstArray<T> {
	return Object.freeze( array.map( x => Object.freeze( x ) ) );
}

export function deepFreeze( target: unknown ) : unknown {
	if( typeof target !== 'object' || target == null ) return target;

	for( const prop of Object.getOwnPropertyNames( target ) ) {
		deepFreeze( (target as StringProperties)[prop] );
	}

	for( const prop of Object.getOwnPropertySymbols( target ) ) {
		deepFreeze( (target as SymbolProperties)[prop] );
	}

	return Object.freeze( target );
}
