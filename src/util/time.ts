export class BasicTime {
	
	#hours: number;
	#minutes: number;
	
	constructor( hours?: number, minutes?: number ) {
		this.#hours = hours || 0;
		this.#minutes = minutes || 0;
	}
	
	public static fromLocalDate( dateTime: Date ) : BasicTime {
		return new BasicTime( dateTime.getHours(), dateTime.getMinutes() );
	}
	
	public static fromUtcDate( dateTime: Date ) : BasicTime {
		return new BasicTime( dateTime.getUTCHours(), dateTime.getUTCMinutes() );
	}
	
	public get hours() : number {
		return this.#hours;
	}
	
	public get minutes() : number {
		return this.#minutes;
	}
	
	public toSeconds() : number {
		const minutes = (this.#hours * 60) + this.#minutes;
		return minutes * 60;
	}
	
	public toMilliseconds() : number {
		return this.toSeconds() * 1000;
	}
	
	public addTo( dateTime: Date ) : Date {
		return new Date( dateTime.valueOf() + this.toMilliseconds() );
	}

	public toString() : string {
		return `${('0' + this.#hours).slice( -2 )}:${('0' + this.#minutes).slice( -2 )}`;
	}
	
}

export class ImmutableDate {
	
	#year: number;
	#month: number;
	#day: number;
	#utc: boolean;
	
	constructor( date: Date, utc: boolean ) {
		this.#utc = utc;
		if( utc ) {
			this.#year = date.getUTCFullYear();
			this.#month = date.getUTCMonth();
			this.#day = date.getUTCDate();
		} else {
			this.#year = date.getFullYear();
			this.#month = date.getMonth();
			this.#day = date.getDate();
		}
	}
	
	public static today( utc: boolean ) : ImmutableDate {
		const date = new ImmutableDate( new Date(), false );
		date.#utc = utc;
		return date;
	}

	public static parse( isoDate: string, utc: boolean ) : ImmutableDate {
		const timestamp = Date.parse( `${isoDate}T00:00:00${utc ? 'Z' : ''}` );
		return new ImmutableDate( new Date( timestamp ), utc );
	}
	
	public get year() : number {
		return this.#year;
	}
	
	public get month() : number {
		return this.#month;
	}
	
	public get day() : number {
		return this.#day;
	}
	
	public get utc() : boolean {
		return this.#utc;
	}
	
	public equals( other: ImmutableDate ) : boolean {
		return (
			this.#year === other.year &&
			this.#month === other.month &&
			this.#day === other.day &&
			this.#utc === other.utc
		);
	}
	
	public toDate( time?: BasicTime ) : Date {
		if( time ) {
			return time.addTo( this.toDate() );
		} else if( this.#utc ) {
			return new Date( Date.UTC( this.#year, this.#month, this.#day ) );
		} else {
			return new Date( this.#year, this.#month, this.#day );
		}
	}
	
	public toString() : string {
		const yearString = this.#year.toString().padStart( 4, '0' );
		const monthString = (this.#month + 1).toString().padStart( 2, '0' );
		const dayString = this.#day.toString().padStart( 2, '0' );
		return `${yearString}-${monthString}-${dayString}`;
	}
	
	public nextDay() : ImmutableDate {
		return this.#addDays( 1 );
	}
	
	public prevDay() : ImmutableDate {
		return this.#addDays( -1 );
	}
	
	public nextWeek() : ImmutableDate {
		return this.#addDays( 7 );
	}
	
	public prevWeek() : ImmutableDate {
		return this.#addDays( -7 );
	}
	
	public nextMonth() : ImmutableDate {
		return this.#addMonths( 1 );
	}
	
	public prevMonth() : ImmutableDate {
		return this.#addMonths( -1 );
	}
	
	public nextYear() : ImmutableDate {
		return this.#addMonths( 12 );
	}
	
	public prevYear() : ImmutableDate {
		return this.#addMonths( -12 );
	}
	
	#addDays( n: number ) : ImmutableDate {
		const date = this.toDate();
		if( this.#utc ) {
			date.setUTCDate( date.getUTCDate() + n );
		} else {
			date.setDate( date.getDate() + n );
		}
		return new ImmutableDate( date, this.utc );
	}
	
	#addMonths( n: number ) : ImmutableDate {
		const expectedMonth = (((this.#month + n) % 12) + 12) % 12;
		const date = this.toDate();
		if( this.#utc ) {
			date.setUTCMonth( date.getUTCMonth() + n );
			while( date.getUTCMonth() !== expectedMonth ) {
				date.setUTCDate( date.getUTCDate() - 1 );
			}
		} else {
			date.setMonth( date.getMonth() + n );
			while( date.getMonth() !== expectedMonth ) {
				date.setDate( date.getDate() - 1 );
			}
		}
		return new ImmutableDate( date, this.utc );
	}
	
}
