import { Nullable } from "./types";

export enum YoutubeLinkFormat {
	Standard,
	Short,
	Embed
}

export class ParsedYoutubeLink {

	#videoId: string;
	#format: YoutubeLinkFormat;

	constructor( videoId: string, format: YoutubeLinkFormat ) {
		this.#videoId = videoId;
		this.#format = format;
	}

	public get videoId() : string {
		return this.#videoId;
	}

	public get format() : YoutubeLinkFormat {
		return this.#format;
	}

	public normalized() : string {
		switch( this.#format ) {
			case YoutubeLinkFormat.Standard: return this.standardLink();
			case YoutubeLinkFormat.Short: return this.shortLink();
			case YoutubeLinkFormat.Embed: return this.embedLink();
			default: throw 'Invalid YouTube Link';
		}
	}

	public standardLink() : string {
		return `https://www.youtube.com/watch?v=${this.#videoId}`;
	}

	public shortLink() : string {
		return `https://youtu.be/${this.#videoId}`;
	}

	public embedLink() : string {
		return `https://www.youtube.com/embed/${this.#videoId}`;
	}

	public thumbnailLink() : string {
		return `https://i.ytimg.com/vi/${this.#videoId}/hq720.jpg`;
	}

	public smallThumbnailLink() : string {
		return `https://i.ytimg.com/vi/${this.#videoId}/hqdefault.jpg`;
	}

}

const g_standardRegex = /^(?:https?:\/\/)?(?:[a-zA-Z0-9][a-zA-Z0-9-]*\.)?youtube\.[a-zA-Z0-9][a-zA-Z0-9-]*\/watch\/?\?(?:v=|[^#]+&v=)([^&#]+)(?:[&#]|$)/;
const g_shortRegex = /^(?:https?:\/\/)?youtu\.be\/([^/?#]+)(?:$|[/?#])/;
const g_embedRegex = /^(?:https?:\/\/)?(?:[a-zA-Z0-9][a-zA-Z0-9-]*\.)?youtube\.[a-zA-Z0-9][a-zA-Z0-9-]*\/embed\/([^?#/]+)(?:[/?#]|$)/;

export function parseYouTubeLink( url: string ) : Nullable<ParsedYoutubeLink> {
	let matches = g_standardRegex.exec( url );
	if( matches && matches.length >= 2 ) {
		return new ParsedYoutubeLink( matches[1], YoutubeLinkFormat.Standard );
	}

	matches = g_shortRegex.exec( url );
	if( matches && matches.length >= 2 ) {
		return new ParsedYoutubeLink( matches[1], YoutubeLinkFormat.Short );
	}

	matches = g_embedRegex.exec( url );
	if( matches && matches.length >= 2 ) {
		return new ParsedYoutubeLink( matches[1], YoutubeLinkFormat.Embed );
	}

	return null;
}
