import { HTMLTemplateResult, html, nothing, ReactiveElement } from 'lit';
import { state } from 'lit/decorators/state.js';
import { Page } from '../apis/pagination';
import { AsyncPipe } from '../util/async-pipe';
import { Nullable, Optional, Weak } from '../util/types';
import { rhdcAsyncAppend } from '../util/lit-extensions';
import { AlertFactory } from '../util/alert-factory';
import { getErrorMessage } from '../util/http-error-parser';
import '../util/alert-factory';
import '../widgets/rhdc-load-more';

export interface RhdcPageRenderOptions {
	renderEmpty?: Weak<(() => unknown) | string>;
	renderError?: Weak<((_:unknown) => unknown) | string>;
}

export interface IRhdcPageState {

	render( options?: Optional<RhdcPageRenderOptions> ) : HTMLTemplateResult;
	lazyReset( getter?: Optional<() => Promise<Page<unknown>>> ) : void;
	reset( getter?: Optional<Promise<Page<unknown>>> ) : void;

	get loading() : boolean;
	get done () : boolean;
	get error() : unknown;
	get count() : number;

}

class RhdcPageState<T> implements IRhdcPageState {

	readonly #itemRenderer: (_:T) => unknown;
	readonly #notifyUpdated: () => void;
	readonly #loadMoreFunc: () => void;
	#pageGetter: Optional<() => Promise<Page<T>>>;

	#loading = false;
	#done = false;
	#items = new AsyncPipe<T>();
	#currentPage: Nullable<Page<T>> = null;
	#count = 0;
	#error: unknown = null;

	constructor(
		itemRenderer: (_:T) => unknown,
		pageGetter: Optional<() => Promise<Page<T>>>,
		updateNotifier: () => void
	) {
		this.#itemRenderer = itemRenderer;
		this.#pageGetter = pageGetter;
		this.#notifyUpdated = updateNotifier;
		this.#loadMoreFunc = () => this.#loadMore();
	}

	lazyReset( getter?: Optional<() => Promise<Page<unknown>>> ) : void {
		this.#loading = false;
		this.#done = false;
		this.#error = null;
		this.#currentPage = null;
		this.#count = 0;
		this.#items = new AsyncPipe<T>();
		if( getter ) this.#pageGetter = getter as () => Promise<Page<T>>;
		this.#notifyUpdated();
	}

	reset( getter?: Optional<Promise<Page<unknown>>> ) : void {
		this.lazyReset( (getter === undefined) ? undefined : () => getter );
		this.#loadMore();
	}

	render( options?: Optional<RhdcPageRenderOptions> ) : HTMLTemplateResult {
		let suffix: unknown;
		if( this.#error && options?.renderError !== null ) {
			suffix = this.#renderError( options?.renderError, this.#error );
		} else if( this.#done ) {
			suffix = (this.#count === 0) ? this.#renderEmpty( options?.renderEmpty ) : nothing;
		} else {
			suffix = html`<rhdc-load-more ?loading=${this.#loading} @rhdc-load-more=${this.#loadMoreFunc}></rhdc-load-more>`;
		}

		return html`${rhdcAsyncAppend( this.#items, this.#itemRenderer )}${suffix}`;
	}

	#renderError( renderer: Optional<((_:unknown) => unknown) | string>, error: unknown ) : unknown {
		if( renderer === undefined ) renderer = 'Failed to fetch results.';
		if( typeof renderer === 'string' ) {
			return AlertFactory.create({
				type: 'danger',
				title: renderer,
				message: getErrorMessage( error )
			});
		}

		return renderer( error );
	}

	#renderEmpty( renderer: Weak<(() => unknown) | string> ) : unknown {
		if( renderer === null ) return nothing;
		if( renderer === undefined ) renderer = 'No results.';
		if( typeof renderer === 'string' ) {
			return html`<span style="color: var(--sl-color-neutral-700);">${renderer}</span>`;
		}

		return renderer();
	}

	async #loadMore() : Promise<void> {
		if( this.#loading || this.#done || !this.#pageGetter ) return;

		this.#error = null;
		this.#loading = true;

		try {
			this.#notifyUpdated();

			if( this.#currentPage ) {
				this.#currentPage = await this.#currentPage.nextAsync();
			} else {
				this.#currentPage = await this.#pageGetter();
			}

			this.#count += this.#currentPage.values.length;
			this.#items.pushMany( this.#currentPage.values );
			this.#done = !this.#currentPage.hasNext;
		} catch( exception: unknown ) {
			this.#error = exception;
			this.#done = true;
		} finally {
			this.#loading = false;
			this.#notifyUpdated();
		}
	}

	get loading() : boolean { return this.#loading; }
	get done() : boolean { return this.#done; }
	get error() : unknown { return this.#error; }
	get count() : number { return this.#count; }

}

type DecoratedElement = ReactiveElement & { [index: PropertyKey]: IRhdcPageState; };

/**
 * Manages the state required for implementing a lazy loader list all in
 * one property. Pass in an item renderer and optionally a function to
 * fetch the first page (alternatively, this function can be provied later
 * using the reset method). An update will be triggered automatically when
 * needed, and the list can be rendered using the render method. The render
 * method does not include the container, which the consumer is responsible
 * for rendering.
 *
 * Example:
 * ```
 * @rhdcPageState( itemRendererFunc, firstPageGetterFunc )
 * myPageState!: IRhdcPageState;
 * ```
 * @category Decorator
 */
export function rhdcPageState<T>(
	renderer: (_:T) => unknown,
	getter?: Optional<() => Promise<Page<T>>>
) {
	return function( target: ReactiveElement, name: PropertyKey ) {
		const proto = target.constructor as typeof ReactiveElement;
		proto.addInitializer( instance => {
			(instance as DecoratedElement)[name] = new RhdcPageState(
				renderer.bind( instance ),
				getter?.bind( instance ),
				() => instance.requestUpdate( name, undefined )
			);
		});

		return state()( target, name );
	}
}
