import { ReactiveController, ReactiveElement } from 'lit';
import { state } from 'lit/decorators/state.js';

type DecoratedElement = ReactiveElement & { [index: PropertyKey]: boolean; };
const viewport: (VisualViewport | Window) = window.visualViewport || window;

class RhdcMediaQueryController implements ReactiveController {

	#host: DecoratedElement;
	#prop: PropertyKey;
	#query: string;

	constructor( host: ReactiveElement, prop: PropertyKey, query: string ) {
		this.#host = host as DecoratedElement;
		this.#prop = prop;
		this.#query = query;

		this._rerunMediaQuery = this._rerunMediaQuery.bind( this );
		this._rerunMediaQuery();
	}

	hostConnected() : void {
		viewport.addEventListener( 'resize', this._rerunMediaQuery );
	}

	hostDisconnected() : void {
		viewport.removeEventListener( 'resize', this._rerunMediaQuery );
	}

	private _rerunMediaQuery() : void {
		this.#host[this.#prop] = window.matchMedia( this.#query ).matches;
	}

}

/**
 * Declares a property that holds the result of a media query and updates itself
 * when the media query evaluation changes, triggering a re-render. The property
 * must be a boolean type.
 *
 * @param query A string containing the media query to evaluate
 *
 * Example:
 * ```
 * @rhdcMediaQuery( '(max-width: 900px)' )
 * isMobile!: boolean;
 * ```
 * @category Decorator
 */
export function rhdcMediaQuery( query: string ) {
	return function( target: ReactiveElement, name: PropertyKey ) {
		const proto = target.constructor as typeof ReactiveElement;
		proto.addInitializer( instance => {
			instance.addController( new RhdcMediaQueryController( instance, name, query ) );
		});

		return state()( target, name );
	}
}
