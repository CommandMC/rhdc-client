import { Auth } from "./auth";

let g_onCooldown = false;

async function reloadAuth() : Promise<void> {
	g_onCooldown = true;
	try {
		await Auth.refreshToken();
		await Auth.initAsync();

		setTimeout( () => { g_onCooldown = false; }, 15000 );
	} catch( exception: unknown ) {
		console.error( exception );
		g_onCooldown = false;
	}
}

export function roleChangeHandler( xhr: XMLHttpRequest ) : void {
	if( g_onCooldown || !xhr.getResponseHeader( 'X-Role-Changed' ) ) return;
	reloadAuth();
}
