import { UserId, Uuid } from "../util/types";
import { initAuthToken, setAuthToken } from "./auth-token";
import { HttpClient, HttpError, HttpErrorType } from "../apis/http-client";
import { Role } from "./roles";
import { getUserContext, setUserContext } from "../decorators/user-context";
import { delayAsync } from "../util/delay";
import { tryGetRetryDelay } from "../util/http-error-parser";

interface UserAuthData {
	userId: UserId;
	username: string;
	role: Role;
	assignedRole: Role;
	tokenExpires: string;
	mfaEnabled: boolean;
	application: 'Browser' | 'ParallelLauncher';
}

const TWO_WEEKS = 1000 * 60 * 60 * 24 * 14;

async function fetchAndStoreIdentityAsync() : Promise<void> {
	try {
		const authInfo = await HttpClient.getAsync<UserAuthData>( '/v3/auth/whoami' );
		setUserContext( Object.freeze( authInfo ) );

		const expiresIn = new Date( authInfo.tokenExpires ).getTime() - Date.now();
		if( expiresIn < TWO_WEEKS ) {
			await Auth.refreshToken();
		}
	} catch( err: unknown ) {
		console.warn( err );
		setUserContext( null );
		setAuthToken( null );
	}
}


export class Auth {

	static async initAsync() : Promise<void> {
		if( !initAuthToken() ) return;
		await fetchAndStoreIdentityAsync();
	}

	static async loginAsync( username: string, password: string, mfaCode?: number ) : Promise<boolean> {
		const loginInfo: Record<string,unknown> = {
			username: username,
			password: password,
			application: 'Browser'
		};

		if( mfaCode != null ) {
			loginInfo.mfaCode = mfaCode;
		}

		try {
			const response = await HttpClient.postJsonAsync<{ token: string }>( '/v3/auth/login', loginInfo );
			setAuthToken( response.token );
			await fetchAndStoreIdentityAsync();
			return true;
		} catch( exception: unknown ) {
			const httpError = exception as HttpError;

			if( mfaCode == null ) {
				const wwwAuth = httpError.tryGetHeader( 'WWW-Authenticate' );
				if( wwwAuth && wwwAuth.includes( 'mfa=required' ) ) {
					return false;
				}
			}

			switch( httpError.type ) {
				case HttpErrorType.Problem: {
					const retryAfter = tryGetRetryDelay( httpError );
					if( retryAfter ) await delayAsync( retryAfter );

					if( httpError.problem.status === 401 || httpError.problem.status === 403 || httpError.problem.status === 429 ) {
						if( !httpError.problem.detail || httpError.problem.detail === 'Not Authenticated' ) {
							throw 'Incorrect username or password.';
						} else {
							throw httpError.problem.detail;
						}
					} else {
						throw 'Unexpected server error.';
					}
				}
				case HttpErrorType.Basic:
					if( httpError.status.code === 401 ) {
						throw 'An unknown authentication failure occurred.';
					} else if( httpError.status.code === 0 ) {
						throw 'Could not reach the romhacking.com server';
					} else {
						throw 'Unexpected server error.';
					}
				default:
					console.error( httpError.exception );
					throw 'An unknown error occurred contacting the server.';
			}
		}
	}

	static logout() : void {
		setAuthToken( null );
		setUserContext( null );
	}

	static async requestCaptcha() : Promise<Blob> {
		try {
			return await HttpClient.getAsync<Blob>( '/v3/auth/captcha' );
		} catch( exception: unknown ) {
			console.error( exception );
			throw 'Failed to fetch CAPTCHA image';
		}
	}

	static async registerAccount( username: string, email: string, password: string, captcha: string ) : Promise<void> {
		try {
			await HttpClient.postJsonAsync<unknown>( '/v3/auth/register', {
				username: username,
				email: email,
				password: password,
				captcha: captcha
			});
		} catch( exception: unknown ) {
			const httpError = exception as HttpError;
			if( httpError.type === HttpErrorType.Problem && httpError.problem.detail && (httpError.problem.status === 400 || httpError.problem.status === 429) ) {
				throw httpError.problem.detail;
			} else if( httpError.type === HttpErrorType.Basic ) {
				throw (httpError.status.code === 0) ? 'Could not reach the romhacking.com server' : 'Unexpected server error.';
			} else {
				throw 'An unknown error occurred contacting the server.';
			}
		}
	}

	static async verifyEmail( emailToken: string ) : Promise<void> {
		try {
			await HttpClient.postTextAsync( '/v3/auth/verify', emailToken );
			await fetchAndStoreIdentityAsync();
		} catch( exception: unknown ) {
			const httpError = exception as HttpError;
			if( httpError.type === HttpErrorType.Problem && (httpError.problem.status === 400 || httpError.problem.status === 404) ) {
				throw 'Email verification code is incorrect.';
			} else if( httpError.type === HttpErrorType.Problem && httpError.problem.detail && httpError.problem.status === 403 ) {
				throw httpError.problem.detail;
			} else if( httpError.type === HttpErrorType.Basic ) {
				throw (httpError.status.code === 0) ? 'Could not reach the romhacking.com server' : 'Unexpected server error.';
			} else {
				throw 'An unknown error occurred contacting the server.';
			}
		}
	}

	static async sendVerificationEmail() : Promise<void> {
		try {
			await HttpClient.postAsync( '/v3/auth/resend-email' );
		} catch( exception: unknown ) {
			const httpError = exception as HttpError;
			if( httpError.type === HttpErrorType.Problem && httpError.problem.detail && httpError.problem.status === 429 ) {
				throw httpError.problem.detail;
			} else if( httpError.type === HttpErrorType.Basic && httpError.status.code === 0 ) {
				throw (httpError.status.code === 0) ? 'Could not reach the romhacking.com server' : 'Unexpected server error.';
			} else {
				throw 'An unknown error occurred contacting the server.';
			}
		}
	}

	static async refreshToken() : Promise<void> {
		const refresh = await HttpClient.postAsync<{token: string}>( '/v3/auth/refresh' );
		setAuthToken( refresh.token );
	}

	static requestPasswordResetAsync( email: string, mfaCode: number ) : Promise<void> {
		return HttpClient.postTextAsync<void>( `/v3/auth/passwordreset/request?mfaCode=${mfaCode}`, email );
	}

	static validatePasswordResetAsync( resetToken: Uuid ) : Promise<void> {
		return HttpClient.postAsync<void>( `/v3/auth/passwordreset/validate/${resetToken}` );
	}

	static async resetPasswordAsync( newPassword: string, resetToken: Uuid, mfaCode?: number ) : Promise<boolean> {
		try {
			await HttpClient.postJsonAsync<void>( '/v3/auth/passwordreset/confirm', {
				password: newPassword,
				resetToken: resetToken,
				mfaCode: mfaCode
			});
			return true;
		} catch( exception: unknown ) {
			if( mfaCode == null && exception instanceof HttpError ) {
				const wwwAuth = exception.tryGetHeader( 'WWW-Authenticate' );
				if( wwwAuth && wwwAuth.includes( 'mfa=required' ) ) return false;
			}
			throw exception;
		}
	}

	static isModOrStaff() : boolean {
		const role = getUserContext()?.role;
		return role === Role.Moderator || role === Role.Staff;
	}

}
