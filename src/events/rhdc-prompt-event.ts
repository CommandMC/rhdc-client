import { Optional } from '../util/types';

export interface RhdcPromptEventDetails {
	title: string;
	message: string;
	affirmText: string;
	rejectText: string;
	resolver: (_:boolean) => void;
}

export interface PromptOptions {
	affirmText?: Optional<string>,
	rejectText?: Optional<string>,
	resolver?: Optional<(_:boolean) => void>
}

export class RhdcPromptEvent extends CustomEvent<RhdcPromptEventDetails> {

	static create( title: string, message: string, options: Optional<PromptOptions> = undefined ) : RhdcPromptEvent {
		return new RhdcPromptEvent( 'rhdc-prompt-event', {
			bubbles: true,
			composed: true,
			detail: {
				title: title,
				message: message,
				affirmText: options?.affirmText || 'Yes',
				rejectText: options?.rejectText || 'No',
				resolver: options?.resolver || (() => {})
			}
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-prompt-event': RhdcPromptEvent;
	}
}
