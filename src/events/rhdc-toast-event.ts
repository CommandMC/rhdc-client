import { Nullable } from '../util/types';

type AlertType = 'primary' | 'success' | 'neutral' | 'warning' | 'danger';

interface RhdcToastEventDetails {
	type: AlertType;
	icon: string;
	summary: Nullable<string>;
	description: Nullable<string>;
}

export class RhdcToastEvent extends CustomEvent<RhdcToastEventDetails> {

	static #construct( type: AlertType, icon: string, summary: Nullable<string>, description: Nullable<string> ) : RhdcToastEvent {
		return new RhdcToastEvent( 'rhdc-toast-event', {
			bubbles: true,
			composed: true,
			detail: {
				type: type,
				icon: icon,
				summary: summary,
				description: description
			}
		});
	}

	static info( summary: Nullable<string>, description: Nullable<string> = null, icon = 'info-circle' ) : RhdcToastEvent {
		return RhdcToastEvent.#construct( 'primary', icon, summary, description );
	}

	static success( summary: Nullable<string>, description: Nullable<string> = null, icon = 'check2-circle' ) : RhdcToastEvent {
		return RhdcToastEvent.#construct( 'success', icon, summary, description );
	}

	static warn( summary: Nullable<string>, description: Nullable<string> = null, icon = 'exclamation-triangle' ) : RhdcToastEvent {
		return RhdcToastEvent.#construct( 'warning', icon, summary, description );
	}

	static error( summary: Nullable<string>, description: Nullable<string> = null, icon = 'exclamation-octagon' ) : RhdcToastEvent {
		return RhdcToastEvent.#construct( 'danger', icon, summary, description );
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-toast-event': RhdcToastEvent;
	}
}
