import { Hack } from '../apis/hacks-api';

interface RhdcHackUpdatedEventDetails {
	hack: Hack;
}

export class RhdcHackUpdatedEvent extends CustomEvent<RhdcHackUpdatedEventDetails> {

	static create( hack: Hack ) : RhdcHackUpdatedEvent {
		return new RhdcHackUpdatedEvent( 'rhdc-hack-updated-event', {
			bubbles: false,
			composed: true,
			detail: { hack: hack }
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-hack-updated-event': RhdcHackUpdatedEvent;
	}
}
