import { OgThing } from "../util/ogp";

interface RhdcLinkMetadataEventDetail {
	linkData: Object;
	previewData: OgThing;
}

export class RhdcLinkMetadataEvent extends CustomEvent<RhdcLinkMetadataEventDetail> {

	static create( linkData: Object, previewData: OgThing ) : RhdcLinkMetadataEvent {
		return new RhdcLinkMetadataEvent( 'rhdc-link-metadata-event', {
			bubbles: true,
			composed: true,
			detail: {
				linkData: linkData,
				previewData: previewData
			}
		});
	}

}

declare global {
	interface GlobalEventHandlersEventMap {
		'rhdc-link-metadata-event': RhdcLinkMetadataEvent;
	}
}
