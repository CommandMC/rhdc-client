import { html, css, CSSResultGroup, PropertyValues } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { DownloadInfo, FilesApi } from '../apis/files';
import { Weak } from '../util/types';
import { toApiUrl } from '../util/url';

@customElement( 'rhdc-image' )
export class RhdcImage extends RhdcElement {

	@property({ attribute: false })
	downloadInfo: Weak<DownloadInfo>;

	@query( 'img' )
	img!: HTMLImageElement;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
				width: auto;
				height: auto;
			}

			img {
				display: inline-block;
				width: inherit;
				height: inherit;
				object-position: center;
				object-fit: cover;
			}
		`;
	}

	override render() : unknown {
		return html`<img part="img" decoding="async" src="about:blank">`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		if( !changedProperties.has( 'downloadInfo' ) || !this.downloadInfo ) return;

		if( this.img ) {
			this.#updateImage();
		} else {
			this.updateComplete.then( this.#updateImage.bind( this ) );
		}
	}

	#updateImage() : void {
		if( !this.downloadInfo ) return;

		if( !this.img ) {
			console.error( 'Failed to update rhdc-image' );
			return;
		}

		if( this.downloadInfo.secured ) {
			this.img.src = 'about:blank';
			FilesApi.getDownloadTokenAsync( this.downloadInfo.webHref ).then( downloadToken => {
				this.img.src = toApiUrl( downloadToken.href );
			}).catch( console.error );
		} else {
			this.img.src = toApiUrl( this.downloadInfo.directHref );
		}
	}

}
