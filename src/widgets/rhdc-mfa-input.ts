import { css, CSSResultGroup, html } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/input/input';

@customElement( 'rhdc-mfa-input' )
export class RhdcMfaInput extends RhdcElement {

	@property({ attribute: 'disabled', type: Boolean })
	disabled = false;

	@property({ attribute: 'optional', type: Boolean })
	optional = false;

	@query( 'sl-input' )
	input!: SlInput;

	public get value() : number {
		return this.input.valueAsNumber || 0;
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			sl-input::part(form-control-label) {
				font-size: var(--sl-input-label-font-size-medium);
			}

			sl-input::part(input) {
				font-family: 'Noto Sans';
				font-size: 1.5rem;
				letter-spacing: 0.25em;
				padding: 0 var(--sl-input-spacing-small);
			}

			sl-input::part(base) {
				min-width: 9rem;
				max-width: 9rem;
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-input type="number"
				part="base"
				size="large"
				label="Authorization Code"
				?disabled=${this.disabled}
				placeholder="000000"
				no-spin-buttons
				?required=${!this.optional}
				pattern="[0-9]{6}"
				minlength="6"
				maxlength="6"
				min="0"
				max="999999"
				autocomplete="one-time-code"
				enterkeyhint="send"
				?spellcheck=${false}
				inputmode="numeric"
				@sl-input=${this.#onInput}
				@keydown=${this.#onKeyDown}
			></sl-input>
		`;
	}

	override focus( options?: FocusOptions ) : void {
		super.focus( options );
		this.input.focus( options );
	}

	#onInput() : void {
		const value = this.input.value;
		if( !value ) return;

		let safeInput = '';
		for( let i = 0; i < value.length; i++ ) {
			const c = value.charAt( i );
			if( c >= '0' && c <= '9' ) {
				safeInput += c;
			}
		}

		safeInput = safeInput.substring( 0, 6 );
		if( safeInput !== value ) {
			this.input.value = safeInput;
		}
	}

	#onKeyDown( event: KeyboardEvent ) : void {
		if( event.key !== 'Enter' ) return;
		this.dispatchEvent( new CustomEvent( 'rhdc-submit' ) );
	}

	public clear() : void {
		this.input.value = '';
	}

	public checkValidity() : boolean {
		return this.input.checkValidity();
	}

	public reportValidity() : boolean {
		if( this.input.checkValidity() ) {
			this.input.setCustomValidity( '' );
			return true;
		}

		this.input.setCustomValidity( 'Please enter the 6-digit code from your authenticator app.' );
		return false;
	}

}
