import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { ref } from 'lit/directives/ref.js';
import { RhdcElement  } from '../rhdc-element';
import { Optional, Weak } from '../util/types';
import { PropertyValues } from '@lit/reactive-element';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';

@customElement( 'rhdc-load-more' )
export class RhdcLoadMore extends RhdcElement {

	@property({ attribute: 'loading', type: Boolean })
	loading = false;

	@property({ attribute: false })
	root: Weak<Element> = null;

	@property({ attribute: false })
	rootMargin = '0px 0px 0px 0px';

	#observer: IntersectionObserver;
	#trigger: Optional<Element>;

	constructor() {
		super();
		this.#observer = this.#createObserver();
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				width: 100%;
			}

			sl-spinner {
				text-align: center;
				font-size: var(--sl-spacing-3x-large);
				--track-width: var(--sl-spacing-2x-small);
			}

			div {
				width: 100%;
				height: var(--sl-spacing-3x-large);
			}

			.centre {
				display: flex;
				justify-content: center;
			}
		`;
	}

	override render() : unknown {
		if( this.loading ) {
			return html`<div class="centre"><sl-spinner></sl-spinner></div>`;
		} else {
			return html`<div ${ref(this.#watch)}></div>`;
		}
	}

	override willUpdate( changedProperties: PropertyValues ) : void {
		if( changedProperties.has( 'root' ) || changedProperties.has( 'rootMargin' ) ) {
			this.#observer.disconnect();
			this.#observer = this.#createObserver();
			if( this.#trigger ) {
				this.#observer.observe( this.#trigger );
			}
		}
	}

	#watch( trigger: Optional<Element> ) : void {
		this.#trigger = trigger;
		if( trigger ) {
			this.#observer.observe( trigger );
		} else {
			this.#observer.disconnect();
		}
	}

	#createObserver() : IntersectionObserver {
		return new IntersectionObserver(
			entries => {
				if( entries && entries.some( entry => entry?.isIntersecting ) ) {
					this.dispatchEvent( new CustomEvent( 'rhdc-load-more', { bubbles: false, composed: false } ) );
				}
			},
			{
				root: this.root || null,
				rootMargin: this.rootMargin
			}
		);
	}

}
