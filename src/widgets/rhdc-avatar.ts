import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { toApiUrl } from '../util/url';
import '@shoelace-style/shoelace/dist/components/avatar/avatar';

const s_cacheBusters = new Map<string, number>();
const s_avatars = new Set<RhdcElement>();

@customElement( 'rhdc-avatar' )
export class RhdcAvatar extends RhdcElement {

	@property({ attribute: false })
	username!: string;

	@property({ attribute: 'square', type: Boolean })
	square = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline-block;
				--size: 3rem;

				--size-internal: var(--size);
			}

			sl-avatar {
				--size: var(--size-internal);
			}

			sl-avatar::part(image) {
				object-position: center;
				background-color: var(--sl-panel-background-color);
			}
		`;
	}

	override render() : unknown {
		let avatarUrl = `/v3/user/${this.username}/avatar`;
		const version = s_cacheBusters.get( this.username );
		if( version ) avatarUrl += `?cacheBuster=${version}`;
		return html`<sl-avatar shape=${this.square ? 'square' : 'rounded'} .image=${toApiUrl( avatarUrl )}></sl-avatar>`;
	}

	override connectedCallback() : void {
		super.connectedCallback();
		s_avatars.add( this );
	}

	override disconnectedCallback() : void {
		s_avatars.delete( this );
		super.disconnectedCallback();
	}

}

export function invalidateAvatarCache( username: string ) : void {
	const version = s_cacheBusters.get( username ) || 0;
	s_cacheBusters.set( username, version + 1 );
	for( const avatar of s_avatars ) {
		avatar.requestUpdate();
	}
}
