import { html, css, CSSResultGroup } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { styleMap } from 'lit/directives/style-map.js';
import { map } from 'lit/directives/map.js';
import { Nullable } from '../util/types';
import { ImmutableDate } from '../util/time';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input.js';
import '@shoelace-style/shoelace/dist/components/input/input.js';
import '@shoelace-style/shoelace/dist/components/dropdown/dropdown.js';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button.js';
import '@shoelace-style/shoelace/dist/components/icon/icon.js';

const MONTH_NAMES = [
	'January',
	'February',
	'March',
	'April',
	'May',
	'June',
	'July',
	'August',
	'September',
	'October',
	'November',
	'December'
];

const hasDateChanged = function( newDate: ImmutableDate, oldDate: ImmutableDate ) {
	return !oldDate || !newDate || !newDate.equals( oldDate );
};

@customElement( 'rhdc-month' )
class RhdcMonth extends RhdcElement {
	
	@property({ attribute: false })
	year: number;
	
	@property({ attribute: false })
	month: number;
	
	@property({ attribute: false, hasChanged: hasDateChanged })
	selected: ImmutableDate;
	
	@property({ attribute: false })
	utc: boolean;
	
	#pendingFocus: Nullable<string> = null;
	
	constructor() {
		super();
		this.selected = ImmutableDate.today( false );
		this.year = this.selected.year;
		this.month = this.selected.month;
		this.utc = false;
		
		this._onFocus = this._onFocus.bind( this );
		this._onBlur = this._onBlur.bind( this );
	}
	
	static override get styles() : CSSResultGroup {
		return css`
			table {
				table-layout: fixed;
				border-collapse: collapse;
				font-size: 1rem;
				color: var(--sl-color-neutral-700);
				background-color: var(--sl-panel-background-color);
			}
			
			thead {
				font-size: 0.8rem;
			}
			
			th {
				min-width: 2rem;
				min-height: 2rem;
				width: 2rem;
				height: 2rem;
				border: 1px solid var(--sl-panel-border-color);
				-moz-user-select: none;
				-webkit-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
			
			thead > tr > th:not(:last-child) {
				border-right: none;
			}
			
			thead > tr > th:not(:first-child) {
				border-left: none;
			}
			
			th > span {
				opacity: 0.5;
			}
			
			th:focus {
				background-color: var(--sl-color-primary-200);
			}
		`;
	}
	
	override render() : unknown {
		let day = this.#getFirstDay();
		
		const weeks = [ day ];
		for( let i = 1; i < 6; i++ ) {
			day = day.nextWeek();
			weeks.push( day );
		}
		
		return html`
			<table>
				<thead>
					<th><span>Sun.</span></th>
					<th><span>Mon.</span></th>
					<th><span>Tue.</span></th>
					<th><span>Wed.</span></th>
					<th><span>Thu.</span></th>
					<th><span>Fri.</span></th>
					<th><span>Sat.</span></th>
				</thead>
				<tbody>
					${map( weeks, this.#renderWeek.bind( this ) )}
				</tbody>
			</table>
		`;
	}
	
	#renderWeek( day: ImmutableDate ) : unknown {
		const days = [ day ];
		for( let i = 1; i < 7; i++ ) {
			day = day.nextDay();
			days.push( day );
		}
		
		return html`<tr>${map( days, this.#renderDay.bind( this ) )}</tr>`;
	}
	
	#renderDay( date: ImmutableDate ) : unknown {
		const styles = {
			fontWeight: date.equals( ImmutableDate.today( this.utc ) ) ? 'bold' : 'normal',
			backgroundColor: date.equals( this.selected ) ? 'var(--sl-color-primary-300)' : undefined
		};
		
		const cellContent = (date.month === this.month) ? date.day.toString() : html`<span>${date.day.toString()}</span>`;
		return html`
			<th
				id="${date.toString()}"
				style="${styleMap( styles )}"
				.tabIndex=${-1}
				@mouseenter=${this.#focusOnHover}
				@keydown=${this.#onKeyDown}
				@click=${this.#onClick}
			>${cellContent}</th>
		`;
	}
	
	override connectedCallback() : void {
		super.connectedCallback();
		this.tabIndex = 0;
		
		this.addEventListener( 'focus', this._onFocus );
		this.addEventListener( 'blur', this._onBlur );
	}
	
	override disconnectedCallback() : void {
		super.disconnectedCallback();
		
		this.removeEventListener( 'focus', this._onFocus );
		this.removeEventListener( 'blur', this._onBlur );
	}
	
	override updated( changedProperties: Map<string,unknown> ) : void {
		super.updated( changedProperties );
		
		if( changedProperties.has( 'selected' ) ) {
			this.#getCell( this.selected.toString() )?.focus();
		} else if( this.#pendingFocus ) {
			this.#getCell( this.#pendingFocus )?.focus();
		}
		
		this.#pendingFocus = null;
	}
	
	#onKeyDown( e: KeyboardEvent ) : void {
		let date = this.#getCellDate( e.target );
		if( !date ) return;
		
		switch( e.key ) {
			case 'ArrowUp':
			case 'Up':
				date = date.prevWeek();
				break;
			case 'ArrowDown':
			case 'Down':
				date = date.nextWeek();
				break;
			case 'ArrowLeft':
			case 'Left':
				date = date.prevDay();
				break;
			case 'ArrowRight':
			case 'Right':
				date = date.nextDay();
				break;
			case 'PageUp':
				date = e.ctrlKey ? date.prevYear() : date.prevMonth();
				break;
			case 'PageDown':
				date = e.ctrlKey ? date.nextYear() : date.nextMonth();
				break;
			case 'Home':
				date = ImmutableDate.today( this.utc );
				break;
			case 'Backspace':
			case 'Undo':
				date = this.selected;
				break;
			case 'Enter':
			case 'Spacebar':
			case ' ':
				this.selected = date;
				this.dispatchEvent( new CustomEvent( 'selected', { bubbles: true, composed: true } ) );
				
				(e.target as HTMLElement)?.blur();
				this.blur();
				
				e.stopPropagation();
				e.preventDefault();
				return;
			default: return;
		}
		
		this.#focusDate( date );
		
		e.stopPropagation();
		e.preventDefault();
	}
	
	#onClick( e: Event ) : void {
		const date = this.#getCellDate( e.target );
		if( !date ) return;
		
		this.selected = date;
		this.dispatchEvent( new CustomEvent( 'selected', { bubbles: true, composed: true } ) );
		
		(e.target as HTMLElement)?.blur();
		this.blur();
		
		e.stopPropagation();
	}
	
	private _onFocus() : void {
		const selectedCell = this.#getCell( this.selected.toString() );
		if( selectedCell ) {
			selectedCell.focus();
		} else {
			this.#focusDate( this.#getFirstDay() );
		}
		this.tabIndex = -1;
	}
	
	private _onBlur() : void {
		this.tabIndex = 0;
	}
	
	#getFirstDay() : ImmutableDate {
		if( this.utc ) {
			const date = new Date( Date.UTC( this.year, this.month, 1 ) );
			date.setUTCDate( 1 - date.getUTCDay() );
			return new ImmutableDate( date, true );
		} else {
			const date = new Date( this.year, this.month, 1 );
			date.setDate( 1 - date.getDay() );
			return new ImmutableDate( date, false );
		}
	}
	
	#focusDate( date: ImmutableDate ) : void {
		const cell = this.#getCell( date.toString() );
		if( cell ) {
			cell.focus();
			return;
		}
		
		this.year = date.year;
		this.month = date.month;
		this.#pendingFocus = date.toString();
		this.dispatchEvent( new CustomEvent( 'month-changed', { bubbles: false, composed: false } ) );
	}
	
	#focusOnHover( e: Event ) : void {
		(e.target as HTMLElement).focus();
	}
	
	#getCellDate( cellElement: Nullable<EventTarget> ) : Nullable<ImmutableDate> {
		const isoDate = (cellElement as Element)?.id;
		if( !isoDate ) return null;
		return ImmutableDate.parse( isoDate, this.utc );
	}
	
	#getCell( id: string ) : Nullable<HTMLTableCellElement> {
		return this.shadowRoot ? this.shadowRoot.getElementById( id ) as Nullable<HTMLTableCellElement> : null;
	}
	
}

@customElement( 'rhdc-calendar' )
class RhdcCalendar extends RhdcElement {
	
	@property({ attribute: false })
	utc: boolean;
	
	@property({ attribute: false, hasChanged: hasDateChanged })
	value: ImmutableDate;
	
	@state()
	year: number;
	
	@state()
	month: number;
	
	@query( '#month' )
	monthElement!: RhdcMonth;
	
	constructor() {
		super();
		
		this.utc = false;
		this.value = ImmutableDate.today( false );
		
		this.year = this.value.year;
		this.month = this.value.month;
	}
	
	static override get styles() : CSSResultGroup {
		return css`
			div#container {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				font-size: 1rem;
				padding: 1rem;
				border: solid 1px var(--sl-panel-border-color);
				background-color: var(--sl-color-neutral-0);
			}
			
			div#container > * {
				flex-grow: 0;
			}
			
			div#header, div#controls {
				display: flex;
				flex-direction: row;
				justify-content: space-between;
			}
			
			div#controls > * {
				flex-grow: 0;
			}
			
			rhdc-button {
				margin-left: 0;
				margin-right: 0;
			}
		`;
	}
	
	override render() : unknown {
		return html`
			<div id="container">
				<div id="header">
					<span>${MONTH_NAMES[this.month]}</span>
					<span>${this.year.toString()}</span>
				</div>
				<div id="controls">
					<sl-icon-button name="chevron-double-left" @click=${this.#prevYear}></sl-icon-button>
					<sl-icon-button name="chevron-left" @click=${this.#prevMonth}></sl-icon-button>
					<sl-icon-button name="house" @click=${this.#home}></sl-icon-button>
					<sl-icon-button name="chevron-right" @click=${this.#nextMonth}></sl-icon-button>
					<sl-icon-button name="chevron-double-right" @click=${this.#nextYear}></sl-icon-button>
				</div>
				<rhdc-month
					id="month"
					.year=${this.year}
					.month=${this.month}
					.utc=${this.utc}
					.selected=${this.value}
					@month-changed=${this.#onMonthChanged}
					@selected=${this.#onSelection}
				></rhdc-month>
			</div>
		`;
	}
	
	override willUpdate( changedProperties: Map<string,unknown> ) : void {
		super.update( changedProperties );
		
		if( changedProperties.has( 'value' ) ) {
			this.year = this.value.year;
			this.month = this.value.month;
		}
	}
	
	override focus( options?: FocusOptions ) : void {
		this.monthElement?.focus( options );
	}
	
	#onSelection( e: Event ) : void {
		const view = e.target as RhdcMonth;
		this.value = view.selected;
	}
	
	#onMonthChanged( e: Event ) : void {
		const view = e.target as RhdcMonth;
		this.year = view.year;
		this.month = view.month;
	}
	
	#prevMonth() : void {
		if( this.month > 0 ) {
			this.month--;
		} else {
			this.month = 11;
			this.year--;
		}
	}
	
	#nextMonth() : void {
		if( this.month < 11 ) {
			this.month++;
		} else {
			this.month = 0;
			this.year++;
		}
	}
	
	#prevYear() : void {
		this.year--;
	}
	
	#nextYear() : void {
		this.year++;
	}
	
	#home() : void {
		this.month = this.value.month;
		this.year = this.value.year;
	}
	
}

@customElement( 'rhdc-date' )
export class RhdcDateInput extends RhdcElement {
	
	@property({ attribute: false, hasChanged: hasDateChanged })
	value: ImmutableDate;
	
	@property({ type: Boolean, attribute: 'utc', reflect: true })
	utc: boolean;
	
	@property({ type: Boolean, attribute: 'disabled', reflect: true })
	disabled: boolean;

	@property({ attribute: 'label', type: String })
	label = '';
	
	@query( 'rhdc-calendar' )
	calendar!: RhdcCalendar;

	@query( 'input' )
	input!: SlInput;
	
	constructor() {
		super();
		
		this.value = ImmutableDate.today( false );
		this.utc = false;
		this.disabled = false;
	}
	
	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline-block;
				margin: 4px;
				max-width: 12ch;
			}
			
			div#container {
				position: relative;
				min-width: 12ch;
				width: 100%;
			}
			
			sl-input {
				box-sizing: border-box;
				width: 100%;
			}
			
			sl-icon {
				position: absolute;
				top: calc( 50% - 0.5rem );
				right: 5px;
				height: 1rem;
				width: 1rem;
				font-size: 1rem;
				pointer-events: none;
			}
			
			div#container:focus-within > rhdc-calendar {
				display: block;
			}
			
			div#container:focus-within > input, div#container:focus-within > input:hover {
				border-color: var(--sl-color-primary-600);
			}
			
			:host(:disabled) rhdc-calendar {
				display: none !important;
			}
		`;
	}
	
	override render() : unknown {
		return html`
			<sl-dropdown
				placement="top"
				.containingElement=${this}
			>
				<sl-input
					slot="trigger"
					type="text"
					label=${this.label}
					required
					readonly
					?disabled=${this.disabled}
					.tabIndex=${0}
					.value=${this.value.toString()}
				>
					<sl-icon name="calendar3" slot="suffix"></sl-icon>
				</sl-input>
				<rhdc-calendar
					.value=${this.value}
					.utc=${this.utc}
					@selected=${this.#onChange}
				></rhdc-calendar>
			</sl-dropdown>
		`;
	}

	public setCustomValidity( message: string ) : void {
		this.input.setCustomValidity( message );
	}
	
	public override focus( options?: FocusOptions ) : void {
		if( this.disabled ) {
			super.focus( options );
		} else {
			this.calendar.focus();
		}
	}
	
	#onChange( e: Event ) : void {
		this.value = this.calendar.value;
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
		this.blur();
		e.stopPropagation();
	}
	
}
 
