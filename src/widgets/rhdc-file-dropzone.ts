import { html, css, CSSResultGroup } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { RhdcFileDropEvent } from '../events/rhdc-file-drop-event';
import { Nullable, Optional } from '../util/types';

@customElement( 'rhdc-file-dropzone' )
export class RhdcFileDropzone extends RhdcElement {

	@property({ attribute: 'disabled', type: Boolean })
	disabled = false;

	@property({ attribute: false })
	value: Nullable<File> = null;

	@property({ attribute: 'accept', type: String })
	accept = '';

	@state()
	dragHover = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				align-items: stretch;
				margin: var(--sl-spacing-small);
				--padding: var(--sl-spacing-small);
			}

			div {
				box-sizing: border-box;
				display: flex;
				width: 100%;
				justify-content: center;
				align-items: center;
				padding: --padding;
				border: 2px solid var(--sl-color-neutral-300);
				border-radius: var(--sl-border-radius-large);
				color: var(--sl-color-neutral-400);
				font-size: var(--sl-font-size-x-large);
				text-align: center;
				background-color: transparent;
				cursor: pointer;
			}

			div:focus {
				border-color: var(--sl-color-primary-400);
			}

			div:hover {
				border-color: var(--sl-color-primary-400);
				color: var(--sl-color-primary-400);
			}

			div[data-active], div[data-active]:hover {
				border-color: var(--sl-color-primary-400);
				background-color: var(--sl-color-neutral-50);
				color: var(--sl-color-primary-400);
			}

			:host([disabled]) > div {
				border-color: var(--sl-color-neutral-500) !important;
				background-color: var(--sl-color-neutral-200) !important;
				color: var(--sl-color-neutral-500) !important;
				cursor: no-drop;
			}
		`;
	}

	override render() : unknown {
		return html`
			<div part="base"
				tabindex="0"
				?data-active=${this.dragHover}
				@dragleave=${this.#onDragLeave}
				@dragover=${this.#onDragOver}
				@drop=${this.#onDrop}
				@keydown=${this.#onKeyDown}
				@click=${this.#onClick}
			>
				<slot></slot>
			</div>
		`;
	}

	override focus( options: Optional<FocusOptions> ) : void {
		this.shadowRoot?.querySelector( 'div' )?.focus( options );
	}

	#onKeyDown( event: KeyboardEvent ) : void {
		if( event.key === ' ' || event.key === 'Spacebar' || event.key === 'Enter' || event.key === 'Accept' ) {
			this.#onClick();
			event.stopPropagation();
		}
	}

	#onClick() : void {
		const fileInput = document.createElement( 'input' );
		fileInput.type = 'file';
		if( this.accept ) {
			fileInput.accept = this.accept;
		}
		fileInput.onchange = () => {
			if( !fileInput.files || !fileInput.files[0] ) return;

			this.value = fileInput.files[0];
			this.dispatchEvent( RhdcFileDropEvent.create( this.value ) );
		};

		fileInput.click();
	}

	#onDragLeave() : void {
		this.dragHover = false;
	}

	#onDragOver( event: DragEvent ) : void {
		if( this.disabled || !event.dataTransfer?.items?.length || event.dataTransfer.items[0].kind !== 'file' ) return;

		this.dragHover = true;
		event.preventDefault();
	}

	#onDrop( event: DragEvent ) : void {
		if( this.disabled || !event.dataTransfer?.files?.length ) return;
		event.preventDefault();

		this.value = event.dataTransfer.files[0];
		this.dispatchEvent( RhdcFileDropEvent.create( this.value ) );

		this.dragHover = false;
	}

}
