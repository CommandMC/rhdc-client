import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { map } from 'lit/directives/map.js';
import { Weak } from '../util/types';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/tag/tag';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/dropdown/dropdown';
import '@shoelace-style/shoelace/dist/components/menu/menu';
import '@shoelace-style/shoelace/dist/components/menu-item/menu-item';
import '@shoelace-style/shoelace/dist/components/visually-hidden/visually-hidden';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlMenuItem from '@shoelace-style/shoelace/dist/components/menu-item/menu-item';
import SlMenu from '@shoelace-style/shoelace/dist/components/menu/menu';
import SlDropdown from '@shoelace-style/shoelace/dist/components/dropdown/dropdown';
import SlSelectEvent from '@shoelace-style/shoelace/dist/events/sl-select';

@customElement( 'rhdc-tag-input' )
export class RhdcTagInput extends RhdcElement {

	@property({ attribute: false })
	value: string[] = [];

	@property({ attribute: 'loading', type: Boolean })
	loading = false;

	@property({ attribute: 'allow-spaces', type: Boolean })
	allowSpaces = false;

	@property({ attribute: 'case-sensitive', type: Boolean })
	caseSensitive = false;

	@property({ attribute: 'highlight-existing', type: Boolean })
	highlightExisting = false;

	@property({ attribute: 'require-match', type: Boolean })
	requireMatch = false;

	@property({ attribute: 'placeholder', type: String })
	placeholder = '';

	@property({ attribute: 'disabled', type: Boolean })
	disabled = false;

	@property({ attribute: 'label', type: String })
	label = '';

	@property({ attribute: 'required', type: Boolean })
	required = false;

	@property({ attribute: 'hoist', type: Boolean })
	hoist = false;

	@property({ attribute: false })
	autocomplete: string[] = [];

	@state()
	tagMatches: string[] = [];

	@query( 'sl-input' )
	input!: SlInput;

	@query( 'sl-menu' )
	menu: Weak<SlMenu>;

	@query( 'sl-menu-item.first' )
	firstSuggestion: Weak<SlMenuItem>;

	@query( 'sl-dropdown' )
	dropdown: Weak<SlDropdown>;

	#tagMap : Map<string,string> = new Map();
	#resizeObserver!: ResizeObserver;
	#onBlur: () => void;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				position: relative;
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
				--visible-rows: 5;
			}

			sl-input {
				width: 100%;
			}

			sl-input::part(form-control) {
				display: flex;
				flex-direction: column;
			}

			sl-menu {
				overflow: unset;
				max-height: unset;
			}

			sl-menu::part(base) {
				overflow-x: visible;
				overflow-y: auto;
				max-height: calc(
					var(--sl-spacing-x-small) + (
						var(--visible-rows) * (
							(1em * var(--sl-line-height-normal)) +
							(var(--sl-spacing-2x-small) * 2)
						)
					)
				);
			}

			.tags {
				display: flex;
				flex-wrap: wrap;
				gap: var(--sl-spacing-x-small);
				align-content: center;
				box-sizing: border-box;
			}

			.label {
				font-size: var(--sl-input-label-font-size-medium);
				color: var(--sl-input-label-color);
				-webkit-text-size-adjust: none;
			}

			.label[data-required]::after {
				content: var(--sl-input-required-content);
				margin-inline-start: var(--sl-input-required-content-offset);
			}
		`;
	}

	override render() : unknown {
		let menu = null;
		if( this.tagMatches.length ) {
			menu = html`
				<sl-menu part="menu" @sl-select=${this.#suggestionSelected}>
					${map( this.tagMatches, this.#renderSuggestion.bind( this ) )}
				</sl-menu>
			`;
		}

		let visibleLabel = null;
		let ariaLabel = null;
		if( this.label ) {
			visibleLabel = html`<div part="label" class="label" aria-hidden="true" ?data-required=${this.required}>${this.label}&nbsp;</div>`;
			ariaLabel = html`<sl-visually-hidden slot="label">${this.label}</sl-visually-hidden>`;
		}

		const prefix = this.loading ? html`<sl-spinner slot="prefix"></sl-spinner>` : null;
		return html`
			${visibleLabel}
			<div part="tags" class="tags">
				${map( this.value, this.#renderTag.bind( this ) )}
			</div>
			<sl-dropdown
				part="base"
				.hoist=${this.hoist}
				?disabled=${this.disabled || this.loading}
				@sl-show=${this.#resizeMenu}
				stay-open-on-select
			>
				<sl-input
					part="input"
					slot="trigger"
					type="text"
					placeholder="${this.placeholder}"
					?disabled=${this.disabled || this.loading}
					enterkeyhint="enter"
					inputmode="text"
					@sl-input=${this.#onInput}
					@keydown=${this.#onKeyDown}
				>${prefix}${ariaLabel}</sl-input>
				${menu}
			</sl-dropdown>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'autocomplete' ) || changedProperties.has( 'caseSensitive' ) ) {
			this.#tagMap.clear();
			for( const tag of this.autocomplete ) {
				if( this.caseSensitive ) {
					this.#tagMap.set( tag, tag );
				} else {
					this.#tagMap.set( tag.toLocaleLowerCase(), tag );
				}
			}
		}
	}

	constructor() {
		super();
		this.#onBlur = this.#processInput.bind( this, true );
	}

	override connectedCallback() : void {
		super.connectedCallback();
		this.addEventListener( 'blur', this.#onBlur );
		this.#resizeObserver = new ResizeObserver( this.#resizeMenu.bind( this ) );
		this.updateComplete.then(() => {
			this.#resizeObserver.observe( this );
		});
	}

	override disconnectedCallback(): void {
		this.#resizeObserver.unobserve( this );
		this.removeEventListener( 'blur', this.#onBlur );
		super.disconnectedCallback();
	}

	protected override updated( changedProperties: PropertyValues ): void {
		super.updated( changedProperties );
		this.#resizeMenu();

		if( this.dropdown && !this.dropdown.open ) {
			this.dropdown.show();
		}
	}

	public reportValidity() : boolean {
		if( !this.required || this.value.length > 0 ) return true;
		this.input.reportValidity();
		return false;
	}

	public setCustomValidity( message: string ) : void {
		this.input.setCustomValidity( message );
	}

	#resizeMenu() : void {
		if( !this.menu ) return;
		this.menu.style.width = `${this.input.clientWidth}px`;
		window.requestAnimationFrame(() => {
			this.dropdown?.reposition();
		});
	}

	#renderTag( tag: string, index: number ) : unknown {
		if( !tag ) return null;
		
		const highlight = this.highlightExisting ? this.#tagMap.has( this.caseSensitive ? tag : tag.toLocaleLowerCase() ) : false;
		return html`
			<sl-tag
				variant="${highlight ? 'primary' : 'neutral'}"
				size="small"
				?removable=${!this.disabled}
				@sl-remove=${this.#removeTag.bind( this, index )}
			>${tag}</sl-tag>
		`;
	}

	#renderSuggestion( tag: string, index: number ) : unknown {
		if( index === 0 ) {
			return html`<sl-menu-item .value=${tag} class="first" @keydown=${this.#onFirstSuggestionKeyDown}>${tag}</sl-menu-item>`;
		} else {
			return html`<sl-menu-item .value=${tag}>${tag}</sl-menu-item>`;
		}
	}

	#onInput() : void {
		if( !this.allowSpaces && /\s/g.test( this.input.value ) ) {
			this.#processInput( true );
		} else {
			this.#updateMatches();
		}
	}

	#onKeyDown( event: KeyboardEvent ) : void {
		event.stopPropagation();
		if( !this.input.value ) {
			return;
		}

		switch( event.key ) {
			case 'Enter':
			case 'Accept':
				this.#processInput( false );
				event.preventDefault();
				break;
			case 'Tab':
				this.#processInput( true );
				event.preventDefault();
				break;
			case 'Spacebar':
			case '':
				if( !this.allowSpaces ) {
					this.#processInput( true );
					event.preventDefault();
				}
				break;
			case 'ArrowDown':
			case 'Down':
				if( this.menu && this.firstSuggestion ) {
					this.menu.focus();
					this.firstSuggestion.tabIndex = 0;
					this.firstSuggestion.focus();
					event.preventDefault();
				}
				break;
		}
	}

	#onFirstSuggestionKeyDown( event: KeyboardEvent ) {
		if( event.key === 'ArrowUp' || event.key === 'Up' ) {
			this.input.focus();
			event.stopPropagation();
			event.preventDefault();
		}
	}

	#processInput( autocomplete: boolean ) : void {
		if( !this.allowSpaces ) {
			const tags = this.input.value.split( /\s+/ );
			for( const tag of tags ) {
				this.#addTag( tag, autocomplete || tags.length > 1 );
			}

			this.input.value = '';
			this.tagMatches = [];
			return;
		}

		const tag = this.input.value.trim();
		this.input.value = '';
		this.tagMatches = [];

		this.#addTag( tag, autocomplete );
	}

	#addTag( tag: string, autocomplete: boolean ) : void {
		if( !tag ) return;

		if( autocomplete || this.requireMatch || !this.caseSensitive ) {
			const match = this.#tagMap.get( tag.toLocaleLowerCase() );
			if( !match && this.requireMatch ) return;
			tag = match || tag;
		}

		if( this.value.includes( tag ) ) return;
		this.value = [ ...this.value, tag ];
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
	}

	#removeTag( index: number ) : void {
		if( index >= this.value.length ) return;
		this.value.splice( index, 1 );
		this.value = [ ...this.value ];
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
	}

	#suggestionSelected( event: SlSelectEvent ) : void {
		this.value = [ ...this.value, event.detail.item.value ];
		this.input.value = '';
		this.tagMatches = [];
		this.input.focus();
		this.dispatchEvent( new CustomEvent( 'rhdc-change' ) );
		this.#onInput();
	}

	#updateMatches() : void {
		const search = this.input.value;
		if( !search || search.length < 3 ) {
			this.tagMatches = [];
			return;
		}

		const searchLower = search.toLocaleLowerCase();

		const p1 = [];
		const p2 = [];
		const p3 = [];

		for( const tag of this.autocomplete ) {
			if( tag.startsWith( search ) ) {
				p1.push( tag );
				continue;
			}

			const tagLower = tag.toLocaleLowerCase();
			if( tagLower.startsWith( searchLower ) ) {
				p2.push( tag );
				continue;
			}

			if( tagLower.includes( searchLower ) ) {
				p3.push( tag );
				continue;
			}
		}

		const matches = [ p1, p2, p3 ].flat();
		if( this.caseSensitive ) {
			const existing = new Set<string>( this.value );
			this.tagMatches = matches.filter( tag => !existing.has( tag ) );
		} else {
			const existing = new Set<string>( this.value.map( tag => tag.toLocaleLowerCase() ) );
			this.tagMatches = matches.filter( tag => !existing.has( tag.toLocaleLowerCase() ) );
		}
	}

}
