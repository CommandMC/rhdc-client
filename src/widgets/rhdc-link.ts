import { html, css, CSSResultGroup } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { rhdcLinkStyles } from '../common-styles';

@customElement( 'rhdc-link' )
export class RhdcLink extends RhdcElement {

	@property({ attribute: 'href', type: String })
	href = 'about:blank';

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline;
			}

			${rhdcLinkStyles}
		`;
	}

	override render() : unknown {
		return html`
			<a part="base" href="${this.href}" @click=${this.#onClick}>
				<slot></slot>
			</a>
		`;
	}

	#onClick( event: Event ) : void {
		event.stopPropagation();
		event.preventDefault();
		this.navigate( this.href );
	}

}
