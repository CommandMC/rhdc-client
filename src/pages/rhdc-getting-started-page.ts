import { css, CSSResultGroup, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { styleMap, StyleInfo } from 'lit/directives/style-map.js';
import { rhdcLinkStyles } from '../common-styles';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-getting-started-page' )
export class RhdcGettingStartedPage extends RhdcElement {

	static override get styles() : CSSResultGroup {
		return css`
			${rhdcLinkStyles}

			:host {
				display: contents;
			}

			.page {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				margin: auto;
				padding: var(--sl-spacing-small) 0;
			}

			.page > img {
				align-self: center;
			}

			.page > h2 {
				padding: 0 var(--sl-spacing-x-small);
			}

			.page > p {
				margin: 0;
				padding: 0 var(--sl-spacing-x-small);
			}

			sl-button {
				align-self: flex-start;
				margin: var(--sl-spacing-medium);
				pointer-events: none;
			}
		`;
	}

	override render() : unknown {
		const rootStyle: StyleInfo = {
			maxWidth: `max( min( 100vw, 1200px ), ${1280 / window.devicePixelRatio}px )`
		};

		const imgStyle1: StyleInfo = {
			width: `${757 / window.devicePixelRatio}px`,
			height: `${872 / window.devicePixelRatio}px`,
			margin: `0 ${-20 / window.devicePixelRatio}px 0 ${-56 / window.devicePixelRatio}`
		};

		const imgStyle2: StyleInfo = {
			width: `${1392 / window.devicePixelRatio}px`,
			height: `${841 / window.devicePixelRatio}px`,
			margin: `0 ${-56 / window.devicePixelRatio}`
		}

		return html`
			<div class="page" style=${styleMap( rootStyle )}>
				<h2>Getting Started</h2>
				<p>
					For the best experience in playing romhacks, download <a href="https://parallel-launcher.ca/" target="_blank">Parallel Launcher</a>.
					Once you have downloaded and installed Parallel Launcher, you can jump into playing a hack directly from this website by clicking on the
					<strong>Play Now</strong> button on each hack page.
				</p>
				<sl-button variant="success" tabindex="-1" aria-readonly="true" aria-disabled="true" inert>
					<sl-icon src="/assets/icons/rhdc-link.svg" slot="prefix"></sl-icon>
					<span>Play Now</span>
				</sl-button>
				<p>
					This will automatically download, patch, and run the hack with the settings recommended by the hack author. You can also manually
					download patch files and run them in Parallel Launcher.
				</p>
				<span>&nbsp;</span>
				<p>
					To better manage your romhacks, create an account here on Romhacking.com by clicking on the Sign In button in the top-right and
					registering a new account. You can then enable Romhacking.com integration in Parallel Launcher as shown below:
				</p>
				<img style=${styleMap( imgStyle1 )} src="/assets/img/pl-enable-rhdc.png" />
				<p>
					With Romhacking.com integration enabled, your playlists on Romhacking.com will automatically be synced with your rom groups in
					Parallel Launcher, and you will be able to download your saved hacks from within Parallel Launcher. Your play time and star
					progress will also automatically be sent to Romhacking.com. To add hacks to your playlists on Romhacking.com, use the
					<strong>Add to Playlist</strong> button on the hack page. You can create new playlists from your user profile page.
				</p>
				<sl-button variant="primary" tabindex="-1" aria-readonly="true" aria-disabled="true" inert>
					<sl-icon name="list-check" slot="prefix"></sl-icon>
					<span>Add to Playlist</span>
				</sl-button>
				<p>
					After enabling Romhacking.com integration in Parallel Launcher, a new button will appear to the left of the refresh button that
					will switch you to a Romhacking.com specific view. This view provides a better experience for your Romhacking.com hacks, but does
					not show any of your roms that are not on Romhacking.com.
				</p>
				<img style=${styleMap( imgStyle2 )} src="/assets/img/pl-rhdc-view.png" />
			</div>
		`;
	}

}
