import { css, CSSResultGroup, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { rhdcLinkStyles } from '../common-styles';

@customElement( 'rhdc-legal-page' )
export class RhdcLegalPage extends RhdcElement {

	static override get styles() : CSSResultGroup {
		return css`
			${rhdcLinkStyles}

			:host {
				display: block;
				margin: auto;
				min-width: min( 800px, 95vw );
				max-width: 800px;
				margin-bottom: var(--sl-spacing-x-large);
			}

			h1 {
				font-size: var(--sl-font-size-2x-large);
				font-weight: bold;
				margin: var(--sl-spacing-large) 0;
			}

			h2 {
				font-size: var(--sl-font-size-large);
				font-weight: bold;
				margin-top: var(--sl-spacing-medium);
				margin-bottom: var(--sl-spacing-x-small);
			}

			p {
				margin: var(--sl-spacing-x-small) 0;
			}
		`;
	}

	override render() : unknown {
		return html`
			<h1 id="legal">Legal</h1>
			<p>
				Romhacking.com is not affiliated in any way with Nintendo&#8482; or its subsidiaries or contracted
				developers. We do not host or provide links to ROMs and are not infringing upon any intellectual
				property. Our service only provides patch files.
			</p>
			<p>
				The romhacking.com website is licensed under version 3 of the GNU Affero General Public License.
				You can view the source code <a href="https://gitlab.com/mpharoah/rhdc-client" target="_blank">here</a>.
			</p>

			<h1 id="privacy">Privacy Policy</h1>
			<p>
				Romhacking.com is committed to protecting your privacy. This privacy policy outlines how we collect,
				use, and protect any personal information that you provide to us through our website.
			</p>

			<h2>Information we collect</h2>
			<p>
				Some interactions with this site may result in your IP address being stored. Additonally, when you
				register an account, you will be asked to provide an email address. You may also choose to provide
				additonal personal information to display on your user profile.
			</p>

			<h2>Use of personal information</h2>
			<p>
				Your IP address may be stored for the purpose of rate limiting and account security as well as
				tracking of unique downloads. Your location may be inferred from your IP address for the purpose of
				showing your login location on the Account Security page.
			</p>
			<p>
				Your email address is used for account verification and to provide a password reset service.
				Staff may also use this email to contact you if necessary. Your email will <strong>not</strong>
				be disclosed to anyone who is not a staff member of Romhacking.com.
			</p>
			<p>
				Any other information you voluntarily provide on your user profile will be visible to all
				site visitors. Providing this information is entirely optional.
			</p>

			<h2>Third-Party Service Providers</h2>
			<p>
				We use the hosting services provided by Amazon Web Services (AWS). We also use CloudFlare as a content
				delivery network. CloudFlare may log information about your use of our website such as your IP address
				and system configuration information such as your web browser and device type. We do not share your
				personal information with any other third parties except as required by law or with	your explicit
				consent.
			</p>
			<p>
				<a href="https://aws.amazon.com/privacy/">AWS Privacy Policy</a><br/>
				<a href="https://www.cloudflare.com/en-ca/privacypolicy/">CloudFlare Privacy Policy</a>
			</p>

			<h2>Security of personal information</h2>
			<p>
				The security of your personal information is important to us, and we take reasonable measures to protect
				your personal data from unauthorized access or disclosure. These measures include SSL data encryption to
				transmit your personal information as well as technical architectures and systems to prevent
				unauthorised third parties from accessing your personal information. However, no security precautions or
				systems can be completely secure. While we will do our best to protect your personal data, we cannot
				guarantee the absolute security of data transmitted to our site.
			</p>

			<h2>Changes to this privacy policy</h2>
			<p>
				We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy
				Policy periodically. Your continued use of the service after we post any modifications to the Privacy
				Policy on this page will constitute your binding acceptance of such changes.
			</p>
			<p>
				Any changes to this privacy policy will be announced via a news post on the front page of this website.
			</p>

		`;
	}

}
