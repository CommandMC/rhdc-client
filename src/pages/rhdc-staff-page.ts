import { css, CSSResultGroup, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { rhdcLinkStyles } from '../common-styles';
import '../components/common/rhdc-staff-member';

@customElement( 'rhdc-staff-page' )
export class RhdcStaffPage extends RhdcElement {

	static override get styles() : CSSResultGroup {
		return css`
			${rhdcLinkStyles}

			:host {
				display: block;
				max-width: min( 100vw, 1000px );
				margin: auto;
				padding: var(--sl-spacing-small) var(--sl-spacing-x-small);
			}

			h1 {
				font-size: var(--sl-font-size-2x-large);
				font-weight: bold;
			}

			h3 {
				font-size: var(--sl-font-size-x-large);
				margin: var(--sl-spacing-medium) 0 var(--sl-spacing-2x-small) 0;
			}

			div {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-x-small);
			}
		`;
	}

	override render() : unknown {
		return html`
			<h1>RHDC Staff</h1>

			<h3>Management</h3>
			<div>
				<rhdc-staff-member username="SimpleFlips">
					The founder and owner of the site. Pays to keep the servers running.
				</rhdc-staff-member>
				<rhdc-staff-member username="Victuracor">
					Helps manage various aspects of the project.
					Can be contacted at <a href="mailto:Victuracor@Trinartia.com">Victuracor@Trinartia.com</a> or at <b>Victuracor#7153</b> on Discord for urgent inqueries.
				</rhdc-staff-member>
			</div>

			<h3>Active Developers</h3>
			<div>
				<rhdc-staff-member username="falcobuster" alias="Matt Pharoah">
					Developer, maintainer, and designer of the new and current Romhacking.com server and website.
					Also created and maintains Parallel Launcher.
				</rhdc-staff-member>
			</div>

			<h3>Other Developers</h3>
			<div>
				<rhdc-staff-member username="WaveParadigm" alias="Adam">
					Primary developer of the original server. Also set up the AWS infrastructure.
				</rhdc-staff-member>
				<rhdc-staff-member username="Bosozoku" alias="Rob">
					Worked on the original website's design.
				</rhdc-staff-member>
				<rhdc-staff-member username="Serialbocks">
					Worked on various features of the original server and website.
				</rhdc-staff-member>
			</div>

			<h3>Special Thanks</h3>
			<rhdc-staff-member username="minikori"></rhdc-staff-member>
			<rhdc-staff-member username="Arthurtilly"></rhdc-staff-member>
			<rhdc-staff-member username="S_NDBB" alias="Gravis"></rhdc-staff-member>
			<rhdc-staff-member username="Rovert"></rhdc-staff-member>
			<rhdc-staff-member username="anonymous_moose"></rhdc-staff-member>
			<rhdc-staff-member username="axollyon"></rhdc-staff-member>
		`;
	}

}
