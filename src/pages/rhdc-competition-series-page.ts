import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { cache } from 'lit/directives/cache.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../rhdc-element';
import { Competition, CompetitionsApi, CompetitionSeries, CompetitionSeriesUpdate } from '../apis/competitions-api';
import { UsersApi } from '../apis/users-api';
import { FilesApi } from '../apis/files';
import { Nullable, Uuid, Weak } from '../util/types';
import { rhdcAsyncAppend } from '../util/lit-extensions';
import { AsyncPipe } from '../util/async-pipe';
import { Page } from '../apis/pagination';
import { getErrorMessage } from '../util/http-error-parser';
import { nextCycle } from '../util/delay';
import { toApiUrl } from '../util/url';
import { RhdcFileDropEvent } from '../events/rhdc-file-drop-event';
import { RhdcCompetitionCard } from '../components/competitions/rhdc-competition-card';
import { RhdcFileDropzone } from '../widgets/rhdc-file-dropzone';
import { RhdcTagInput } from '../widgets/rhdc-tag-input';
import { RhdcDialog } from '../components/common/rhdc-dialog';
import { RhdcMarkdownEditor } from '../widgets/rhdc-markdown-editor';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/avatar/avatar';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '../components/competitions/rhdc-competition-card';
import '../components/common/rhdc-username';
import '../components/common/rhdc-dialog';
import '../widgets/rhdc-markdown-editor';
import '../widgets/rhdc-file-dropzone';
import '../widgets/rhdc-tag-input';
import '../widgets/rhdc-load-more';
import '../widgets/rhdc-markdown';

@customElement( 'rhdc-competition-series-page' )
export class RhdcCompetitionSeriesPage extends RhdcElement {

	@property({ attribute: false })
	seriesSlug!: Uuid | string;

	@state()
	series: Nullable<CompetitionSeries> = null;

	@state()
	userList: Nullable<Readonly<string[]>> = null;

	@state()
	editing = false;

	@state()
	updating = false;

	@state()
	loading = false;

	@state()
	done = false;

	@state()
	deleteDisabled = false;

	@state()
	logoUrl = 'about:blank';

	@state()
	newLogoDataUrl = '';

	@query( 'rhdc-dialog' )
	deleteDialog!: RhdcDialog;

	#competitions: AsyncPipe<Competition> = new AsyncPipe<Competition>();
	#currentPage: Nullable<Page<Competition>> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				max-width: min( 100vw, calc( 800px + 2 * var(--sl-spacing-x-small) ) );
				margin: auto;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-small);
				box-sizing: border-box;
				margin-top: var(--sl-spacing-small);
			}

			.series-info {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			.series-info > div {
				display: flex;
				align-items: flex-start;
			}

			.series-info > div > sl-avatar {
				--size: 96px;
				flex-shrink: 0;
				margin: var(--sl-spacing-x-small);
			}

			.series-info > div > div {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
				flex-grow: 1;
			}

			.series-info .title {
				display: flex;
				align-items: center;
				font-size: var(--sl-font-size-2x-large);
				gap: var(--sl-spacing-2x-small);
			}

			.title > h3 {
				margin: 0;
				font-size: inherit;
				flex-grow: 1;
				text-overflow: ellipsis;
				overflow-x: hidden;
				flex-shrink: 1;
			}

			.hosts {
				display: flex;
				overflow-x: hidden;
				flex-shrink: 1;
				gap: 0.5ch;
			}

			.hosts > span {
				color: var(--sl-color-neutral-600);
			}

			.hosts > span:last-child {
				display: none;
			}

			.button-tray {
				display: flex;
				flex-direction: row-reverse;
				gap: var(--sl-spacing-x-small);
			}

			.spacer {
				flex-grow: 1;
			}

			.competitions {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			.series-editor {
				display: flex;
				align-items: flex-start;
				gap: var(--sl-spacing-small);
			}

			.series-editor > div:first-child {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-x-small);
			}

			sl-avatar {
				--size: 96px;
			}

			rhdc-file-dropzone {
				width: 96px;
				height: 96px;
				margin: 0;
				--pading: 0;
				font-size: 2rem;
			}

			.series-editor > div:last-child {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-small);
				flex-grow: 1;
			}

			sl-icon[hidden], sl-icon-button[hidden] {
				display: none;
			}

			sl-button.create {
				align-self: flex-start;
			}
		`;
	}

	override render() : unknown {
		let lazyLoader: unknown = nothing;
		if( !this.done ) {
			lazyLoader = html`<rhdc-load-more ?loading=${this.loading} @rhdc-load-more=${this.#loadMore}></rhdc-load-more>`;
		}

		let seriesInfo: HTMLTemplateResult;
		if( !this.series ) {
			seriesInfo = html`<sl-spinner></sl-spinner>`;
		} else if( this.editing ) {
			seriesInfo = this.#renderSeriesEditor();
		} else {
			seriesInfo = this.#renderSeriesInfo();
		}

		let createButton: unknown = nothing;
		if( this.series?.canManage ) {
			createButton = html`<sl-button variant="primary" class="create" @click=${this.navAction( `/competitions/series/${this.seriesSlug}/new` )}>New Competition</sl-button>`;
		}

		return html`
			${seriesInfo}

			<h2>Competitions</h2>
			<div class="competitions">
				${createButton}
				${rhdcAsyncAppend( this.#competitions, comp => html`<rhdc-competition-card id="${comp.competitionId as string}" no-icon .competition=${comp}></rhdc-competition-card>` )}
				${lazyLoader}
			</div>

			<rhdc-dialog label="Confirm Deletion" destructive ?disabled=${this.deleteDisabled} .confirmText=${'Delete'}>
				<p>
					Are you sure you want to delete this competition series? This will also delete all competitions in the series.
					To confirm that you want to delete this series, type the name of the series (<b>${this.series?.name}</b>) below.
				</p>
				<br/>
				<sl-input type="text"
					id="confirm-input"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="off"
					enterkeyhint="done"
					?spellcheck=${false}
					inputmode="text"
					@sl-input=${this.#onConfirmationInput}
				></sl-input>
			</rhdc-dialog>
		`;
	}

	#renderSeriesInfo() : HTMLTemplateResult {
		return html`
			<div class="series-info">
				<div>
					${cache( html`<sl-avatar shape="rounded" image="${this.logoUrl}" initials="${this.series!.name.substring( 0, 1 )}"></sl-avatar>` )}
					<div>
						<div class="title">
							<sl-icon name="eye-slash" ?hidden=${this.series!.public}></sl-icon>
							<sl-icon name="lock" ?hidden=${!this.series!.locked || !this.series!.canManage}></sl-icon>
							<h3>${this.series!.name}</h3>
							<sl-icon-button name="pencil" label="Edit" ?hidden=${!this.series!.canManage} @click=${() => this.editing = true}></sl-icon-button>
						</div>
						<div class="hosts">
							${map( this.series!.hosts, host => html`<rhdc-username .user=${host}></rhdc-username><span>&bull;</span>` )}
						</div>
					</div>
				</div>
				<rhdc-markdown trusted .markdown=${this.series!.description}></rhdc-markdown>
			</div>
		`;
	}

	#renderSeriesEditor() : HTMLTemplateResult {
		return html`
			<div class="series-editor">
				<div>
					${cache( html`<sl-avatar shape="rounded" image="${this.newLogoDataUrl}" initials="?"></sl-avatar>` )}
					<rhdc-file-dropzone id="logo-upload" accept="image/png" ?disabled=${this.updating} @rhdc-file-drop-event=${this.#uploadLogo}>
						<sl-icon name="upload"></sl-icon>
					</rhdc-file-dropzone>
				</div>
				<div>
					<sl-input type="text"
						id="title-input"
						.value=${this.series!.name}
						placeholder="Series Name"
						?readonly=${!this.series!.canEdit}
						?disabled=${this.updating}
						label="Title"
						required
						autocorrect="on"
						enterkeyhint="next"
						inputmode="text"
					></sl-input>
					<rhdc-tag-input
						id="hosts-input"
						.value=${this.series!.hosts.map( u => u.username )}
						?loading=${!this.userList}
						case-sensitive
						require-match
						placeholder="Enter username here. Tab to autocomplete."
						?disabled=${this.updating || !this.userList || !this.series!.canEdit}
						label="Hosts"
						required
						.autocomplete=${this.userList || []}
					></rhdc-tag-input>
					<rhdc-markdown-editor
						id="description-input"
						trusted
						.value=${this.series!.description}
						label="Description"
						placeholder="description"
						rows="8"
						?disabled=${this.updating}
						required
						spellcheck
						enterkeyhint="next"
					></rhdc-markdown-editor>
					<sl-checkbox
						id="prizes-checkbox"
						?disabled=${this.updating || !this.series!.canEdit}
						?checked=${this.series!.canOfferPrizes}
					>Can Offer Cash Prizes</sl-checkbox>
					<sl-checkbox
						id="locked-checkbox"
						?disabled=${this.updating || !this.series!.canEdit}
						?checked=${this.series!.locked}
					>Locked</sl-checkbox>
					<div class="button-tray">
						<sl-button variant="primary" ?loading=${this.updating} @click=${this.#saveChanges}>Save</sl-button>
						<sl-button variant="neutral" ?disabled=${this.updating} @click=${() => this.editing = false}>Cancel</sl-button>
						<div class="spacer"></div>
						<sl-button variant="danger" ?disabled=${this.updating} @click=${this.#deleteSeries} ?hidden=${!this.series!.canEdit}>Delete</sl-button>
					</div>
				</div>
			</div>
		`;
	}

	protected override firstUpdated( changedProperties: PropertyValues ) : void {
		super.firstUpdated( changedProperties );
		this.#focusLinkedCompetition();
	}

	protected override willUpdate( changedProperties: PropertyValues ): void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'seriesSlug' ) ) {
			this.#currentPage = null;
			this.#competitions = new AsyncPipe<Competition>();
			this.done = false;
			this.#fetchSeries();
		}
		if( changedProperties.has( 'series' ) ) {
			if( !this.series ) {
				this.logoUrl = 'about:blank';
			} else if( this.series.logo.secured ) {
				this.logoUrl = 'about:blank';
				FilesApi.getDownloadTokenAsync( this.series.logo.webHref ).then( dlToken => {
					this.logoUrl = toApiUrl( dlToken.href );
				});
			} else {
				this.logoUrl = toApiUrl( this.series.logo.directHref );
			}
		}
		if( changedProperties.has( 'editing' ) && this.editing ) {
			this.newLogoDataUrl = this.logoUrl;
			if( !this.userList ) this.#fetchUsernames();
		}
	}

	async #fetchSeries() : Promise<void> {
		try {
			this.series = await CompetitionsApi.getCompetitionSeriesAsync( this.seriesSlug );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load competition series info', getErrorMessage( exception ) );
		}
	}

	async #fetchUsernames() : Promise<void> {
		try {
			this.userList = await UsersApi.getAllUsernamesAsync();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load list of usernames. Competition series hosts cannot be updated.', getErrorMessage( exception ) );
		}
	}

	async #focusLinkedCompetition() : Promise<void> {
		const id = window.location.hash?.substring( 1 );
		if( !id || !/^[0-9a-f]{4}(?:[0-9a-f]{4}-){4}[0-9a-f]{12}$/i.test( id ) ) return;

		this.loading = true;
		while( !this.done ) {
			await this.#loadMore();
			this.loading = true;
			await this.updateComplete;
			await nextCycle();

			const competition = this.shadowRoot?.getElementById( id ) as Weak<RhdcCompetitionCard>;
			if( !competition ) continue;

			competition.expandAndFocus();
		}
		this.loading = false;
	}

	async #loadMore() : Promise<void> {
		this.loading = true;
		try {
			if( this.#currentPage ) {
				this.#currentPage = await this.#currentPage.nextAsync();
			} else {
				this.#currentPage = await CompetitionsApi.getCompetitionsPagedAsync( this.seriesSlug );
			}

			this.#competitions.pushMany( this.#currentPage.values );
			if( !this.#currentPage.hasNext ) {
				this.done = true;
				this.#competitions.end();
			}
		} catch( exception: unknown ) {
			this.toastError( 'Failed to load hack comments', getErrorMessage( exception ) );
			this.done = true;
			this.#competitions.end();
		} finally {
			this.loading = false;
		}
	}

	async #uploadLogo( event: RhdcFileDropEvent ) : Promise<void> {
		try {
			await CompetitionsApi.validateCompetitionSeriesLogoAsync( event.detail.file );
			this.newLogoDataUrl = await new Promise<string>( resolve => {
				const fileReader = new FileReader();
				fileReader.readAsDataURL( event.detail.file );
				fileReader.addEventListener( 'loadend', () => {
					resolve( fileReader.result as string );
				});
			});
		} catch( exception: unknown ) {
			this.toastError( 'Image rejected. Please upload a .PNG image that is at least 96x96 pixels.', getErrorMessage( exception ) );
		}
	}

	async #saveChanges() : Promise<void> {
		this.updating = true;
		try {
			const update: CompetitionSeriesUpdate = {
				description: this.#getInput<RhdcMarkdownEditor>( 'description-input' ).value
			};

			if( this.series!.canEdit ) {
				update.name = this.#getInput<SlInput>( 'title-input' ).value;
				update.canOfferPrizes = this.#getInput<SlCheckbox>( 'prizes-checkbox' ).checked;
				update.locked = this.#getInput<SlCheckbox>( 'locked-checkbox' ).checked;
				if( this.userList ) {
					update.hosts = this.#getInput<RhdcTagInput>( 'hosts-input' ).value;
				}
			}

			this.series = await CompetitionsApi.updateCompetitionSeriesAsync( this.seriesSlug, update );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to update series', getErrorMessage( exception ) );
			this.updating = false;
			return;
		}

		const newLogo = this.#getInput<RhdcFileDropzone>( 'logo-upload' ).value;
		if( !newLogo ) {
			this.updating = false;
			this.editing = false;
			return;
		}

		try {
			await CompetitionsApi.updateCompetitionSeriesLogoAsync( this.seriesSlug, newLogo );
		} catch( exception: unknown ) {
			this.toastWarn( 'Failed to update series logo', getErrorMessage( exception ) );
		} finally {
			this.updating = false;
			this.editing = false;
		}
	}

	#deleteSeries() : void {
		this.deleteDisabled = true;
		this.#getInput<SlInput>( 'confirm-input' ).value = '';
		this.deleteDialog.runAsync( async () => {
			try {
				await CompetitionsApi.deleteCompetitionSeriesAsync( this.seriesSlug );
				this.toastInfo( 'Competition series deleted' );
				this.navigate( '/competitions' );
			} catch( exception: unknown ) {
				this.toastError( 'Failed to delete series', getErrorMessage( exception) );
				throw exception;
			}
		});
	}

	#onConfirmationInput() : void {
		this.deleteDisabled = this.#getInput<SlInput>( 'confirm-input' ).value !== this.series!.name;
	}

	#getInput<T extends HTMLElement>( id: string ) : T {
		return this.shadowRoot?.getElementById( id ) as T;
	}

}
