import { css, CSSResultGroup, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { RhdcNavEvent } from '../events/rhdc-nav-event';
import { RhdcElement } from '../rhdc-element';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-404-page' )
export class Rhdc404Page extends RhdcElement {

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				align-items: center;
				justify-content: center;
			}

			.buttons {
				display: flex;
				justify-content: space-evenly;
			}

			h1 {
				margin: 0;
				font-size: 8rem;
				text-align: center;
				font-family: var(--sl-font-mono);
				color: var(--sl-color-neutral-200);
			}

			h2 {
				margin: 0;
				font-size: 2rem;
				text-align: center;
				font-family: var(--sl-font-sans);
				color: var(--sl-color-neutral-800);
			}

			img {
				margin: 1rem 0;
				width: 720px;
				height: 480px;
			}

			@media screen and (max-width: 720px) {
				img {
					width: 360px;
					height: 240px;
					image-rendering: pixelated;
				}
			}

		`;
	}
	
	override render() : unknown {
		return html`
			<div>
				<h1>404</h1>
				<h2>Page Not Found</h2>
				<img src="/assets/img/crash.png" />
				<div class="buttons">
					<sl-button variant="primary" @click=${this.#goHome}>
						<sl-icon slot="prefix" name="house-door"></sl-icon>
						<span>Go to Homepage</span>
					</sl-button>
					<sl-button variant="primary" @click=${this.#goBack} ?disabled=${window.history.length < 2}>
						<sl-icon slot="prefix" name="chevron-left"></sl-icon>
						<span>Go Back</span>
					</sl-button>
				</div>
			</div>
		`;
	}

	#goHome() : void {
		this.dispatchEvent( RhdcNavEvent.create( '/' ) );
	}

	#goBack() : void {
		window.history.back();
	}

}
