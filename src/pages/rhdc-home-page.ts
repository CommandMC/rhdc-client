import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';
import { rhdcMediaQuery } from '../decorators/media-query';
import { RhdcElement } from '../rhdc-element';
import { Auth } from '../auth/auth';
import { Uuid, Weak } from '../util/types';
import { delayAsync } from '../util/delay';
import { RhdcDialog } from '../components/common/rhdc-dialog';
import { getErrorMessage, tryGetRetryDelay } from '../util/http-error-parser';
import { HttpError } from '../apis/http-client';
import { RhdcRequireLoginEvent } from '../events/rhdc-login-event';
import { RhdcMfaInput } from '../widgets/rhdc-mfa-input';
import generatePassword from '../util/generate-password';
import SlDrawer from '@shoelace-style/shoelace/dist/components/drawer/drawer';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/drawer/drawer';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '../components/news/rhdc-news';
import '../components/sidebar/rhdc-sidebar';
import '../components/common/rhdc-dialog';
import '../widgets/rhdc-mfa-input';

async function rateLimit( apiCall: () => Promise<void> ) : Promise<void> {
	try {
		await apiCall();
	} catch( exception: unknown ) {
		if( exception instanceof HttpError && exception.problem?.status === 429 ) {
			const retryAfter = tryGetRetryDelay( exception );
			if( retryAfter ) {
				await delayAsync( retryAfter );
				await apiCall();
				return;
			}
		}

		throw exception;
	}
}

@customElement( 'rhdc-home-page' )
export class RhdcHomePage extends RhdcElement {

	@state()
	loading = false;

	@state()
	preloading = false;

	@state()
	requireMfaCode = false;

	@rhdcMediaQuery( 'only screen and (max-width: 900px)' )
	mobile!: boolean;

	@query( 'sl-drawer' )
	drawer?: SlDrawer;

	@query( 'rhdc-dialog' )
	passwordResetDialog!: RhdcDialog;

	@query( '#new-password' )
	newPasswordInput?: Weak<SlInput>;

	@query( '#confirm-password' )
	confirmPasswordInput?: Weak<SlInput>;

	@query( 'rhdc-mfa-input' )
	mfaInput?: Weak<RhdcMfaInput>;

	override connectedCallback(): void {
		super.connectedCallback();

		const emailToken = new URLSearchParams( window.location.search ).get( 'emailToken' );
		if( emailToken ) {
			this.#verifyEmail( emailToken );
			return;
		}
	}

	static override get styles() : CSSResultGroup {
		return css`
			.wrapper {
				padding: var(--sl-spacing-small);
				max-width: 1200px;
				margin: auto;
			}

			.wrapper.desktop {
				display: flex;
				gap: var(--sl-spacing-small);
			}

			rhdc-news {
				flex-grow: 1;
			}

			rhdc-sidebar {
				flex-grow: 0;
			}

			sl-drawer {
				--size: 100vw;
			}

			sl-drawer::part(base) {
				z-index: var(--sl-z-index-drawer);
			}

			.mobile > rhdc-news {
				margin-bottom: calc( var(--sl-input-height-medium) + 2 * var(--sl-spacing-small) );
			}

			.mobile > sl-button {
				position: fixed;
				right: var(--sl-spacing-small);
				bottom: var(--sl-spacing-small);
				z-index: calc( var(--sl-z-index-drawer) - 1 );
			}

			sl-button.close {
				margin: var(--sl-spacing-small) 0;
			}

			sl-input::part(form-control) {
				display: flex;
				align-items: center;
				gap: var(--sl-spacing-medium);
			}

			sl-input::part(form-control-label) {
				flex-shrink: 0;
				width: 10ch;
				text-align: right;
			}

			sl-input::part(form-control-input) {
				flex-grow: 1;
			}

			sl-input#confirm-password::part(form-control-label) {
				font-size: 0.8em;
				white-space: pre-line;
				width: 12.5ch;
				margin-top: -1em;
			}

			rhdc-dialog > div {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-x-small);
				align-items: stretch;
			}

			rhdc-dialog > div > sl-button {
				align-self: flex-start;
			}

			rhdc-dialog::part(base) {
				min-width: 500px;
			}

			sl-spinner {
				align-self: center;
				margin: var(--sl-spacing-medium) 0;
			}

			p {
				margin-top: 0;
			}

			rhdc-mfa-input[hidden] {
				display: none;
			}
		`;
	}
	
	override render() : unknown {
		let sidebar: HTMLTemplateResult;
		if( this.mobile ) {
			sidebar = html`
				<sl-button variant="primary" size="medium" circle @click=${this.#openSidebar}>
					<sl-icon name="chevron-left" label="show sidebar"></sl-icon>
				</sl-button>
				<sl-drawer contained no-header label="sidebar">
					<rhdc-sidebar></rhdc-sidebar>
					<sl-button variant="primary" class="close" circle @click=${this.#closeSidebar}>
						<sl-icon name="chevron-right" label="close"></sl-icon>
					</sl-button>
				</sl-drawer>
			`;
		} else {
			sidebar = html`<rhdc-sidebar></rhdc-sidebar>`;
		}

		let passwordResetDialogBody: HTMLTemplateResult;
		if( this.preloading ) {
			passwordResetDialogBody = html`<sl-spinner></sl-spinner>`;
		} else {
			passwordResetDialogBody = html`
				<p>Enter your new password.</p>
				<sl-input type="password"
					id="new-password"
					label="Password"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="new-password"
					inputmode="text"
					minlength="8"
					password-toggle
					?disabled=${this.loading}
				></sl-input>
				<sl-input type="password"
					id="confirm-password"
					label="${'Verify\nPassword'}"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="off"
					inputmode="text"
					minlength="8"
					password-toggle
					?disabled=${this.loading}
				></sl-input>
				<sl-button @click=${this.#generatePassword} ?disabled=${this.loading}>Generate Password</sl-button>
				<rhdc-mfa-input ?hidden=${!this.requireMfaCode} ?disabled=${this.loading}></rhdc-mfa-input>
			`;
		}

		return html`
			<div class="wrapper ${this.mobile ? 'mobile' : 'desktop'}">
				<rhdc-news></rhdc-news>
				${sidebar}
			</div>
			<rhdc-dialog label="Password Reset" persistence="strong" .confirmText=${'Reset Password'} ?disabled=${this.preloading}>
				<div>${passwordResetDialogBody}</div>
			</rhdc-dialog>
		`;
	}

	protected override firstUpdated( changedProperties: PropertyValues ): void {
		super.firstUpdated( changedProperties );

		const passwordResetToken = new URLSearchParams( window.location.search ).get( 'passwordReset' );
		if( passwordResetToken ) {
			this.#resetPassword( passwordResetToken as Uuid );
		}
	}

	#openSidebar() : void {
		this.drawer?.show();
	}

	#closeSidebar() : void {
		this.drawer?.hide();
	}

	async #verifyEmail( emailToken: string ) : Promise<void> {
		try {
			await Auth.verifyEmail( emailToken );
		} catch( errorMessage: unknown ) {
			this.toastError( 'Email Verification Failure', errorMessage as string );
			return;
		}
	}

	async #resetPassword( resetToken: Uuid ) : Promise<void> {
		this.requireMfaCode = false;
		this.passwordResetDialog.runAsync( async () => {
			const newPasswordInput = this.newPasswordInput!;
			const confirmPasswordInput = this.confirmPasswordInput!;
			this.mfaInput?.clear();

			if( !newPasswordInput.reportValidity() || !confirmPasswordInput.reportValidity() ) {
				throw false;
			}

			if( newPasswordInput.value !== confirmPasswordInput.value ) {
				confirmPasswordInput.setCustomValidity( 'Passwords do not match' );
				confirmPasswordInput.focus();
				throw false;
			}

			this.loading = true;
			try {
				if( this.requireMfaCode ) {
					await rateLimit( () => Auth.resetPasswordAsync( newPasswordInput.value, resetToken, this.mfaInput?.value || 0 ).then() );
				} else {
					let needsMfaCode = false;
					await rateLimit( async () => {
						needsMfaCode = !await Auth.resetPasswordAsync( newPasswordInput.value, resetToken );
					});

					if( needsMfaCode ) {
						this.requireMfaCode = true;
						this.toastInfo( 'Additional Authorization Required', 'Please enter your 6-digit authorization code.' );
						await this.updateComplete;
						this.mfaInput?.focus();
						throw false;
					}
				}
				
				this.toastSuccess( 'Password Reset', 'You may now login with your new password.' );
				this.dispatchEvent( RhdcRequireLoginEvent.create( null ) );
				window.history.replaceState( null, '', window.location.origin + window.location.pathname );
			} catch( exception: unknown ) {
				this.toastError( 'Password reset failed', getErrorMessage( exception ) );
				throw exception;
			} finally {
				this.loading = false;
			}
		});

		this.preloading = true;
		try {
			await rateLimit( () => Auth.validatePasswordResetAsync( resetToken ) );
		} catch{
			this.toastError( 'Password reset link is expired or invalid.' );
			this.passwordResetDialog.close();
		} finally {
			this.preloading = false;
		}
	}

	#generatePassword() : void {
		const passwordInput = this.newPasswordInput;
		if( !passwordInput ) return;
		passwordInput.value = generatePassword();
		passwordInput.passwordVisible = true;
		passwordInput.focus();
		this.confirmPasswordInput!.value = '';
	}

}
