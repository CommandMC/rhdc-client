import { css, CSSResultGroup, html } from 'lit';
import { customElement, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { Uuid, Nullable } from '../util/types';
import { rhdcUserContext, UserAuth } from '../decorators/user-context';
import { AccountSecurityApi, SecurityCredentials } from '../apis/account-security-api';
import { Auth } from '../auth/auth';
import { RhdcMfaInput } from '../widgets/rhdc-mfa-input';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '../components/security/rhdc-account-security-password';
import '../components/security/rhdc-account-security-mfa';
import '../components/security/rhdc-account-security-logins';
import '../widgets/rhdc-mfa-input';

@customElement( 'rhdc-account-security-page' )
export class RhdcAccountSecurityPage extends RhdcElement {

	@state()
	accountSecurityToken: Nullable<Uuid> = null;

	@state()
	timedOut = false;

	@state()
	loading = false;

	@query( 'sl-input' )
	passwordInput!: SlInput;

	@query( 'rhdc-mfa-input' )
	mfaInput!: RhdcMfaInput;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	#timeoutHandle = 0;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: flex-start;
				max-width: min( 100vw, calc( 800px + 2 * var(--sl-spacing-x-small) ) );
				margin: auto;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-small);
				box-sizing: border-box;
				margin-top: var(--sl-spacing-small);
			}

			h1 {
				font-size: var(--sl-font-size-2x-large);
				font-weight: normal;
				margin: 0;
			}

			rhdc-mfa-input[hidden] {
				display: none;
			}
		`;
	}

	override render() : unknown {
		if( !this.currentUser ) {
			return html`
				<h1>Access Denied</h1>
				<p>You must be logged in to view this page</p>
			`;
		}

		if( !this.accountSecurityToken ) {
			const headerText = this.timedOut ?
				'Your account security session has expired. Please re-enter your credentials to continue.' :
				'This is a secured page. Please verify your identity by re-entering your credentials.';
			
			return html`
				<h1>Verify Your Identity</h1>
				<p>${headerText}</p>

				<sl-input type="password"
					label="Password"
					?disabled=${this.loading}
					password-toggle
					required
					minlength="8"
					autocapitalize="off"
					autocorrect="off"
					autocomplete="current-password"
					autofocus
					enterkeyhint="${this.currentUser.mfaEnabled ? 'next' : 'send'}"
					?spellcheck=${false}
					inputmode="text"
					@keydown=${this.#passwordKeyDown}
				></sl-input>

				<rhdc-mfa-input
					?disabled=${this.loading}
					?hidden=${!this.currentUser.mfaEnabled}
					@rhdc-submit=${this.#requestAccess}
				></rhdc-mfa-input>

				<sl-button variant="primary" ?loading=${this.loading} @click=${this.#requestAccess}>Submit</sl-button>
			`;
		}

		return html`
			<h1 id="password">Change Password</h1>
			<rhdc-account-security-password .accountSecurityToken=${this.accountSecurityToken}></rhdc-account-security-password>

			<sl-divider></sl-divider>

			<h1 id="mfa">Multi-Factor Authentication</h1>
			<rhdc-account-security-mfa .accountSecurityToken=${this.accountSecurityToken} .userContext=${this.currentUser}></rhdc-account-security-mfa>

			<sl-divider></sl-divider>

			<h1 id="logins">Active Logins</h1>
			<rhdc-account-security-logins .accountSecurityToken=${this.accountSecurityToken} .userContext=${this.currentUser}></rhdc-account-security-logins>
		`;
	}

	override connectedCallback() : void {
		super.connectedCallback();
		this.#timeoutHandle = window.setTimeout( this.#timeout.bind( this ), 20 * 60 * 1000 );
	}

	override disconnectedCallback() : void {
		super.disconnectedCallback();
		window.clearTimeout( this.#timeoutHandle );
		if( this.accountSecurityToken ) {
			AccountSecurityApi.revokePageAccessAsync( this.accountSecurityToken );
		}
	}

	#timeout() : void {
		this.timedOut = true;
		this.accountSecurityToken = null;
	}

	#passwordKeyDown( event: KeyboardEvent ) : void {
		if( event.key !== 'Enter' || !this.currentUser ) return;
		if( this.currentUser.mfaEnabled ) {
			this.mfaInput.focus();
		} else {
			this.#requestAccess();
		}
	}

	async #requestAccess() : Promise<void> {
		if( !this.currentUser ) return;

		if( !this.passwordInput.reportValidity() ) return;
		if( this.currentUser.mfaEnabled && !this.mfaInput.reportValidity() ) return;

		const credentials: SecurityCredentials = { password: this.passwordInput.value };
		if( this.currentUser.mfaEnabled ) {
			credentials.mfaCode = this.mfaInput.value;
		}

		this.loading = true;
		try {
			this.accountSecurityToken = await AccountSecurityApi.requestPageAccessAsync( credentials );
		} catch{
			await Auth.initAsync();
			if( this.currentUser ) {
				this.toastError( 'Access Denied', 'Check that your credentials are correct and try again.' );
			} else {
				this.toastError( 'Access Denied', 'You have been logged out due to repeated authentication failures.' );
			}
		} finally {
			this.loading = false;
		}

	}

}
