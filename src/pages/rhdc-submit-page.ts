import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { state } from 'lit/decorators.js';
import { customElement } from 'lit/decorators.js';
import { Competition, CompetitionsApi } from '../apis/competitions-api';
import { HackSubmissionApi, HackSubmissionSession } from '../apis/hack-submission-api';
import { Role } from '../auth/roles';
import { rhdcUserContext, UserAuth } from '../decorators/user-context';
import { RhdcHackSubmissionUpdatedEvent } from '../events/rhdc-hack-submission-updated-event';
import { RhdcElement } from '../rhdc-element';
import { RhdcSubmissionComponent } from '../components/hacks/submission/rhdc-submission-component';
import { ConstArray, Nullable } from '../util/types';
import '../components/hacks/submission/rhdc-submission-competition';
import '../components/hacks/submission/rhdc-submission-patch';
import '../components/hacks/submission/rhdc-submission-metadata';
import '../components/hacks/submission/rhdc-submission-emulation';
import '../components/hacks/submission/rhdc-submission-media';
import '../components/hacks/submission/rhdc-submission-layout';
import '../components/hacks/submission/rhdc-submission-confirm';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/breadcrumb/breadcrumb';
import '@shoelace-style/shoelace/dist/components/breadcrumb-item/breadcrumb-item';
import '@shoelace-style/shoelace/dist/components/divider/divider';

@customElement( 'rhdc-submit-page' )
export class RhdcSubmitPage extends RhdcElement {

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	@state()
	session: Nullable<HackSubmissionSession> = null;

	@state()
	competitions: ConstArray<Competition> = [];

	@state()
	step = 0;

	@state()
	loading = true;

	#saving = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				padding: var(--sl-spacing-small);
				max-width: 1200px;
				margin: auto;

				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			sl-spinner {
				margin-top: var(--sl-spacing-4x-large);
				align-self: center;
				font-size: 256px;
				--track-width: 16px;
			}
		`;
	}

	override render() : unknown {
		if( this.loading || !this.session ) {
			return html`<sl-spinner></sl-spinner>`;
		}

		if( this.step < 0 ) {
			console.error( `Invalid step value ${this.step}` );
			this.step = 0;
		} else if( this.step > 6 ) {
			console.error( `Invalid step value ${this.step}` );
			this.step = 6;
		}

		const steps: HTMLTemplateResult[] = [];
		switch( this.step ) {
			case 6: steps.push( html`<sl-breadcrumb-item>Confirmation</sl-breadcrumb-item>` );
			case 5: steps.push( html`<sl-breadcrumb-item @click=${this.#goto.bind( this, 5 )}>Star Layout</sl-breadcrumb-item>` );
			case 4: steps.push( html`<sl-breadcrumb-item @click=${this.#goto.bind( this, 4 )}>Media</sl-breadcrumb-item>` );
			case 3: steps.push( html`<sl-breadcrumb-item @click=${this.#goto.bind( this, 3 )}>Emulation Info</sl-breadcrumb-item>` );
			case 2: steps.push( html`<sl-breadcrumb-item @click=${this.#goto.bind( this, 2 )}>Metadata</sl-breadcrumb-item>` );
			default: steps.push( html`<sl-breadcrumb-item @click=${this.#goto.bind( this, 1 )}>Patch Upload</sl-breadcrumb-item>` ); break;
		}

		if( this.competitions.length > 0 ) {
			steps.push( html`<sl-breadcrumb-item @click=${this.#goto.bind( this, 0 )}>Competition</sl-breadcrumb-item>` );
		}

		steps.reverse();

		return html`
			<sl-breadcrumb>${steps}</sl-breadcrumb>
			<sl-divider></sl-divider>
			<rhdc-submission-competition
				id="step0"
				?hidden=${this.step !== 0}
				.submission=${this.session!}
				.competitions=${this.competitions}
				@rhdc-hack-submission-updated-event=${this.#onSubmissionUpdate}
				@rhdc-submission-next-event=${() => this.step++}
			></rhdc-submission-competition>
			<rhdc-submission-patch
				id="step1"
				?hidden=${this.step !== 1}
				.submission=${this.session!}
				.showPrevButton=${this.competitions.length > 0}
				@rhdc-hack-submission-updated-event=${this.#onSubmissionUpdate}
				@rhdc-submission-prev-event=${() => this.step--}
				@rhdc-submission-next-event=${() => this.step++}
			></rhdc-submission-patch>
			<rhdc-submission-metadata
				id="step2"
				?hidden=${this.step !== 2}
				.submission=${this.session!}
				@rhdc-hack-submission-updated-event=${this.#onSubmissionUpdate}
				@rhdc-submission-prev-event=${() => this.step--}
				@rhdc-submission-next-event=${() => this.step++}
			></rhdc-submission-metadata>
			<rhdc-submission-emulation
				id="step3"
				?hidden=${this.step !== 3}
				.submission=${this.session}
				@rhdc-hack-submission-updated-event=${this.#onSubmissionUpdate}
				@rhdc-submission-prev-event=${() => this.step--}
				@rhdc-submission-next-event=${() => this.step++}
			></rhdc-submission-emulation>
			<rhdc-submission-media
				id="step4"
				?hidden=${this.step !== 4}
				.submission=${this.session}
				@rhdc-hack-submission-updated-event=${this.#onSubmissionUpdate}
				@rhdc-submission-prev-event=${() => this.step--}
				@rhdc-submission-next-event=${() => this.step++}
			></rhdc-submission-media>
			<rhdc-submission-layout
				id="step5"
				?hidden=${this.step !== 5}
				.submission=${this.session}
				@rhdc-hack-submission-updated-event=${this.#onSubmissionUpdate}
				@rhdc-submission-prev-event=${() => this.step--}
				@rhdc-submission-next-event=${() => this.step++}
			></rhdc-submission-layout>
			<rhdc-submission-confirm
				id="step6"
				?hidden=${this.step !== 6}
				.submission=${this.session}
				@rhdc-submission-prev-event=${() => this.step--}
			></rhdc-submission-confirm>
		`;
	}

	override connectedCallback() : void {
		super.connectedCallback();
		if( !this.currentUser ) {
			this.#initialize();
		}
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'currentUser' ) ) {
			this.#initialize();
		}
	}

	async #initialize() : Promise<void> {
		this.loading = true;

		if( !this.currentUser ) {
			this.toastError( 'Not Authorized', 'You must be logged in to submit a hack' );
			this.navigate( '/' );
			return;
		}

		if( this.currentUser.role === Role.Banned ) {
			this.toastError( 'Not Authorized', 'You may not submit hacks because your account has been banned.' );
			this.navigate( '/' );
			return;
		}

		if( this.currentUser.role === Role.Unverified ) {
			this.toastError( 'Not Authorized', 'You must verify your e-mail address before you can submit hacks.' );
			this.navigate( '/' );
			return;
		}

		this.competitions = await CompetitionsApi.getActiveCompetitionsAsync();
		this.step = (this.competitions.length > 0) ? 0 : 1;

		this.session = null;
		this.session = await HackSubmissionApi.getSessionAsync();
		if( this.session.inProgess ) {
			const resume = await this.confirmAsync(
				'Resume last session?',
				'You left the page while a previous submission was in progress. Would you like to restore it or start fresh with a new submission?',
				'Resume',
				'Start Over'
			);

			if( resume ) {
				if( this.session.hackFile ) {
					if( !this.session.metadata ) {
						this.step = 2;
					} else if( !this.session.emulation ) {
						this.step = 3;
					} else if( !this.session.media ) {
						this.step = 4;
					} else if( !this.session.starLayout ) {
						this.step = 5;
					} else {
						this.step = 6;
					}
				} else if( this.session.competition ) {
					this.step = 1;
				}
			} else {
				this.session = null;
				this.session = await HackSubmissionApi.resetSessionAsync();
			}
		}

		this.loading = false;
	}

	#onSubmissionUpdate( event : RhdcHackSubmissionUpdatedEvent ) : void {
		this.session = event.detail.session;
	}

	async #goto( step: number ) : Promise<void> {
		if( this.#saving ) return;

		this.#saving = true;
		await (this.shadowRoot?.getElementById( `step${step}` ) as Nullable<RhdcSubmissionComponent>)?.trySaveAsync();
		this.#saving = false;

		this.step = step;
	}

}
