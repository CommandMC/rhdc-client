import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { CommentActivity, PlayTimeActivity, ProgressActivity, UserActivity } from '../../apis/users-api';
import { RhdcElement } from '../../rhdc-element';
import { ConstArray, ConstRef, Nullable } from '../../util/types';
import { AlertFactory } from '../../util/alert-factory';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/relative-time/relative-time';
import '@shoelace-style/shoelace/dist/components/details/details';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../../widgets/rhdc-link';

@customElement( 'rhdc-user-recent-activity' )
export class RhdcUserRecentActivity extends RhdcElement {

	@property({ attribute: false })
	activity!: Nullable<UserActivity>;

	@property({ attribute: 'loading', type: Boolean })
	loading = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: row;
				align-items: flex-start;
				gap: var(--sl-spacing-2x-small);
			}

			h3 {
				margin: 0;
				padding: 0;
			}

			.column {
				flex-grow: 1;
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
				min-width: 375px;
			}

			.right {
				text-align: end;
			}

			.right > div {
				display: flex;
				align-items: center;
				gap: 0.5ch;
			}

			.no-activity {
				color: var(--sl-color-neutral-700);
			}

			sl-divider {
				flex-grow: 0;
			}

			sl-spinner {
				font-size: 2rem;
			}

			table {
				border-collapse: collapse;
			}

			td {
				padding: var(--sl-spacing-2x-small) var(--sl-spacing-3x-small);
			}

			td:first-child {
				width: 99%;
			}

			td:last-child {
				white-space: nowrap;
			}

			sl-relative-time, .unknown-time {
				color: var(--sl-color-neutral-600);
			}

			sl-icon[name="check-lg"] {
				color: var(--sl-color-success-500);
			}

			sl-icon[name="star-fill"] {
				color: var(--sl-color-amber-500);
			}

			.comment {
				white-space: pre-wrap;
			}

			@media only screen and (max-width: 1188px) and (min-width: 782px) {
				:host {
					flex-wrap: wrap;
				}

				.column {
					max-width: calc( 50vw ) - 16px;
				}

				sl-divider:last-of-type {
					display: none;
				}
			}

			@media only screen and (max-width: 781px) {
				:host {
					flex-direction: column;
					align-items: stretch;
				}

				.column {
					min-width: unset;
				}

				sl-spinner {
					align-self: flex-start;
				}

				sl-divider {
					display: none;
				}
			}
		`;
	}

	override render() : unknown {
		if( this.loading ) {
			return html`<sl-spinner></sl-spinner>`;
		}

		if( !this.activity ) {
			return AlertFactory.create({
				type: 'danger',
				message: 'Failed to load recent user activity'
			});
		}

		return html`
			<div class="column">
				<h3>Recently Played</h3>
				${this.#renderRecentlyPlayed( this.activity.recentlyPlayed )}
			</div>
			<sl-divider></sl-divider>
			<div class="column">
				<h3>Recent Progress</h3>
				${this.#renderRecentProgress( this.activity.recentProgress )}
			</div>
			<sl-divider></sl-divider>
			<div class="column">
				<h3>Recent Comments</h3>
				${this.#renderRecentComments( this.activity.recentComments )}
			</div>
		`;
	}

	#renderRecentlyPlayed( activity: ConstArray<PlayTimeActivity> ) : HTMLTemplateResult {
		if( activity.length === 0 ) {
			return html`<span class="no-activity">This user is not tracking their play time.</span>`;
		}

		return html`
			<table>
				<tbody>
					${map( activity, this.#renderRecentlyPlayedItem.bind( this ) )}
				</tbody>
			</table>
		`;
	}

	#renderRecentProgress( activity: ConstArray<ProgressActivity> ) : HTMLTemplateResult {
		if( activity.length === 0 ) {
			return html`<span class="no-activity">This user has not collected any stars.</span>`;
		}

		return html`
			<table>
				<tbody>
					${map( activity, this.#renderRecentProgressItem.bind( this ) )}
				</tbody>
			</table>
		`;
	}

	#renderRecentComments( activity: ConstArray<CommentActivity> ) : HTMLTemplateResult {
		if( activity.length === 0 ) {
			return html`<span class="no-activity">This user has not left any comments.</span>`;
		}

		return html`${map( activity, this.#renderRecentCommentItem.bind( this ) )}`;
	}

	#renderRecentlyPlayedItem( activity: Readonly<PlayTimeActivity> ) : HTMLTemplateResult {
		return html`
			<tr>
				<td>
					${this.#renderTimestamp( activity.timestamp )}<br/>
					<rhdc-link href=${activity.hackHref}>${activity.hackName}</rhdc-link>
				</td>
				<td>${activity.formatPlayTime()}</td>
			</tr>
		`;
	}

	#renderRecentProgressItem( activity: Readonly<ProgressActivity> ) : HTMLTemplateResult {
		const progress = activity.starsTotal <= 1 ?
			html`<sl-icon name="check-lg"></sl-icon>` :
			html`
				<div>
					<span>${activity.starsCollected} / ${activity.starsTotal}</span>
					<sl-icon name="star-fill"></sl-icon>
				</div>
			`;

		return html`
			<tr>
				<td>
					${this.#renderTimestamp( activity.timestamp )}<br/>
					<rhdc-link href=${activity.hackHref}>${activity.hackName}</rhdc-link>
				</td>
				<td class="right">${progress}</td>
			</tr>
		`;
	}

	#renderRecentCommentItem( activity: Readonly<CommentActivity> ) : HTMLTemplateResult {
		const summary = activity.isReply ?
			html`Replied to a comment on <rhdc-link href=${activity.hackHref}>${activity.hackName}</rhdc-link>` :
			html`Commented on <rhdc-link href=${activity.hackHref}>${activity.hackName}</rhdc-link>`;

		return html`
			<sl-details>
				<div slot="summary">
					${this.#renderTimestamp( activity.timestamp )}<br/>
					<span>${summary}</span>
				</div>
				<span class="comment">${activity.message}</span>
			</sl-details>
		`;
	}

	#renderTimestamp( date: ConstRef<Date> ) : HTMLTemplateResult {
		if( date.getTime() > 0 ) {
			return html`<sl-relative-time .date=${date as Date}></sl-relative-time>`;
		} else {
			return html`<span class="unknown-time">Unknown</span>`;
		}
	}

}
