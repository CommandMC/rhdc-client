import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, query, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { HacksApi, Hack, HackSortField, MatureFilter } from '../../apis/hacks-api';
import { Nullable, UserId } from '../../util/types';
import { RhdcHackSortBar } from '../hacks/rhdc-hack-sort-bar';
import { rhdcPageState, IRhdcPageState } from '../../decorators/page-state';
import '../../decorators/page-state';
import '../hacks/rhdc-hack-sort-bar';
import '../hacks/rhdc-hack-card';

@customElement( 'rhdc-user-hacks' )
export class RhdcUserHacks extends RhdcElement {

	@property({ attribute: false })
	userSlug!: UserId | string;

	@query( 'rhdc-hack-sort-bar' )
	sortBar!: Nullable<RhdcHackSortBar>;

	@query( '.hacks' )
	scrollArea!: Nullable<HTMLDivElement>;

	@rhdcPageState( (hack: Hack) => html`<rhdc-hack-card .hack=${hack}></rhdc-hack-card>` )
	pageState!: IRhdcPageState;

	#minHeight = 0;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				padding: var(--sl-spacing-small) 0;
				box-sizing: border-box;
			}

			.hacks {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
				max-height: 600px;
				overflow-y: auto;
				position: relative;
				padding-right: var(--sl-spacing-x-small);
				margin-top: var(--sl-spacing-medium);
			}

			p {
				margin: 0;
				color: var(--sl-color-neutral-600);
			}

			h3 {
				margin: 0;
				padding: 0;
			}
		`;
	}

	override render() : unknown {
		if( this.pageState.done && !this.pageState.error && this.pageState.count === 0 ) {
			return html`
				<h3>Published Hacks</h3>
				<p>This user has not published any hacks.</p>
			`;
		}

		/* eslint-disable indent */
		return html`
			<h3>Published Hacks</h3>
			<rhdc-hack-sort-bar @rhdc-change=${this.#getHacks}></rhdc-hack-sort-bar>
			<div class="hacks" @scroll=${this.#updateScrollBorder}>
				${this.pageState.render({
					renderEmpty: null,
					renderError: 'Failed to load hacks.'
				})}
			</div>
		`;
		/* eslint-enable indent */
	}

	protected override willUpdate( changedProperties: PropertyValues ): void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'userSlug' ) ) {
			this.#getHacks();
		}
	}

	async #getHacks() : Promise<void> {
		this.#minHeight = 0;
		this.pageState.reset( HacksApi.getUserHacksAsync(
			this.userSlug,
			this.sortBar ? this.sortBar.sortField : HackSortField.DateUploaded,
			this.sortBar ? this.sortBar.sortDescending : true,
			MatureFilter.Include,
		));
	}

	#updateScrollBorder() : void {
		if( this.clientHeight > this.#minHeight ) {
			this.#minHeight = this.clientHeight;
			this.style.minHeight = this.#minHeight + 'px';
		}

		if( !this.scrollArea ) return;

		if( this.scrollArea.clientHeight + this.scrollArea.scrollTop < this.scrollArea.scrollHeight ) {
			this.scrollArea.style.borderBottom = '1px dashed var(--sl-panel-border-color)';
		} else {
			this.scrollArea.style.borderBottom = 'unset';
		}
	}

}

