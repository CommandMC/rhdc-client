import { css, CSSResultGroup, html, HTMLTemplateResult, nothing } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { PlaylistsApi, Playlist } from '../../apis/playlists-api';
import { getErrorMessage } from '../../util/http-error-parser';
import { rhdcLinkStyles, rhdcIconButtonStyles } from '../../common-styles';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-playlist-link' )
export class RhdcPlaylistLink extends RhdcElement {

	@property({ attribute: false })
	playlist!: Readonly<Playlist>;

	@property({ attribute: 'movable', type: Boolean })
	movable = false;

	@state()
	loading = false;

	@state()
	editing = false;

	@query( 'sl-input' )
	renameInput!: SlInput;

	static override get styles() : CSSResultGroup {
		return css`
			${rhdcLinkStyles}
			${rhdcIconButtonStyles}

			:host {
				display: flex;
				gap: var(--sl-spacing-x-small);
				align-items: center;
				margin: var(--sl-spacing-3x-small) 0;
			}

			.content {
				display: flex;
				gap: 1ch;
				white-space: nowrap;
				align-items: center;
				cursor: default;
				flex-grow: 1;
			}

			sl-icon-button::part(base), sl-icon::part(base) {
				padding: var(--sl-spacing-3x-small);
			}

			sl-input {
				flex-grow: 1;
			}

			a {
				cursor: pointer;
			}

			sl-icon[hidden] {
				visibility: hidden;
				cursor: unset;
				opacity: 0;
			}

			sl-icon-button[name="eye"] {
				color: var(--rhdc-text-color);
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-icon name="grip-vertical" ?hidden=${!this.movable}></sl-icon>
			<div
				class="content"
				draggable="true"
				@dragstart=${this.#cancelEvent}
			>${this.#renderContent()}</div>
		`;
	}

	#renderContent() : HTMLTemplateResult {
		if( this.editing ) {
			return html`
				<sl-input
					type="text"
					placeholder="Playlist Name"
					required
					autocapitalize="on"
					autocorrect="on"
					autocomplete="off"
					autofocus
					enterkeyhint="done"
					inputmode="text"
					?disabled=${this.loading}
					.value=${this.playlist.name}
					@keydown=${this.#onInput}
				></sl-input>
				<sl-icon-button
					name="check"
					label="Save"
					?disabled=${this.loading}
					@click=${this.#renamePlaylist}
				></sl-icon-button>
				<sl-icon-button
					name="x"
					label="Cancel"
					?disabled=${this.loading}
					@click=${() => this.editing = false}
				></sl-icon-button>
			`;
		}

		let visibilityIcon: unknown = nothing;
		if( this.playlist.canEdit || !this.playlist.public ) {
			visibilityIcon = html`
				<sl-icon-button
					name=${this.playlist.public ? 'eye' : 'eye-slash'}
					label=${this.playlist.public ? 'Public' : 'Private'}
					?disabled=${this.loading || !this.playlist.canEdit}
					@click=${this.#toggleVisibility}
				></sl-icon-button>
			`;
		}

		let editButtons: unknown = nothing;
		if( this.playlist.canEdit ) {
			editButtons = html`
				<sl-icon-button
					name="pencil"
					label="Edit"
					?disabled=${this.loading}
					@click=${() => this.editing = true}
				></sl-icon-button>
				<sl-icon-button
					class="danger"
					name="trash"
					label="Delete"
					?disabled=${this.loading}
					@click=${this.#deletePlaylist}
				></sl-icon-button>
			`;
		}

		const playlistLink = (this.playlist.hackCount > 0) ?
			html`<a @click=${this.#playlistClicked}>${this.playlist.name}</a>` :
			html`<span>${this.playlist.name}</span>`;

		return html`
			${visibilityIcon}
			${playlistLink}
			<span>(${this.playlist.hackCount.toString()})</span>
			${editButtons}
		`;
	}

	async #toggleVisibility() : Promise<void> {
		this.loading = true;
		try {
			this.playlist = await PlaylistsApi.updatePlaylistAsync( this.playlist.name, {
				public: !this.playlist.public
			});
		} catch( exception: unknown ) {
			this.toastError( 'Failed to change playlist visibility', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #deletePlaylist() : Promise<void> {
		if( !await this.confirmAsync(
			'Confirm Delete',
			`Are you sure you want to delete the playlist "${this.playlist.name}"`,
			'Delete',
			'Cancel'
		)) return;

		this.loading = true;
		try {
			await PlaylistsApi.deletePlaylistAsync( this.playlist.name );
			this.dispatchEvent( new CustomEvent( 'rhdc-playlist-deleted' ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to delete playlist', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #renamePlaylist() : Promise<void> {
		if( !this.renameInput.reportValidity() ) return;

		this.loading = true;
		try {
			this.playlist = await PlaylistsApi.updatePlaylistAsync( this.playlist.name, {
				name: this.renameInput.value
			});

			this.editing = false;
			this.dispatchEvent( new CustomEvent( 'rhdc-playlist-renamed' ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to rename playlist', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	#playlistClicked() : void {
		this.dispatchEvent( new CustomEvent( 'rhdc-playlist-clicked' ) );
	}

	#onInput( event: KeyboardEvent ) : void {
		switch( event.key ) {
			case 'Enter':
			case 'Accept':
				this.#renamePlaylist();
				break;
			case 'Escape':
			case 'Esc':
			case 'Cancel':
				this.editing = false;
				break;
			default: return;
		}
	}

	#cancelEvent( event: Event ) : void {
		event.stopPropagation();
		event.preventDefault();
	}

}
