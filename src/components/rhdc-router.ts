import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { Uuid } from '../util/types';
import '../pages/rhdc-home-page';
import '../pages/rhdc-404-page';
import '../pages/rhdc-hacks-list-page';
import '../pages/rhdc-submit-page';
import '../pages/rhdc-hack-page';
import '../pages/rhdc-user-page';
import '../pages/rhdc-leaderboard-page';
import '../pages/rhdc-competitions-homepage';
import '../pages/rhdc-competition-series-create-page';
import '../pages/rhdc-competition-series-page';
import '../pages/rhdc-competition-create-page';
import '../pages/rhdc-competition-page';
import '../pages/rhdc-search-page';
import '../pages/rhdc-modqueue-page';
import '../pages/rhdc-staff-page';
import '../pages/rhdc-rules-page';
import '../pages/rhdc-legal-page';
import '../pages/rhdc-getting-started-page';
import '../pages/rhdc-account-security-page';

@customElement( 'rhdc-router' )
export class RhdcRouter extends RhdcElement {

	@property({ attribute: false })
	path: string = document.location.pathname;

	@property({ attribute: 'not-found', type: Boolean })
	notFound = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				position: relative;
			}

			* {
				box-sizing: border-box;
				width: 100%;
			}
		`;
	}

	override render() : unknown {
		if( this.notFound ) return html`<rhdc-404-page></rhdc-404-page>`;

		if( !this.path || this.path === '/' || this.path === '/index.html' ) {
			return html`<rhdc-home-page></rhdc-home-page>`;
		} else if( this.path === '/hacks' ) {
			return html`<rhdc-hacks-list-page></rhdc-hacks-list-page>`;
		} else if( this.path === '/submit' ) {
			return html`<rhdc-submit-page></rhdc-submit-page>`;
		} else if( this.path.startsWith( '/hack/' ) ) {
			const hackSlug = this.path.substring( '/hack/'.length );
			if( hackSlug.length > 0 ) {
				return html`<rhdc-hack-page .hackSlug=${hackSlug}></rhdc-hack-page>`;
			}
		} else if( this.path.startsWith( '/user/' ) ) {
			const userSlug = this.path.substring( '/user/'.length );
			if( userSlug.length > 0 ) {
				return html`<rhdc-user-page .userSlug=${userSlug}></rhdc-user-page>`;
			}
		} else if( this.path === '/leaderboard' ) {
			return html`<rhdc-leaderboard-page></rhdc-leaderboard-page>`;
		} else if( this.path === '/competitions' || this.path === '/competitions/series' ) {
			return html`<rhdc-competitions-homepage></rhdc-competitions-homepage>`;
		} else if( this.path === '/competitions/new-series' ) {
			return html`<rhdc-competition-series-create-page></rhdc-competition-series-create-page>`;
		} else if( this.path.startsWith( '/competitions/series/' ) ) {
			let series = this.path.substring( '/competitions/series/'.length );
			if( series ) {
				if( series.endsWith( '/new' ) ) {
					series = series.substring( 0, series.length - '/new'.length );
					return html`<rhdc-competition-create-page .seriesSlug=${series}></rhdc-competition-create-page>`;
				}
				return html`<rhdc-competition-series-page .seriesSlug=${series}></rhdc-competition-series-page>`;
			} else {
				return html`<rhdc-competitions-homepage></rhdc-competitions-homepage>`;
			}
		} else if( this.path.startsWith( '/competitions/' ) ) {
			const competitionId = this.path.substring( '/competitions/'.length );
			if( competitionId.length === 36 ) {
				return html`<rhdc-competition-page .competitionId=${competitionId as Uuid}></rhdc-competition-page>`;
			}
		} else if( this.path.startsWith( '/search/' ) ) {
			const searchTerm = decodeURIComponent( this.path.substring( '/search/'.length ) );
			if( searchTerm.trim() ) {
				return html`<rhdc-search-page .searchTerm=${searchTerm}></rhdc-search-page>`;
			}
		} else if( this.path === '/modqueue' ) {
			return html`<rhdc-modqueue-page></rhdc-modqueue-page>`;
		} else if( this.path === '/staff' || this.path === '/about' ) {
			return html`<rhdc-staff-page></rhdc-staff-page>`;
		} else if( this.path === '/rules' ) {
			return html`<rhdc-rules-page></rhdc-rules-page>`;
		} else if( this.path === '/legal' ) {
			return html`<rhdc-legal-page></rhdc-legal-page>`;
		} else if( this.path === '/gettingstarted' || this.path === '/howtopatch' ) {
			return html`<rhdc-getting-started-page></rhdc-getting-started-page>`;
		} else if( this.path === '/security' ) {
			return html`<rhdc-account-security-page></rhdc-account-security-page>`;
		}

		return html`<rhdc-404-page></rhdc-404-page>`;
	}

}
