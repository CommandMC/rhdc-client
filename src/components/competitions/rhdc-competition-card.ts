import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, query } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { Competition } from '../../apis/competitions-api';
import { formatLocalDate } from '../../util/formatting';
import { Weak } from '../../util/types';
import { toApiUrl } from '../../util/url';
import SlDetails from '@shoelace-style/shoelace/dist/components/details/details';
import '@shoelace-style/shoelace/dist/components/avatar/avatar';
import '@shoelace-style/shoelace/dist/components/badge/badge';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/details/details';
import '../../widgets/rhdc-markdown';
import '../../widgets/rhdc-link';

@customElement( 'rhdc-competition-card' )
export class RhdcCompetitionCard extends RhdcElement {

	@property({ attribute: false })
	competition!: Competition;

	@property({ attribute: 'no-icon', type: Boolean })
	noIcon = false;

	@query( 'sl-details' )
	base: Weak<SlDetails>;

	#expansionPending = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			sl-details > div {
				display: flex;
				width: 100%;
				gap: var(--sl-spacing-small);
				align-items: flex-start;
			}

			.header {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
				margin-right: var(--sl-spacing-small);
				flex-grow: 1;
			}

			.header > div {
				display: flex;
				align-items: center;
				justify-content: space-between;
				gap: var(--sl-spacing-large);
			}

			.header > div:first-child > span {
				color: var(--sl-color-neutral-600);
			}

			sl-details > p {
				margin: 0;
				white-space: pre-line;
			}

			.title {
				font-size: var(--sl-font-size-x-large);
			}

			sl-avatar {
				--size: 4rem;
				flex-grow: 0;
			}
		`;
	}

	protected override render() : unknown {
		const now = new Date();

		let badge: HTMLTemplateResult;
		if( this.competition.startDate > now ) {
			badge = html`<sl-badge variant="neutral" pill>Upcoming</sl-badge>`;
		} else if( this.competition.endDate > now ) {
			badge = html`<sl-badge variant="success" pill>Submissions Open</sl-badge>`;
		} else if( this.competition.published ) {
			badge = html`<sl-badge variant="primary" pill>Concluded</sl-badge>`;
		} else {
			badge = html`<sl-badge variant="warning" pill>Submissions Closed</sl-badge>`;
		}

		const title = (this.competition.published || this.competition.canEdit || this.competition.canPublish || this.competition.canJudge) ?
			html`<rhdc-link class="title" href=${`/competitions/${this.competition.competitionId}`}>${this.competition.name}</rhdc-link>` :
			html`<span class="title">${this.competition.name}</span>`;

		let submitButton: unknown = nothing;
		if( this.competition.canSubmit ) {
			submitButton = html`<sl-button variant="primary" @click=${this.navAction( `/submit#${this.competition.competitionId}` )}>Submit Hack</sl-button>`;
		}

		let submissionCount: unknown = nothing;
		if( this.competition.submissionCount !== undefined ) {
			submissionCount = html`<div>${this.competition.submissionCount} submissions</div>`;
		}

		let seriesIcon: unknown = nothing;
		if( !this.noIcon ) {
			const logoUrl = toApiUrl( `/v3/competitions/series/${this.competition.seriesId}/logo` );
			seriesIcon = html`<sl-avatar shape="rounded" image="${logoUrl}" initials="?"></sl-avatar>`;
		}

		return html`
			<sl-details part="base">
				<div slot="summary">
					${seriesIcon}
					<div class="header">
						<div>
							<span>${formatLocalDate( this.competition.startDate )} to ${formatLocalDate( this.competition.endDate )}</span>
							${badge}
						</div>
						<div>
							${title}
							${submitButton}
						</div>
						${submissionCount}
					</div>
				</div>
				<rhdc-markdown trusted .markdown=${this.competition.description}></rhdc-markdown>
			</sl-details>
		`;
	}

	protected override updated( changedProperties: PropertyValues ) : void {
		super.updated( changedProperties );
		this.#tryExpandAndFocus();
	}

	public expandAndFocus() : void {
		this.#expansionPending = true;
		this.#tryExpandAndFocus();
	}

	#tryExpandAndFocus() : void {
		if( !this.#expansionPending || !this.base ) return;

		const target = this.base.shadowRoot?.querySelector( '::part( base )' ) as Weak<HTMLElement>;
		if( !target ) return;

		const focusOptions = { focusVisible: true, preventScroll: true } as unknown as FocusOptions;

		this.base.show();
		this.focus( focusOptions );
		
		if( !(target.tabIndex >= 0) ) {
			target.tabIndex = -1;
		}

		target.focus( focusOptions );
		this.scrollIntoView({ behavior: 'smooth', block: 'center' });

		this.#expansionPending = false;
	}

}
