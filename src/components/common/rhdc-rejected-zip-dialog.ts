import { css, CSSResultGroup, html } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../rhdc-element';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/button/button';

@customElement( 'rhdc-rejected-zip-dialog' )
export class RhdcRejectedZipDialog extends RhdcElement {

	@state()
	rejectedFiles: string[] = [];

	@state()
	allowedExtensions: string[] = [];

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			sl-dialog {
				min-width: min( 50vw );
				max-width: min( 90vw );
			}

			p {
				margin: 1em 0;
			}

			ul.files {
				font-family: var(--sl-font-mono);
				padding-right: 1em;
				line-height: 1.2em;
				max-height: 6em;
				margin: 1em 0;
				overflow: auto;
			}

			ul.files > li {
				white-space: pre;
			}

			div {
				font-family: var(--sl-font-mono);
				display: flex;
				flex-wrap: wrap;
				width: 100%;
				gap: 0 1ch;
			}

		`;
	}

	override render() : unknown {
		return html`
			<sl-dialog label="Zip Upload Rules">
				<p>
					Sorry, but this zip file cannot be submitted because it contains files of an unknown or unsupported type.
					Please ensure that every file in the zip has an allowed file extension.
					The following ${this.rejectedFiles.length === 1 ? 'file is' : 'files are'} causing your submission to be rejected:
				</p>
				<ul class="files">
					${map( this.rejectedFiles, filePath => html`<li>${filePath}</li>` )}
				</ul>
				<p>Only the following file types are allowed in zip uploads:</p>
				<ul>
					<li>Patch files</li>
					<li>Plain text files</li>
					<li>Star layout files</li>
					<li><i>Bowser's Blueprints</i> project files</li>
					<li>Decomp map files</li>
					<li>PDF files</li>
					<li>Image, audio, and video files</li>
				</ul>
				<p>
					<span>The full list of allowed file extensions is as follows:</span>
					<div>
						${map( this.allowedExtensions, ext => html`<span>${ext}</span>` )}
					</div>
				</p>
				<sl-button slot="footer" variant="primary" @click=${this.hide.bind( this )}>Okay</sl-button>
			</sl-dialog>
		`;
	}

	public show( rejectedFiles: string[], allowedExtensions: string[] ) : void {
		this.rejectedFiles = rejectedFiles;
		this.allowedExtensions = allowedExtensions;
		this.shadowRoot?.querySelector( 'sl-dialog' )?.show();
	}

	public hide() : void {
		this.shadowRoot?.querySelector( 'sl-dialog' )?.hide();
	}

}
