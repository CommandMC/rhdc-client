import { css, CSSResultGroup, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-info-bubble' )
export class RhdcInfoBubble extends RhdcElement {

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline;
			}

			sl-icon {
				color: var(--sl-color-primary-600);
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-tooltip placement="top-start" part="tooltip">
				<sl-icon tabindex="0" name="info-circle-fill"></sl-icon>
				<span slot="content"><slot></slot></span>
			</sl-tooltip>
		`;
	}

}
