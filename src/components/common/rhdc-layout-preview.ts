import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { live } from 'lit/directives/live.js';
import { AdvancedStarLayout, BasicStarLayout, BasicGroup, BasicCourse, StarLayout, AdvancedCourse, AdvancedGroup, getMimeType } from '../../util/star-layout';
import { RhdcElement } from '../../rhdc-element';
import { Theme } from '../../util/theme';
import { Nullable } from '../../util/types';
import '@shoelace-style/shoelace/dist/components/switch/switch';

@customElement( 'rhdc-layout-preview' )
export class RhdcLayoutPreview extends RhdcElement {

	@property({ attribute: false })
	starLayout!: Readonly<StarLayout>;

	@property({ attribute: 'completed', type: Boolean, reflect: true })
	completed = false;

	@state()
	darkMode = Theme.isDarkMode;

	#iconUrl = '/assets/img/star.png';
	#greyIcon = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-2x-small);
			}

			.layout {
				font-family: 'Noto Sans', var(--sl-font-sans);
				font-size: 10pt;
				background-color: #eff0f1;
				color: #232627;
				padding: 6px;
			}

			.layout[data-dark] {
				background-color: #31363b;
				color: #eff0f1;
			}

			.advanced {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: 6px;
			}

			.columns {
				display: flex;
				align-items: flex-start;
				gap: 6px;
			}

			.section {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: 6px;
			}

			.group-name {
				text-align: center;
			}

			.group-name, .course-name {
				white-space: pre;
			}

			td:first-child {
				text-align: end;
			}

			td > div {
				display: flex;
				gap: 6px;
			}

			img {
				width: 24px;
				height: 24px;
				object-fit: fill;
			}

			img[data-grey] {
				filter: grayscale( 1.0 );
			}
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( this.completed ) {
			this.#greyIcon = false;
			if( this.starLayout.collectedStarIcon ) {
				this.#iconUrl = this.#getIconUrl( this.starLayout.collectedStarIcon );
			} else {
				this.#iconUrl = '/assets/img/star.png';
			}
		} else if( this.starLayout.missingStarIcon ) {
			this.#iconUrl = this.#getIconUrl( this.starLayout.missingStarIcon );
			this.#greyIcon = false;
		} else if( this.starLayout.collectedStarIcon ) {
			this.#iconUrl = this.#getIconUrl( this.starLayout.collectedStarIcon );
			this.#greyIcon = true;
		} else {
			this.#iconUrl = '/assets/img/star.png';
			this.#greyIcon = true;
		}
	}

	override render() : unknown {
		const layout = (this.starLayout.$schema === 'https://parallel-launcher.ca/layout/advanced-01/schema.json') ?
			this.#renderAdvanced( this.starLayout ) :
			this.#renderBasic( this.starLayout );

		return html`
			<sl-switch ?checked=${live( this.darkMode )} @sl-change=${this.#toggleDarkMode}>Dark Mode</sl-switch>
			<sl-switch ?checked=${live( this.completed )} @sl-change=${this.#toggleCompleted}>Completed Save File</sl-switch>
			<div class="layout" ?data-dark=${this.darkMode}>${layout}</div>
		`;
	}

	override connectedCallback(): void {
		super.connectedCallback();
		this.darkMode = Theme.isDarkMode;
	}

	#toggleDarkMode() : void {
		this.darkMode = !this.darkMode;
	}

	#toggleCompleted() : void {
		this.completed = !this.completed;
	}

	#getIconUrl( base64: string ) : string {
		const mimeType = getMimeType( base64 );
		if( !mimeType ) return 'about:blank';
		return `data:${mimeType};base64,${base64}`;
	}

	#renderBasic( layout: BasicStarLayout ) : HTMLTemplateResult {
		const leftSide: BasicGroup[] = [];
		const rightSide: BasicGroup[] = [];

		for( const group of layout.groups ) {
			if( group.side === 'left' ) {
				leftSide.push( group );
			} else {
				rightSide.push( group );
			}
		}

		return html`
			<div class="columns">
				${this.#renderBasicGroups( leftSide )}
				${this.#renderBasicGroups( rightSide )}
			</div>
		`;
	}

	#renderBasicGroups( groups: BasicGroup[] ) : Nullable<HTMLTemplateResult> {
		const rows: HTMLTemplateResult[] = [];
		for( const group of groups ) {
			rows.push( html`<div class="group-name">${group.name}</div>` );
			if( group.courses.length ) {
				rows.push( html`<table><tbody>${this.#renderBasicCourses( group.courses )}</tbody></table>` );
			}
		}

		return rows.length ? html`<div class="section">${rows}</div>` : null;
	}

	#renderBasicCourses( courses: BasicCourse[] ) : HTMLTemplateResult[] {
		const output: HTMLTemplateResult[] = [];
		for( const course of courses ) {
			output.push( html`
				<tr>
					<td class="course-name">${course.name}</td>
					<td><div>${this.#renderStars( course.starMask )}</div></td>
				</tr>
			`);
		}
		return output;
	}

	#renderAdvanced( layout: AdvancedStarLayout ) : HTMLTemplateResult {
		const sides = {
			top: [] as AdvancedGroup[],
			left: [] as AdvancedGroup[],
			right: [] as AdvancedGroup[],
			bottom: [] as AdvancedGroup[]
		};

		for( const group of layout.groups ) {
			sides[group.side].push( group );
		}

		return html`
			<div class="advanced">
				${this.#renderAdvancedGroups( sides.top )}
				<div class="columns">
					${this.#renderAdvancedGroups( sides.left )}
					${this.#renderAdvancedGroups( sides.right )}
				</div>
				${this.#renderAdvancedGroups( sides.bottom )}
			</div>
		`;
	}

	#renderAdvancedGroups( groups: AdvancedGroup[] ) : Nullable<HTMLTemplateResult> {
		const rows: HTMLTemplateResult[] = [];
		for( const group of groups ) {
			rows.push( html`<div class="group-name">${group.name}</div>` );
			if( group.courses.length ) {
				rows.push( html`<table><tbody>${this.#renderAdvancedCourses( group.courses )}</tbody></table>` );
			}
		}

		return rows.length ? html`<div class="section">${rows}</div>` : null;
	}

	#renderAdvancedCourses( courses: AdvancedCourse[] ) : HTMLTemplateResult[] {
		const output: HTMLTemplateResult[] = [];
		for( const course of courses ) {
			const stars = course.data.map( datum => this.#renderStars( datum.mask ) ).flat();
			output.push( html`
				<tr>
					<td class="course-name">${course.name}</td>
					<td><div>${stars}</div></td>
				</tr>
			`);
		}
		return output;
	}

	#renderStars( starMask: number ) : HTMLTemplateResult[] {
		const output: HTMLTemplateResult[] = [];
		while( starMask > 0 ) {
			output.push( html`<img ?data-grey=${this.#greyIcon} src=${this.#iconUrl}>` );
			starMask &= starMask - 1;
		}
		return output;
	}

}
