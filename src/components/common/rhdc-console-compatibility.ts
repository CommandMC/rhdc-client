import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { ConsoleCompatibility } from '../../apis/hacks-api';
import { Nullable } from '../../util/types';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';

interface DisplayInfo {
	tooltip: string;
	colour: string;
}

@customElement( 'rhdc-console-compatibility' )
export class RhdcConsoleCompatibility extends RhdcElement {

	@property({ attribute: false })
	compatibility: Nullable<ConsoleCompatibility> = null;

	@property({ attribute: 'hoist', type: Boolean })
	hoist = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: inline;
			}

			sl-tooltip::part(base__popup) {
				white-space: normal;
				text-align: justify
			}
		`;
	}

	override render() : unknown {
		const displayInfo = this.#getDisplayInfo();
		return html`
			<sl-tooltip content=${displayInfo.tooltip} ?hoist=${this.hoist} ?disabled=${!displayInfo.tooltip}>
				<span style="${`color: var(--sl-color-${displayInfo.colour}-500);`}">${this.compatibility?.toString() || 'Unknown'}</span>
			</sl-tooltip>
		`;
	}

	#getDisplayInfo() : DisplayInfo {
		switch( this.compatibility ) {
			case ConsoleCompatibility.Excellent:
				return {
					colour: 'green',
					tooltip: 'This hack works perfectly on console. Console players will have a good experience.'
				};
			case ConsoleCompatibility.Good:
				return {
					colour: 'blue',
					tooltip: 'This hack works well on console, but there is minor lag in some areas, the low resolution of N64 negatively affects gameplay, and/or there are other things that make emulator the recommended way to play the game.'
				};
			case ConsoleCompatibility.Playable:
				return {
					colour: 'amber',
					tooltip: 'This hack is playable on console, but may suffer from minor lag in many areas or major lag in at least one area.'
				};
			case ConsoleCompatibility.Unoptimized:
				return {
					colour: 'orange',
					tooltip: 'This hack displays correctly on console and does not crash, but it has too much lag to be playable on N64 hardware.'
				};
			case ConsoleCompatibility.None:
				return {
					colour: 'red',
					tooltip: 'This hack crashes and/or doesn\'t display correctly on console.'
				};
			default:
				return {
					colour: 'gray',
					tooltip: ''
				};
		}
	}

}
