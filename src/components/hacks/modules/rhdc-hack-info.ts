import { css, CSSResultGroup, html, HTMLTemplateResult, nothing, PropertyValues } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { guard } from 'lit/directives/guard.js';
import { until } from 'lit/directives/until.js';
import { rhdcUserContext, UserAuth } from '../../../decorators/user-context';
import { WeakRichUserRef } from '../../../apis/users-api';
import { RhdcElement } from '../../../rhdc-element';
import { Hack, HacksApi } from '../../../apis/hacks-api';
import { HackSubmissionApi } from '../../../apis/hack-submission-api';
import { CompetitionsApi } from '../../../apis/competitions-api';
import { Nullable, ConstRef } from '../../../util/types';
import { map } from 'lit/directives/map.js';
import { Role } from '../../../auth/roles';
import { countLayoutStars, StarLayout } from '../../../util/star-layout';
import { HttpClient, HttpError } from '../../../apis/http-client';
import { FilesApi, DownloadInfo } from '../../../apis/files';
import { RhdcHackUpdatedEvent } from '../../../events/rhdc-hack-updated-event';
import { getErrorMessage } from '../../../util/http-error-parser';
import { RhdcTagInput } from '../../../widgets/rhdc-tag-input';
import { getUrlPath, toApiUrl } from '../../../util/url';
import { getOrdinalSuffix } from '../../../util/formatting';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea';
import SlCheckbox from '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/option/option';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '@shoelace-style/shoelace/dist/components/checkbox/checkbox';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '../../../widgets/rhdc-tag-input';
import '../../../widgets/rhdc-link';
import '../../common/rhdc-username';

@customElement( 'rhdc-hack-info' )
export class RhdcHackInfo extends RhdcElement {

	@property({ attribute: false })
	hack!: Hack;

	@state()
	editing = false;

	@state()
	usernameHints: Nullable<string[]> = null;

	@state()
	tagHints: Nullable<string[]> = null;

	@state()
	loading = false;

	@query( 'sl-dialog' )
	claimDialog!: SlDialog;

	@query( 'sl-select' )
	authorSelect!: SlSelect;

	@query( 'sl-textarea' )
	reasonTextarea!: SlTextarea;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			a.layout-dl {
				color: var(--sl-color-success-600);
				text-decoration: none;
			}

			a.layout-dl:hover, a.layout-dl:focus-visible {
				color: var(--sl-color-success-700);
				text-decoration: dotted underline;
			}

			h1 {
				margin: 0;
				margin-bottom: var(--sl-spacing-x-small);
			}

			h1 > sl-button {
				float: inline-end;
			}

			.authors, .tags {
				display: flex;
				flex-wrap: wrap;
			}

			.authors {
				gap: 0.5ch;
			}

			.tags {
				gap: var(--sl-spacing-x-small);
			}

			.tags > b {
				align-self: center;
			}

			.description {
				white-space: pre-wrap;
				padding-left: var(--sl-spacing-2x-small);
				margin: var(--sl-spacing-2x-small) 0;
				border-left: var(--sl-spacing-3x-small) solid var(--sl-color-neutral-500);
			}

			.dot {
				color: var(--sl-color-neutral-600);
			}

			.dot:last-of-type {
				display: none;
			}

			#stars {
				max-width: 200px;
			}

			.button-tray {
				display: flex;
				align-self: flex-start;
				gap: var(--sl-spacing-x-small);
			}

			.button-tray > sl-button {
				min-width: 80px;
			}

			.star-info > sl-icon {
				color: var(--sl-color-amber-500);
				font-size: 0.75em;
				padding-inline-start: var(--sl-spacing-3x-small);
			}

			.star-info > .divider {
				color: var(--sl-color-neutral-600);
			}

			sl-tag::part(content) {
				align-self: baseline;
			}

			sl-dialog {
				--width: min( 600px, 90vw );
			}

			sl-dialog > div {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-small);
			}

			sl-icon-button[name="person-up"]::part(base) {
				padding: 0;
			}

			sl-icon[data-place="1"] {
				color: #ffd700;
			}

			sl-icon[data-place="2"] {
				color: #c0c0c0;
			}

			sl-icon[data-place="3"] {
				color: #cd7f32;
			}

			sl-icon[data-place="4"] {
				color: #1d8541;
			}

			sl-icon[data-place="5"] {
				color: #5724a5;
			}

			sl-button > sl-icon {
				font-size: 1rem;
			}
		`;
	}

	override render() : unknown {
		return this.editing ? this.#renderEditView() : this.#renderReadonlyView();
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( !changedProperties.has( 'editing' ) ) return;
		if( !this.editing ) return;

		if( !this.usernameHints ) {
			HackSubmissionApi.getAllUsernamesAsync().then( users => this.usernameHints = users );
		}

		if( !this.tagHints ) {
			HackSubmissionApi.getPublicTagsAsync().then( tags => this.tagHints = tags );
		}
	}

	#renderReadonlyView() : HTMLTemplateResult {
		let privateIcon = null;
		if( this.hack.private ) {
			privateIcon = html`
				<sl-tooltip content="This hack is private. It is only visible to moderators, staff, and the hack uploader and authors.">
					<sl-icon name="eye-slash"></sl-icon>
				</sl-tooltip>
			`;
		}

		let editButton = null;
		if( this.hack.canEditInfo ) {
			editButton = html`<sl-icon-button name="pencil" @click=${() => this.editing = true}></sl-icon-button>`;
		}

		let matureTag = null;
		if( this.hack.mature ) {
			matureTag = html`<sl-tag variant="danger" size="small" pill>Mature</sl-tag>`;
		}

		let starLayoutLink: unknown = null;
		if( this.hack.layout ) {
			if( this.hack.layout.secured ) {
				starLayoutLink = html`<a class="layout-dl" href="javascript: void(0)" @click=${this.#downloadLayout}>Has Star Layout</a>`;
			} else {
				starLayoutLink = html`<a class="layout-dl" href="${toApiUrl( this.hack.layout.directHref )}" download type="${this.hack.layout.mimeType}">Has Star Layout</a>`;
			}

			starLayoutLink = html`
				<span class="divider"> | </span>
				${starLayoutLink}
				<rhdc-info-bubble>
					This hack contains a star layout, allowing you to view which stars you have collected as you play in Parallel Launcher if
					you have RHDC Integration enabled. You do not need to manually download this file as Parallel Launcher will automatically
					download it for you the first time you play.
				</rhdc-info-bubble>
			`;
		}

		let claimLink: unknown = nothing;
		if( this.hack.canClaimOwnership ) {
			claimLink = html`
				<sl-tooltip content="I am an author of this hack">
					<sl-icon-button
						name="person-up"
						?disabled=${this.loading}
						@click=${() => this.claimDialog.show()}
					></sl-icon-button>
				</sl-tooltip>
			`;
		}

		const authorOptions: HTMLTemplateResult[] = [];
		for( const author of this.hack.authors ) {
			if( author.userId ) continue;
			authorOptions.push( html`<sl-option value="${author.username}">${author.username}</sl-option>` );
		}

		let competitionBadge: unknown = nothing;
		if( this.hack.competitionId ) {
			competitionBadge = guard(
				[ this.hack.competitionId, this.hack.competitionPlacement ],
				() => until( this.#renderCompetitionBadgeAsync(), html`<sl-spinner></sl-spinner>` )
			);
		}

		let deleteButton: unknown = nothing;
		if( this.hack.canDelete ) {
			deleteButton = html`
				<sl-button
					variant="danger"
					?loading=${this.loading}
					@click=${this.#deleteHack}
				>
					<sl-icon label="Delete Hack" name="trash"></sl-icon>
				</sl-button>
			`;
		}

		return html`
			<h1>${this.hack.title} ${privateIcon} ${editButton} ${deleteButton}</h1>
			<div class="authors">
				<b>Authors:</b>
				${map( this.hack.authors, this.#renderAuthor )}
				${claimLink}
			</div>
			<div class="tags">
				<b>Tags:</b>
				<sl-tag variant="primary" size="small" pill>${this.hack.category}</sl-tag>
				${matureTag}
				${map( this.hack.tags, tag => html`<sl-tag size="small" pill>${tag}</sl-tag>` )}
			</div>
			${competitionBadge}
			<div class="description">${this.hack.description}</div>
			<div class="star-info">
				<span>${this.hack.stars}</span><sl-icon name="star-fill"></sl-icon>
				<span> stars</span>
				<span class="divider"> | </span>
				<span>${this.hack.completionBonus || 'no'} completion bonus</span>
				${starLayoutLink}
			</div>
			<sl-dialog label="Claim Ownership">
				<div>
					<p>
						If you are an author of this hack, select which listed author, if any, is you, and provide an explanation if necessary.
						The moderators will approve or reject your claim in the next few days.
					</p>

					<sl-select label="Unregistered author to replace" clearable .defaultValue=${''} ?disabled=${this.loading || authorOptions.length === 0}>
						<sl-option value="">I am not listed as an author.</sl-option>
						${authorOptions}
					</sl-select>

					<sl-textarea
						label="Explanation (Optional)"
						rows="2"
						?disabled=${this.loading}
						autocomplete="off"
						enterkeyhint="done"
						spellcheck
						inputmode="text"
					></sl-textarea>
				</div>

				<sl-button slot="footer" variant="primary" ?loading=${this.loading} @click=${this.#claimOwnership}>Make Claim</sl-button>
				<sl-button slot="footer" variant="neutral" ?disabled=${this.loading} @click=${() => this.claimDialog.hide()}>Cancel</sl-button>
			</sl-dialog>
		`;
	}

	#renderEditView() : HTMLTemplateResult {
		return html`
			<sl-input type="text"
				id="title"
				size="large"
				minlength="1"
				placeholder="Title"
				required
				autofocus
				autocapitalize="on"
				inputmode="text"
				?disabled=${this.loading}
				.value=${this.hack.title}
			></sl-input>
			<rhdc-tag-input
				id="authors"
				.value=${this.hack.authors.map( a => a.username )}
				?loading=${!this.usernameHints}
				case-sensitive
				highlight-existing
				placeholder="Enter username here. Tab to autocomplete."
				label="Author(s)"
				required
				?disabled=${this.loading}
				.autocomplete=${this.usernameHints || []}
			></rhdc-tag-input>
			<rhdc-tag-input
				id="tags"
				.value=${this.hack.tags}
				?loading=${!this.tagHints}
				allow-spaces
				placeholder="Enter text here and hit Enter to add a tag."
				label="Tags"
				?disabled=${this.loading}
				.autocomplete=${this.tagHints || []}
			></rhdc-tag-input>
			<sl-textarea
				id="description"
				placeholder="Description"
				rows="6"
				resize="vertical"
				autocapitalize="sentences"
				required
				spellcheck
				inputmode="text"
				?disabled=${this.loading}
				.value=${this.hack.description}
			></sl-textarea>
			<sl-input type="number"
				id="stars"
				label="Stars"
				?disabled=${this.loading}
				min="0"
				max="9999"
				step="1"
				required
				enterkeyhint="next"
				inputmode="numeric"
				.valueAsNumber=${this.hack.stars}
			></sl-input>
			<sl-checkbox
				id="public"
				?checked=${!this.hack.private}
				?disabled=${this.loading}
			>Public</sl-checkbox>
			<div class="button-tray">
				<sl-button
					variant="primary"
					?loading=${this.loading}
					@click=${this.#save.bind( this )}
				>Save</sl-button>
				<sl-button
					variant="neutral"
					?loading=${this.loading}
					@click=${() => this.editing = false}
				>Cancel</sl-button>
			</div>
			<sl-divider></sl-divider>
		`;
	}

	#renderAuthor( author: WeakRichUserRef ) : HTMLTemplateResult {
		const name = author.userId ? html`<rhdc-username .user=${author}></rhdc-username>` : html`<span>${author.username}</span>`;
		return html`${name}<span class="dot">&bull;</span>`;
	}

	async #renderCompetitionBadgeAsync() : Promise<HTMLTemplateResult> {
		const competition = await CompetitionsApi.getCompetitionAsync( this.hack.competitionId! );
		const competitionLink = html`<rhdc-link href="/competitions/${this.hack.competitionId}">${competition.name}</rhdc-link>`;

		if( !this.hack.competitionPlacement || this.hack.competitionPlacement > 10 ) {
			return html`<div><span>Participated in </span>${competitionLink}</div>`;
		}

		if( this.hack.competitionPlacement > 5 ) {
			return html`<div><span>Placed top 10 in </span>${competitionLink}</div>`;
		}

		return html`
			<div>
				<sl-icon
					name="${this.hack.competitionPlacement! <= 3 ? 'trophy-fill' : 'award-fill'}"
					data-place="${this.hack.competitionPlacement!}"
				></sl-icon>
				<span>${this.hack.competitionPlacement}${getOrdinalSuffix( this.hack.competitionPlacement! )} place in </span>
				<rhdc-link href="/competitions/${this.hack.competitionId}">${competition.name}</rhdc-link>
			</div>
		`;
	}

	async #save() : Promise<void> {
		const title = this.#getInput<SlInput>( 'title' ).value;
		const authors = this.#getInput<RhdcTagInput>( 'authors' ).value;
		const tags = this.#getInput<RhdcTagInput>( 'tags' ).value;
		const description = this.#getInput<SlTextarea>( 'description' ).value;
		const stars = this.#getInput<SlInput>( 'stars' ).valueAsNumber;
		const isPrivate = !this.#getInput<SlCheckbox>( 'public' ).checked;

		if( !title?.trim() ) {
			this.toastError( 'Update Failed', 'You may not remove this hack\'s title' );
			return;
		}

		if( !description?.trim() ) {
			this.toastError( 'Update Failed', 'You may not remove this hack\'s description' );
			return;
		}

		if(
			this.currentUser!.role !== Role.Staff &&
			this.currentUser!.role !== Role.Moderator &&
			!authors.includes( this.currentUser!.username ) &&
			( authors.length > 0 || this.currentUser!.userId !== this.hack.uploader?.userId ) &&
			await !this.confirmAsync( 'Warning!', 'By removing yourself as an author, you will no longer be able to edit this hack. Are you sure you want to continue?', 'Save', 'Cancel' )
		) return;

		if( stars > 1 && this.hack.layout ) {
			const layoutStars = await this.#tryGetLayoutStarCount( this.hack.layout );
			if(
				layoutStars !== null &&
				stars !== layoutStars &&
				await !this.confirmAsync( 'Confirm Star Count', 'The star count you have provided does not match the number of stars in your star layout file. Automatic starpower submissions will not function correctly. Are you sure you want to continue?', 'Save', 'Cancel' )
			) return;
		}

		try {
			this.loading = true;
			this.hack = await HacksApi.updateHackAsync( this.hack.hackId, {
				title: title,
				description: description,
				stars: stars,
				private: isPrivate,
				authors: authors,
				tags: tags
			});

			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );

			this.editing = false;
			this.toastSuccess( 'Hack updated' );
		} catch( exception: unknown ) {
			this.toastError( 'Update Failed', getErrorMessage( exception ) );
			return;
		} finally {
			this.loading = false;
		}
	}

	#getInput<T extends HTMLElement>( id: string ) : T {
		return this.shadowRoot!.getElementById( id ) as T;
	}

	async #tryGetLayoutStarCount( downloadInfo: ConstRef<DownloadInfo> ) : Promise<Nullable<number>> {
		try {
			if( downloadInfo.secured ) {
				const downloadToken = await FilesApi.getDownloadTokenAsync( getUrlPath( downloadInfo.webHref ) );
				const layout = await HttpClient.getAsync<StarLayout>( getUrlPath( downloadToken.href ) );
				return countLayoutStars( layout );
			} else {
				const layout = await HttpClient.getAsync<StarLayout>( getUrlPath( downloadInfo.directHref ) );
				return countLayoutStars( layout );
			}
		} catch{
			return null;
		}
	}

	async #downloadLayout() : Promise<void> {
		const downloadToken = await FilesApi.getDownloadTokenAsync( getUrlPath( this.hack.layout!.webHref ) );
		
		const virtualLink = document.createElement( 'a' );
		virtualLink.href = toApiUrl( downloadToken.href );
		virtualLink.type = downloadToken.mimeType;
		virtualLink.setAttribute( 'download', 'download' );
		virtualLink.click();
	}

	async #claimOwnership() : Promise<void> {
		const targetAuthor = this.authorSelect.value as string;
		const reason = this.reasonTextarea.value;

		this.loading = true;
		try {
			await HacksApi.claimHackOwnershipAsync( this.hack!.hackId, targetAuthor, reason );
			this.claimDialog.hide();
		} catch( exception : unknown ) {
			if( exception instanceof HttpError && exception.problem?.status === 409 ) {
				this.toastInfo( exception.problem.detail! );
				this.claimDialog.hide();
			} else {
				this.toastError( 'Failed to claim ownership', getErrorMessage( exception ) );
			}
		} finally {
			this.loading = false;
		}
	}

	async #deleteHack() : Promise<void> {
		if( await this.confirmAsync( 'Confirm Delete', 'Are you sure you want to delete this hack? This action cannot be undone.', 'Delete', 'Cancel' ) ) {
			this.loading = true;
			try {
				await HacksApi.deleteHackAsync( this.hack.hackId );
				this.toastSuccess( 'Hack Deleted' );
				this.navigate( '/hacks' );
			} catch( exception: unknown ) {
				this.toastError( 'Failed to delete hack', getErrorMessage( exception ) );
			} finally {
				this.loading = false;
			}
		}
	}

}
