import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { when } from 'lit/directives/when.js';
import { RhdcElement } from '../../../rhdc-element';
import { Starpower, StarpowerApi, StarpowerUpdateDto } from '../../../apis/starpower-api';
import { Category, Hack } from '../../../apis/hacks-api';
import { getErrorMessage } from '../../../util/http-error-parser';
import { Nullable } from '../../../util/types';
import { RhdcDialog } from '../../common/rhdc-dialog';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlSwitch from '@shoelace-style/shoelace/dist/components/switch/switch';
import SlRating from '@shoelace-style/shoelace/dist/components/rating/rating';
import SlHoverEvent from '@shoelace-style/shoelace/dist/events/sl-hover';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/progress-bar/progress-bar';
import '@shoelace-style/shoelace/dist/components/rating/rating';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/switch/switch';
import '../../common/rhdc-info-bubble';
import '../../common/rhdc-dialog';

@customElement( 'rhdc-hack-starpower' )
export class RhdcHackStarpower extends RhdcElement {

	@property({ attribute: false })
	starpower!: Starpower;

	@property({ attribute: false })
	hack!: Hack;

	@state()
	editing = false;

	@state()
	loading = false;

	@state()
	hoverRating = 0;

	@state()
	hoverDifficulty = 0;

	@query( '#star-input' )
	starCountInput!: Nullable<SlInput>;

	@query( '#star-input2' )
	dialogStarCountInput!: Nullable<SlInput>;

	@query( '#rating-input' )
	ratingInput!: Nullable<SlRating>;

	@query( '#difficulty-input' )
	difficultyInput!: Nullable<SlRating>;

	@query( '#completed' )
	completeToggle!: Nullable<SlSwitch>;

	@query( '#proof-input' )
	proofInput!: Nullable<SlInput>;

	@query( 'rhdc-dialog' )
	dialog!: RhdcDialog;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			sl-card > div {
				display: flex;
				flex-direction: column;
			}

			sl-progress-bar {
				--height: 0.375rem;
				width: 100%;
			}

			sl-rating[label="Difficulty"] {
				--symbol-color-active: var(--sl-color-primary-600);
			}

			sl-rating[label="Difficulty"][data-kaizo] {
				--symbol-color-active: var(--sl-color-danger-500);
			}

			.label {
				font-size: var(--sl-font-size-2x-small);
			}

			.label, .play-time {
				white-space: nowrap;
			}

			sl-icon[name="star-fill"] {
				color: var(--sl-color-amber-500);
			}

			sl-icon[name="check-circle"] {
				color: var(--sl-color-success-500);
			}

			sl-icon[name="exclamation-circle"] {
				color: var(--sl-color-warning-500);
			}

			div.star-count {
				display: flex;
				align-items: center;
			}

			sl-dialog > div {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			#star-input2 {
				align-self: flex-start;
				max-width: 256px;
			}

			sl-card > div > div:last-child {
				margin-top: var(--sl-spacing-x-small);
			}

			.space-left {
				margin-left: 1ch;
			}
		`;
	}

	override render() : unknown {
		let starCount: HTMLTemplateResult;
		if( this.hack.stars <= 1 ) {
			starCount = html`<sl-switch
				id="completed"
				?checked=${this.starpower.hackComplete}
				@sl-change=${this.#setCompletion}
			>Complete</sl-switch>`;
		} else if( this.editing && ( !this.hack.needsVerification || this.starpower.canApprove ) ) {
			starCount = html`
				<span class="label">Progress</span><br/>
				<sl-input
					id="star-input"
					type="number"
					value="${this.starpower.claimedStarPoints}"
					min="0"
					max="${this.hack.stars}"
					step="1"
					required
					enterkeyhint="done"
					inputmode="numeric"
					autofocus
					@sl-change=${this.#update.bind( this, false )}
				>
					<sl-icon name="star-fill" slot="suffix"></sl-icon>
				</sl-input>
			`;
		} else {
			const parts: HTMLTemplateResult[] = [];
			parts.push( html`
				<span>
					<span>${this.starpower.claimedStarPoints} / ${this.hack.stars}</span>
					<sl-icon name="star-fill"></sl-icon>
				</span>
			` );
			if( this.starpower.approved ) {
				parts.push( html`
					<sl-tooltip content="Approved">
						<sl-icon label="Approved" name="check-circle" class="space-left"></sl-icon>
					</sl-tooltip>
				` );
			} else if( this.hack.needsVerification ) {
				parts.push( html`
					<sl-tooltip content="Pending Approval">
						<sl-icon label="Pending Approval" name="exclamation-circle" class="space-left"></sl-icon>
					</sl-tooltip>
				` );
			}
			
			if( this.starpower.canEdit ) {
				parts.push( html`
					<sl-icon-button name="pencil" @click=${this.#editStars}></sl-icon-button>
				` );
			}

			starCount = html`
				<span class="label">Progress</span>
				<div class="star-count">${parts}</div>
			`;
		}

		let ratingText: string;
		switch( this.hoverRating || this.starpower?.rating ) {
			case 1: ratingText = ' - Zero Effort'; break;
			case 2: ratingText = ' - Poor Quality'; break;
			case 3: ratingText = ' - A Little Rough'; break;
			case 4: ratingText = ' - Unremarkable'; break;
			case 5: ratingText = ' - Decent'; break;
			case 6: ratingText = ' - Pretty Good'; break;
			case 7: ratingText = ' - Very Good'; break;
			case 8: ratingText = ' - Excellent'; break;
			case 9: ratingText = ' - Exceptional'; break;
			case 10: ratingText = ' - Legendary'; break;
			default: ratingText = '';
		}

		let difficultyText: string;
		if( this.hack.category === Category.Kaizo ) {
			switch( this.hoverDifficulty || this.starpower?.difficulty ) {
				case 1: difficultyText = ' - Beginner Kaizo'; break;
				case 2: difficultyText = ' - Easy Kaizo'; break;
				case 3: difficultyText = ' - Standard Kaizo'; break;
				case 4: difficultyText = ' - Moderate Kaizo'; break;
				case 5: difficultyText = ' - Challenging Kaizo'; break;
				case 6: difficultyText = ' - Difficult Kaizo'; break;
				case 7: difficultyText = ' - Expert Kaizo'; break;
				case 8: difficultyText = ' - Master Kaizo'; break;
				case 9: difficultyText = ' - Almost Impossible'; break;
				case 10: difficultyText = ' - TAS Only'; break;
				default: difficultyText = '';
			}
		} else {
			switch( this.hoverDifficulty || this.starpower?.difficulty ) {
				case 1: difficultyText = ' - No Challenge'; break;
				case 2: difficultyText = ' - Very Easy'; break;
				case 3: difficultyText = ' - Casual'; break;
				case 4: difficultyText = ' - Classic SM64'; break;
				case 5: difficultyText = ' - Moderate'; break;
				case 6: difficultyText = ' - Challenging'; break;
				case 7: difficultyText = ' - Difficult'; break;
				case 8: difficultyText = ' - Very Difficult'; break;
				case 9: difficultyText = ' - Extremely Difficult'; break;
				case 10: difficultyText = ' - Borderline Kaizo'; break;
				default: difficultyText = '';
			}
		}

		return html`
			<sl-card>
				${when( this.hack.stars > 1, this.#renderProgressBar.bind( this ) )}
				<div>
					<div>
						<b>Your Progress & Ratings</b><br/>
						<span class="label">Rating${ratingText}</span><br/>
						<sl-rating
							id="rating-input"
							label="Rating"
							value="${this.starpower.rating || 0}"
							max="10"
							?readonly=${!this.starpower.canEdit}
							@sl-change=${this.#update.bind( this, true )}
							@sl-hover=${this.#updateRatingHover}
						></sl-rating>
					</div>
					<div>
						<span class="label">Difficulty${difficultyText}</span><br/>
						<sl-rating
							id="difficulty-input"
							label="Difficulty"
							?data-kaizo=${this.hack.category === Category.Kaizo}
							value="${this.starpower.difficulty || 0}"
							max="10"
							?readonly=${!this.starpower.canEdit}
							.getSymbol=${() => '<sl-icon name="fire"></sl-icon>'}
							@sl-change=${this.#update.bind( this, true )}
							@sl-hover=${this.#updateDifficultyHover}
						></sl-rating>
					</div>
					<div>
						<span class="label">Play Time</span><br/>
						<span class="play-time">${this.#renderPlayTime()}</span>
						<span ?hidden=${this.starpower.playTime > 0}>
							<rhdc-info-bubble>Enable romhacking.com integration in Parallel Launcher to track your play time</rhdc-info-bubble>
						</span>
					</div>
					<div>
						${starCount}
					</div>
				</div>
			</sl-card>
			<rhdc-dialog no-header .confirmText=${'Confirm'}>
				<div>
					<p>Kaizo hacks require video proof of your progress. Please provide a link to a video or playlist showing your gameplay.</p>

					<sl-input
						id="star-input2"
						type="number"
						label="Stars"
						value="${this.starpower.claimedStarPoints}"
						min="0"
						max="${this.hack.stars}"
						step="1"
						required
						enterkeyhint="next"
						inputmode="numeric"
						?hidden=${this.hack.stars <= 1}
					>
						<sl-icon name="star-fill" slot="suffix"></sl-icon>
					</sl-input>

					<sl-input
						id="proof-input"
						type="url"
						label="Proof Video Link"
						placeholder="Enter a valid URL here"
						required
						enterkeyhint="done"
						inputmode="url"
					></sl-input>
				</div>
			</rhdc-dialog>
		`;
	}

	#renderProgressBar() : HTMLTemplateResult {
		return html`<sl-progress-bar slot="image" value="${100 * this.starpower.starPoints / this.hack.stars}"></sl-progress-bar>`;
	}

	#renderPlayTime() : string {
		if( this.starpower.playTime <= 0 ) {
			return 'No play time on record';
		} else if( this.starpower.playTime < 60000 ) {
			const seconds = Math.floor( this.starpower.playTime / 1000 );
			return `${seconds} seconds`;
		} else if( this.starpower.playTime < 3600000 ) {
			const minutes = Math.floor( this.starpower.playTime / 60000 );
			return `${minutes} minutes`;
		} else {
			const hours = Math.floor( this.starpower.playTime / 3600000 );
			const minutes = Math.floor( this.starpower.playTime / 60000 ) % 60;
			return `${hours}hr ${minutes}min`;
		}
	}

	async #update( ratingOnly: boolean ) : Promise<void> {
		const update: StarpowerUpdateDto = {
			rating: this.ratingInput!.value || null,
			difficulty: this.difficultyInput!.value || null
		};

		if( !ratingOnly ) {
			if( this.hack.stars <= 1 ) {
				update.hackComplete = this.completeToggle!.checked;
			} else if( this.editing && Number.isFinite( this.starCountInput?.valueAsNumber ) ) {
				update.starPoints = this.starCountInput!.valueAsNumber;
			}

			this.hoverRating = 0;
			this.hoverDifficulty = 0;
		}

		await this.#doUpdate( update );
		this.editing = false;
	}

	#updateRatingHover( event: SlHoverEvent ) : void {
		this.hoverRating = (event.detail.phase !== 'end') ? event.detail.value : 0;
	}

	#updateDifficultyHover( event: SlHoverEvent ) : void {
		this.hoverDifficulty = (event.detail.phase !== 'end') ? event.detail.value : 0;
	}

	async #editStars() : Promise<void> {
		if( !this.hack.needsVerification ) {
			this.editing = true;
			return;
		}

		await this.dialog.runAsync( async () => {
			const update: StarpowerUpdateDto = { proofLink: this.proofInput!.value };
			if( !Number.isFinite( this.dialogStarCountInput?.valueAsNumber ) ) {
				throw new Error( "You must enter a valid number of stars." );
			}

			update.starPoints = this.dialogStarCountInput!.valueAsNumber;
			await this.#doUpdate( update );
		});
	}

	async #setCompletion() : Promise<void> {
		if( this.hack.needsVerification && !this.starpower.canApprove ) {
			if( this.completeToggle!.checked ) {
				await this.dialog.runAsync( async () => {
					await this.#doUpdate({
						hackComplete: true,
						proofLink: this.proofInput!.value
					});
				});
				return;
			} else if( !await this.confirmAsync( 'Confirm Progress Reset', 'If you mark this hack as incomplete, you will need to resubmit proof if/when you complete it again. Are you sure you want to mark this hack as incomplete?', 'Yes', 'Cancel' ) ) {
				this.completeToggle!.checked = true;
				return;
			}
		}

		await this.#doUpdate({ hackComplete: this.completeToggle!.checked });
	}

	async #doUpdate( update: StarpowerUpdateDto ) : Promise<void> {
		this.loading = true;
		try {
			this.starpower = await StarpowerApi.updateStarpowerAsync( this.hack.hackId, update );
			this.toastSuccess( "Star progress updated." );
		} catch( exception: unknown ) {
			this.toastError( getErrorMessage( exception ) );
			throw exception;
		} finally {
			this.loading = false;
		}
	}

}
