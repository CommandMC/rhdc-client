import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../../../rhdc-element';
import { map } from 'lit/directives/map.js';
import { PlaylistCheck, PlaylistsApi } from '../../../apis/playlists-api';
import { ConstArray, HexId, Nullable } from '../../../util/types';
import SlDropdown from '@shoelace-style/shoelace/dist/components/dropdown/dropdown';
import SlSelectEvent from '@shoelace-style/shoelace/dist/events/sl-select';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/dropdown/dropdown';
import '@shoelace-style/shoelace/dist/components/menu/menu';
import '@shoelace-style/shoelace/dist/components/menu-item/menu-item';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';

@customElement( 'rhdc-hack-playlist-button' )
export class RhdcHackPlaylistButton extends RhdcElement {

	@property({ attribute: false })
	hackId!: HexId;

	@state()
	playlists: Nullable<ConstArray<PlaylistCheck>> = null;

	@query( 'sl-dropdown' )
	dropdown!: SlDropdown;

	static override get styles() : CSSResultGroup {
		return css`:host { display: inline-block; }`;
	}

	override render() : unknown {
		const menuItems = this.playlists ?
			map( this.playlists, this.#renderPlaylist.bind( this ) ) :
			html`<sl-spinner></sl-spinner>`;

		return html`
			<sl-dropdown stay-open-on-select @sl-show=${this.#loadPlaylists}>
				<sl-button slot="trigger" variant="primary">
					<sl-icon slot="prefix" name="list-check"></sl-icon>
					Add to Playlist
				</sl-button>
				<sl-menu @sl-select=${this.#selectPlaylist}>
					${menuItems}
				</sl-menu>
			</sl-dropdown>
		`;
	}

	async #loadPlaylists() : Promise<void> {
		if( this.playlists ) return;
		try {
			this.playlists = await PlaylistsApi.getPlaylistsAndCheckHackAsync( this.hackId );
		} catch{
			this.toastError( 'Server Error', 'Failed to load playlists.' );
			this.dropdown.hide();
		}
	}

	#renderPlaylist( playlist: PlaylistCheck ) : HTMLTemplateResult {
		return html`
			<sl-menu-item
				type="checkbox"
				?checked=${playlist.hasHack}
				.value=${playlist.name}
			>${playlist.name}</sl-menu-item>
		`;
	}

	async #selectPlaylist( event: SlSelectEvent ) : Promise<void> {
		event.detail.item.disabled = true;
		try {
			if( event.detail.item.checked ) {
				await PlaylistsApi.addHackToPlaylistAsync( event.detail.item.value, this.hackId );
			} else {
				await PlaylistsApi.removeHackFromPlaylistAsync( event.detail.item.value, this.hackId );
			}
		} catch( exception: unknown ) {
			event.detail.item.checked = !event.detail.item.checked;
			this.toastError( 'Server Error', 'Failed to update playlist.' );
			return;
		} finally {
			event.detail.item.disabled = false;
		}

		this.playlists = Object.freeze( this.playlists?.map(
			pl => pl.name === event.detail.item.value ? new PlaylistCheck({ name: pl.name, hasHack: event.detail.item.checked }) : pl
		) || null );
	}


}
