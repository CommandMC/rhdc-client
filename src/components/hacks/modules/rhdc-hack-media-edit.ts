import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { guard } from 'lit/directives/guard.js';
import { until } from 'lit/directives/until.js';
import { RhdcElement } from '../../../rhdc-element';
import { getErrorMessage } from '../../../util/http-error-parser';
import { parseYouTubeLink } from '../../../util/youtube-link';
import { RhdcFileDropEvent } from '../../../events/rhdc-file-drop-event';
import { RhdcDragEvent } from '../../../widgets/rhdc-drag-item';
import { Hack, HacksApi } from '../../../apis/hacks-api';
import { DownloadInfo, FilesApi } from '../../../apis/files';
import { ConstRef } from '../../../util/types';
import { RhdcHackUpdatedEvent } from '../../../events/rhdc-hack-updated-event';
import { toApiUrl } from '../../../util/url';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '../../../widgets/rhdc-drag-item';
import '../../../widgets/rhdc-file-dropzone';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-hack-media-edit' )
export class RhdcHackMediaEdit extends RhdcElement {

	@property({ attribute: false })
	hack!: Hack;

	@state()
	loading = false;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-x-small);
			}

			.screenshots {
				display: flex;
				flex-wrap: wrap;
			}

			.videos {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
			}

			img {
				height: 240px;
				width: auto;
				object-position: center;
				object-fit: cover;
			}

			rhdc-drag-item > div {
				position: relative;
			}

			rhdc-drag-item:first-child img {
				width: 320px;
			}

			rhdc-drag-item sl-button {
				position: absolute;
				top: var(--sl-spacing-x-small);
				right: var(--sl-spacing-x-small);
				display: none;
			}

			rhdc-drag-item:hover sl-button, rhdc-drag-item:focus sl-button {
				display: block;
				cursor: default;
			}

			.img-loading {
				width: 240px;
				height: 240px;
				display: flex;
				justify-content: center;
				align-items: center;
				font-size: 120px;
				box-sizing: border-box;
				border: 1px solid var(--sl-color-neutral-500);
			}

			img.error {
				box-sizing: border-box;
				border: 1px solid var(--sl-color-danger-500);
			}

			sl-input {
				max-width: 900px;
			}

			sl-input[data-invalid]:not(:focus)::part(base) {
				border-color: var(--sl-color-danger-500);
			}

			rhdc-file-dropzone {
				width: 240px;
				height: 240px;
				margin: var(--sl-spacing-x-small);
			}

			rhdc-file-dropzone > sl-icon {
				font-size: 100px;
			}

			sl-button.done {
				align-self: flex-start;
			}

		`;
	}

	override render() : unknown {
		return html`
			<div class="screenshots">
				${this.#renderScreenshots()}
			</div>
			<div class="videos">
				<b>Videos</b>
				${this.#renderYouTubeLinkInput( 0 )}
				${this.#renderYouTubeLinkInput( 1 )}
				${this.#renderYouTubeLinkInput( 2 )}
			</div>
			<sl-button class="done" variant="primary" @click=${this.#done}>Done Editing</sl-button>
		`;
	}

	#renderYouTubeLinkInput( index: number ) : HTMLTemplateResult {
		return html`
			<sl-input type="url"
				id=${`yt${index + 1}`}
				.value=${this.hack.videos[index] || ''}
				clearable
				placeholder="YouTube video link (Optional)"
				?disabled=${this.loading}
				autocapitalize="off"
				autocorrect="off"
				autocomplete="url"
				enterkeyhint="next"
				inputmode="url"
				@sl-change=${this.#onYouTubeLinkChanged}
			></sl-input>
		`;
	}

	#renderScreenshots() : unknown[] {
		const screenshots: unknown[] = this.hack.screenshots.map(
			(screenshot, i) => guard( [screenshot, this.loading], this.#renderScreenshot.bind( this, screenshot, i ) )
		);

		if( this.hack.screenshots.length < 10 && !this.loading ) {
			screenshots.push( html`
				<rhdc-file-dropzone
					accept=".png,.bmp,.gif,.jpg,.jpeg,image/png,image/bmp,image/gif,image/jpeg"
					@rhdc-file-drop-event=${this.#onUploadScreenshot}
				>
					<sl-icon name="plus-lg"></sl-icon>
				</rhdc-file-dropzone>
			` );
		}

		return screenshots;
	}

	#renderScreenshot( imageInfo: ConstRef<DownloadInfo>, index: number ) : HTMLTemplateResult {
		const image = until(
			this.#fetchImageAsync( imageInfo, index ),
			html`<div class="img-loading"></div>`
		);

		return html`
			<rhdc-drag-item
				horizontal
				?disabled=${this.loading}
				.data=${index}
				.groupId=${'submission-screenshots'}
				@rhdc-drag-event=${this.#onScreenshotDragged}
			>${image}</rhdc-drag-item>
		`;
	}

	async #fetchImageAsync( imageInfo: ConstRef<DownloadInfo>, index: number ) : Promise<HTMLTemplateResult> {
		if( !imageInfo.secured ) {
			return html`<div><img src="${toApiUrl( imageInfo.directHref )}">${this.#renderDeleteButton( index )}</div>`;
		}

		const downloadToken = await FilesApi.getDownloadTokenAsync( imageInfo.webHref );
		return html`<div><img src="${toApiUrl( downloadToken.href )}">${this.#renderDeleteButton( index )}</div>`;
	}

	#renderDeleteButton( index: number ) {
		return html`
			<sl-button
				variant="danger"
				?loading=${this.loading}
				@click=${this.#removeScreenshotAsync.bind( this, index )}
			>
				<sl-icon name="trash"></sl-icon>
			</sl-button>
		`;
	}

	async #onUploadScreenshot( event: RhdcFileDropEvent ) : Promise<void> {
		const image = event.detail.file;
		if( ![ 'image/png', 'image/bmp', 'image/gif', 'image/jpeg' ].includes( image.type ) ) {
			this.toastError( 'Screenshot Rejected', 'Screenshots must be in one of the following formats: PNG, BMP, GIF, JPEG' );
			return;
		}

		this.loading = true;
		try {
			this.hack = await HacksApi.addScreenshotAsync( this.hack.hackId, image );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
		} catch( exception: unknown ) {
			this.toastError( 'Screenshot Rejected', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #removeScreenshotAsync( index: number ) : Promise<void> {
		this.loading = true;
		try {
			this.hack = await HacksApi.removeScreenshotAsync( this.hack.hackId, index );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to delete screenshot', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #onScreenshotDragged( event: RhdcDragEvent ) : Promise<void> {
		const source = event.detail.source.data as number;
		const before = (event.detail.target.data as number) + (event.detail.before ? 0 : 1);

		if( source === before || source === before - 1 ) return;
		this.loading = true;
		try {
			this.hack = await HacksApi.moveScreenshotAsync( this.hack.hackId, source, before );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to reorder screenshots', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #onYouTubeLinkChanged( event: Event ) : Promise<void> {
		const input = event.target as SlInput;

		let url = input.value;
		if( !url ) {
			input.setCustomValidity( '' );
		} else {
			const link = parseYouTubeLink( url );
			if( !link ) {
				input.setCustomValidity( 'The provided URL is not a valid YouTube link.' );
				return;
			}
			url = link.standardLink();
		}

		const newVideos: string[] = [];
		for( let i = 1; i <= 3; i++ ) {
			const linkInput = this.shadowRoot?.getElementById( `yt${i}` ) as SlInput;
			if( !linkInput.value ) continue;
			if( !parseYouTubeLink( linkInput.value ) ) return;
			newVideos.push( linkInput.value );
		}

		this.loading = true;
		try {
			this.hack = await HacksApi.updateHackVideosAsync( this.hack.hackId, newVideos );
			this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to update videos', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	#done() : void {
		this.dispatchEvent( new CustomEvent( 'rhdc-editing-done' ) );
	}

}
