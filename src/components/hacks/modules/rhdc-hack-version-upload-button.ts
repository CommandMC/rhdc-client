import { css, CSSResultGroup, html, nothing } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../../../rhdc-element';
import { HacksApi, Hack, HackFileInfo, HackVersionUpdateDto, HackSaveType, HackFlag, ConsoleCompatibility, GfxPlugin, GfxPluginFlag, HackControllerSupport } from '../../../apis/hacks-api';
import { RhdcHackUpdatedEvent } from '../../../events/rhdc-hack-updated-event';
import { getErrorMessage } from '../../../util/http-error-parser';
import { RhdcFileDropEvent } from '../../../events/rhdc-file-drop-event';
import { Nullable } from '../../../util/types';
import { RhdcPluginEditor } from '../rhdc-plugin-editor';
import { AlertFactory } from '../../../util/alert-factory';
import { formatBytes } from '../../../util/formatting';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import SlRequestCloseEvent from '@shoelace-style/shoelace/dist/events/sl-request-close';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/divider/divider';
import '@shoelace-style/shoelace/dist/components/alert/alert';
import '../../../widgets/rhdc-file-dropzone';
import '../rhdc-plugin-editor';

const makeRow = function( name: string, value: string ) : unknown {
	return html`
		<tr>
			<td>${name}</td>
			<td>${value}</td>
		</tr>
	`;
};

const formatSupport = function( controllerSupport: HackControllerSupport ) {
	switch( controllerSupport ) {
		case HackControllerSupport.Required:
			return 'Yes (Required)';
		case HackControllerSupport.Supported:
			return 'Yes';
		default:
			return 'No';
	}
};

const formatSaveType = function( saveType: Nullable<HackSaveType> ) {
	if( !saveType ) return 'Unknown';
	switch( saveType ) {
		case HackSaveType.None: return 'None (or MemPak only)';
		case HackSaveType.Eeprom4k: return 'EEPROM 4kb';
		case HackSaveType.Eeprom16k: return 'EEPROM 16 kb';
		case HackSaveType.Sram: return 'SRAM (256 kb)';
		case HackSaveType.FlashRam: return 'Flash RAM';
		default: return 'Unknown';
	}
}

@customElement( 'rhdc-hack-version-upload-button' )
export class RhdcHackVersionUploadButton extends RhdcElement {

	@property({ attribute: false })
	hack!: Hack;

	@state()
	loading = false;

	@state()
	patchInfo: Nullable<HackFileInfo> = null;

	@state()
	editingInfo = false;

	@query( 'sl-dialog' )
	dialog!: SlDialog;

	@query( 'rhdc-plugin-editor' )
	pluginEditor!: RhdcPluginEditor;

	#defaultSettings: HackVersionUpdateDto = {};

	static override get styles() : CSSResultGroup {
		return css`
			.upload {
				display: flex;
				gap: var(--sl-spacing-2x-small);
			}

			rhdc-file-dropzone {
				box-sizing: border-box;
				min-height: 300px;
				max-height: 600px;
			}

			rhdc-file-dropzone > p {
				max-width: 75%;
			}

			.upload > div {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-small);
			}

			table {
				border-spacing: var(--sl-spacing-small) 0;
			}

			td {
				color: var(--sl-color-neutral-600);
				font-weight: normal;
				text-align: left;
			}

			td:last-child {
				width: 99%;
			}

			sl-dialog[data-wide] {
				--width: min( 900px, 95vw );
			}

			sl-dialog[data-x-wide] {
				--width: min( 1200px, 95vw );
			}

			div[hidden] {
				display: none !important;
			}
		`;
	}

	override render() : unknown {
		if( !this.hack || !this.hack.canEditVersions ) return nothing;

		let patchDetails: unknown = nothing;
		if( this.patchInfo ) {
			const successAlert = AlertFactory.create({ type: 'success',	message: 'Patch File Accepted' });
			const warningAlert = AlertFactory.create({
				type: 'warning',
				title: 'Warning',
				message: 'Your ROM\'s internal name is not encoded properly. It should be in Shift-JIS format.',
				hidden: this.patchInfo.internalRomNameGood
			});

			patchDetails = html`
				<sl-divider vertical></sl-divider>
				<div>
					${successAlert}
					${warningAlert}
					<table>
						<tbody>
							${makeRow( 'Patch File', this.patchInfo.patchFilename )}
							${makeRow( 'Internal Name', this.patchInfo.internalRomName )}
							${makeRow( 'Sha1 Checksum', this.patchInfo.hackSha1 )}
							${makeRow( 'Hack File Size', formatBytes( this.patchInfo.hackFileSize ) )}
							${makeRow( 'Mouse Support', formatSupport( this.patchInfo.mouseSupport ) )}
							${makeRow( 'GCC Support', formatSupport( this.patchInfo.gamecubeControllerSupport ) )}
							${makeRow( 'Save Type', formatSaveType( this.patchInfo.saveType ) )}
						</tbody>
					</table>
				</div>
			`;
		}

		return html`
			<sl-button part="button" variant="primary" @click=${this.#open}>
				<sl-icon slot="prefix" name="plus"></sl-icon>
				Add New Version
			</sl-button>
			<sl-dialog
				?data-wide=${this.editingInfo}
				?data-x-wide=${!!this.patchInfo && !this.editingInfo}
				@sl-request-close=${this.#onRequestDialogClose}
			>
				<div class="upload" ?hidden=${this.editingInfo}>
					<rhdc-file-dropzone
						accept='.bps,.zip,application/x-bps-patch,application/zip'
						?disabled=${this.loading}
						@rhdc-file-drop-event=${this.#patchUploaded}
					>
						<p>Upload a file by dragging it here or clicking here</p>
					</rhdc-file-dropzone>
					${patchDetails}
				</div>

				<div ?hidden=${!this.editingInfo}>
					<rhdc-plugin-editor
						.defaultValue=${this.#defaultSettings}
						.saveType=${this.patchInfo?.saveType || null}
						?loading=${this.loading}
					></rhdc-plugin-editor>
				</div>

				<sl-button
					slot="footer"
					variant="primary"
					?disabled=${!this.patchInfo || (this.loading && !this.editingInfo)}
					?loading=${this.loading && this.editingInfo}
					@click=${this.#next}
				>${this.editingInfo ? 'Save' : 'Continue'}</sl-button>

				<sl-button
					slot="footer"
					variant="neutral"
					?disabled=${this.loading}
					@click=${this.#back}
				>${this.editingInfo ? 'Back' : 'Cancel'}</sl-button>
			</sl-dialog>
		`;
	}

	#open() : void {
		this.patchInfo = null;
		this.editingInfo = false;
		this.dialog.show();
	}

	async #patchUploaded( event: RhdcFileDropEvent ) : Promise<void> {
		this.patchInfo = null;
		this.loading = true;
		try {
			this.patchInfo = await HacksApi.uploadHackVersionAsync( this.hack.hackId, event.detail.file );
			this.#defaultSettings = {
				plugin: this.patchInfo.pluginGuess || undefined,
				pluginFlags: (this.patchInfo.pluginGuess === GfxPlugin.ParaLLEl) ? [ GfxPluginFlag.UpscaleTexrects ] : [],
				hackFlags: (this.patchInfo.saveType === HackSaveType.Eeprom16k ? [ HackFlag.BigEEPROM ] : [] ),
				consoleCompatibility: (this.patchInfo.pluginGuess && this.patchInfo.pluginGuess !== GfxPlugin.ParaLLEl) ? ConsoleCompatibility.None : null
			};
		} catch( exception: unknown ) {
			this.toastError( 'Patch Rejected', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #next() : Promise<void> {
		if( !this.patchInfo ) return;
		
		if( this.editingInfo && this.pluginEditor.value ) {
			this.loading = true;
			try {
				this.hack = await HacksApi.commitHackVersionAsync( this.hack.hackId, this.pluginEditor.value );
				this.dispatchEvent( RhdcHackUpdatedEvent.create( this.hack ) );
				this.dialog.hide();
			} catch( exception: unknown ) {
				this.toastError( 'Server Error', getErrorMessage( exception ) );
			} finally {
				this.loading = false;
			}
		} else {
			this.editingInfo = true;
		}
	}

	#back() : void {
		if( this.editingInfo ) {
			this.editingInfo = false;
		} else {
			this.dialog.hide();
		}
	}

	#onRequestDialogClose( event: SlRequestCloseEvent ) : void {
		if( event.detail.source === 'overlay' ) event.preventDefault();
	}

}
