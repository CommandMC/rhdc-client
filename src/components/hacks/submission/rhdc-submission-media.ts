import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { asyncReplace } from 'lit/directives/async-replace.js';
import { guard } from 'lit/directives/guard.js';
import { RhdcSubmissionComponent } from './rhdc-submission-component';
import { HackSubmissionApi, HackSubmissionSession } from '../../../apis/hack-submission-api';
import { RhdcHackSubmissionUpdatedEvent } from '../../../events/rhdc-hack-submission-updated-event';
import { getErrorMessage } from '../../../util/http-error-parser';
import { FileLike, Optional, SparseArray } from '../../../util/types';
import { AsyncPipe } from '../../../util/async-pipe';
import { parseYouTubeLink } from '../../../util/youtube-link';
import { RhdcFileDropEvent } from '../../../events/rhdc-file-drop-event';
import { RhdcDragEvent } from '../../../widgets/rhdc-drag-item';
import { DragUtil } from '../../../util/drag-util';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '../../../widgets/rhdc-drag-item';
import '../../../widgets/rhdc-file-dropzone';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-submission-media' )
export class RhdcSubmissionMedia extends RhdcSubmissionComponent {

	@property({ attribute: false })
	submission!: HackSubmissionSession;

	@state()
	loading = false;

	@state()
	uploading = false;

	@state()
	screenshots: SparseArray<FileLike> = [];

	#imageCache: Map<string,FileLike>;
	#promiseQueue: Promise<void>;
	#errored = false;
	#version: number;

	constructor() {
		super();
		this.#imageCache = new Map<string,FileLike>();
		this.#promiseQueue = Promise.resolve();
		this.#version = 0;
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host([hidden]) {
				display: none !important;
			}

			:host {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-large);
			}

			.section {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-x-small);
			}

			.button-tray {
				display: flex;
				align-self: stretch;
			}

			.spacer {
				flex-grow: 1;
			}

			.screenshots {
				display: flex;
				flex-wrap: wrap;
			}

			img {
				height: 240px;
				width: auto;
				object-position: center;
				object-fit: cover;
			}

			rhdc-drag-item > div {
				position: relative;
			}

			rhdc-drag-item:first-child img {
				width: 320px;
			}

			rhdc-drag-item sl-button {
				position: absolute;
				top: var(--sl-spacing-x-small);
				right: var(--sl-spacing-x-small);
				display: none;
			}

			rhdc-drag-item:hover sl-button, rhdc-drag-item:focus sl-button {
				display: block;
				cursor: default;
			}

			.img-loading {
				width: 240px;
				height: 240px;
				display: flex;
				justify-content: center;
				align-items: center;
				font-size: 120px;
				box-sizing: border-box;
				border: 1px solid var(--sl-color-neutral-500);
			}

			img.error {
				box-sizing: border-box;
				border: 1px solid var(--sl-color-danger-500);
			}

			sl-input {
				max-width: 900px;
			}

			sl-input[data-invalid]:not(:focus)::part(base) {
				border-color: var(--sl-color-danger-500);
			}

			rhdc-file-dropzone {
				width: 240px;
				height: 240px;
				margin: var(--sl-spacing-x-small);
			}

			rhdc-file-dropzone > sl-icon {
				font-size: 100px;
			}

			h3, p {
				margin: 0;
			}

		`;
	}

	override render() : unknown {
		return html`
			<div class="section">
				<h3>YouTube Videos (Optional)</h3>
				<p>You may add up to 3 YouTube videos showcasing your hack.</p>
				${this.#renderYouTubeLinkInput( 0 )}
				${this.#renderYouTubeLinkInput( 1 )}
				${this.#renderYouTubeLinkInput( 2 )}
			</div>
			<div class="section">
				<h3>Screenshots</h3>
				<p>
					You may add up to 10 screenshots showcasing your hack.
					You must upload at least one screenshot.
					You can drag and drop your uploaded screenshots to rearrange them.
					The first screenshot will be used as the thumbnail for your hack.
				</p>
				<div class="screenshots">${this.#renderScreenshots()}</div>
			</div>
			<div class="button-tray">
				<sl-button variant="primary" ?disabled=${this.uploading} ?loading=${this.loading} @click=${this.#goPrev}>
					<sl-icon slot="prefix" name="chevron-left"></sl-icon>
					<span>Back</span>
				</sl-button>
				<div class="spacer"></div>
				<sl-button variant="primary" ?disabled=${this.uploading || this.screenshots.length < 1} ?loading=${this.loading} @click=${this.#goNext}>
					<sl-icon slot="suffix" name="chevron-right"></sl-icon>
					<span>Next</span>
				</sl-button>
			</div>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'submission' ) ) {
			this.#version++;

			if( !this.submission.media ) {
				this.#imageCache.clear();
				return;
			}

			this.screenshots = [];
			for( let i = 0; i < this.submission.media.screenshots.length; i++ ) {
				const name = this.submission.media.screenshots[i];
				if( this.#imageCache.has( name ) ) {
					this.screenshots[i] = this.#imageCache.get( name );
				} else {
					this.#fetchImageAsync( i, name, this.#version );
				}
			}

			this.screenshots = [ ...this.screenshots ];
		}
	}

	override disconnectedCallback(): void {
		this.#imageCache.clear();
	}

	async #fetchImageAsync( index: number, name: string, version: number ) : Promise<void> {
		const blob = this.#nameBlob( name, await HackSubmissionApi.fetchScreenshotAsync( index ) );
		if( this.#version !== version ) return;
		if( this.submission.media?.screenshots[index] !== name ) return;
		this.#imageCache.set( name, blob );
		this.screenshots[index] = blob;
		this.screenshots = [ ...this.screenshots ];
	}

	#renderYouTubeLinkInput( index: number ) : HTMLTemplateResult {
		return html`
			<sl-input type="url"
				id=${`yt${index + 1}`}
				.value=${this.submission.media?.videos[index] || ''}
				clearable
				placeholder="YouTube video link (Optional)"
				?disabled=${this.loading}
				autocapitalize="off"
				autocorrect="off"
				autocomplete="url"
				enterkeyhint="next"
				inputmode="url"
				@sl-change=${this.#onYouTubeLinkChanged}
				@blur=${this.#onYouTubeLinkChanged}
			></sl-input>
		`;
	}

	#renderScreenshots() : unknown[] {
		const screenshots: unknown[] = this.screenshots.map(
			(screenshot, i) => guard( [screenshot, this.loading || this.uploading], this.#renderScreenshot.bind( this, screenshot, i ) )
		);

		if( this.screenshots.length < 10 && !this.uploading ) {
			screenshots.push( html`
				<rhdc-file-dropzone
					accept=".png,.bmp,.gif,.jpg,.jpeg,image/png,image/bmp,image/gif,image/jpeg"
					@rhdc-file-drop-event=${this.#onUploadScreenshot}
				>
					<sl-icon name="plus-lg"></sl-icon>
				</rhdc-file-dropzone>
			` );
		}

		return screenshots;
	}

	#renderScreenshot( image: Optional<Blob>, index: number ) : HTMLTemplateResult {
		const pipe = new AsyncPipe<HTMLTemplateResult>();
		pipe.push( html`<div class="img-loading"></div>` );

		if( image ) {
			const fileReader = new FileReader();
			fileReader.readAsDataURL( image );

			fileReader.addEventListener( 'loadend', () => {
				if( fileReader.result ) {
					pipe.push( html`<div><img src=${fileReader.result}>${this.#renderDeleteButton( index )}</div>` );
				} else {
					pipe.push( html`<div><img src="about:blank" class="error">${this.#renderDeleteButton( index )}</div>` );
				}
				pipe.end();
			});
		} else {
			pipe.end();
		}

		return html`
			<rhdc-drag-item
				horizontal
				?disabled=${this.loading || this.uploading}
				.data=${index}
				.groupId=${'submission-screenshots'}
				@rhdc-drag-event=${this.#onScreenshotDragged}
			>${asyncReplace( pipe )}</rhdc-drag-item>
		`;
	}

	#renderDeleteButton( index: number ) {
		return html`
			<sl-button
				variant="danger"
				?loading=${this.uploading}
				@click=${this.#removeScreenshotAsync.bind( this, index )}
			>
				<sl-icon name="trash"></sl-icon>
			</sl-button>
		`;
	}

	async #onUploadScreenshot( event: RhdcFileDropEvent ) : Promise<void> {
		const image = event.detail.file;
		if( ![ 'image/png', 'image/bmp', 'image/gif', 'image/jpeg' ].includes( image.type ) ) {
			this.toastError( 'Screenshot Rejected', 'Screenshots must be in one of the following formats: PNG, BMP, GIF, JPEG' );
			return;
		}

		this.uploading = true;
		await this.#queueAction( async () => {
			await HackSubmissionApi.uploadScreenshotAsync( image );
			this.#imageCache.set( image.name, image );
			this.screenshots.push( image );
			this.screenshots = [ ...this.screenshots ];
		}, 'Screenshot Rejected' );
		this.uploading = false;
	}

	async #removeScreenshotAsync( index: number ) : Promise<void> {
		this.uploading = true;
		await this.#queueAction( async () => {
			const screenshotFilename = this.screenshots[index]?.name || '';
			await HackSubmissionApi.removeScreenshotAsync( index );
			this.#imageCache.delete( screenshotFilename );
		});
		this.uploading = false;
	}

	async #onScreenshotDragged( event: RhdcDragEvent ) : Promise<void> {
		const source = event.detail.source.data as number;
		const before = (event.detail.target.data as number) + (event.detail.before ? 0 : 1);

		if( source === before || source === before - 1 ) return;

		await this.#queueAction( async() => {
			this.screenshots = DragUtil.withMovedElement( this.screenshots, source, before );
			await HackSubmissionApi.moveScreenshotAsync( source, before );
		}, 'Move Failed' );
	}

	async #refreshSession() : Promise<void> {
		this.submission = await HackSubmissionApi.getSessionAsync();
		this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
	}

	#nameBlob( name: string, blob: Blob ) {
		(blob as FileLike).name = name;
		return blob as FileLike;
	}

	#onYouTubeLinkChanged( event: Event ) : void {
		this.#validateYouTubeLink( (event.target as SlInput) );
	}

	#validateYouTubeLink( input: SlInput ) : boolean {
		if( !input.value ) {
			input.setCustomValidity( '' );
			this.#updateYouTubeLinksAsync();
			return true;
		}

		const link = parseYouTubeLink( input.value );
		if( link ) {
			input.value = link.embedLink();
			input.setCustomValidity( '' );
			this.#updateYouTubeLinksAsync();
			return true;
		} else {
			input.setCustomValidity( 'The provided URL is not a valid YouTube link.' );
			return false;
		}
	}

	async #updateYouTubeLinksAsync() : Promise<boolean> {
		const newLinks: string[] = [];
		
		const input1 = this.shadowRoot!.getElementById( 'yt1' ) as SlInput;
		const input2 = this.shadowRoot!.getElementById( 'yt2' ) as SlInput;
		const input3 = this.shadowRoot!.getElementById( 'yt3' ) as SlInput;

		if( input1.value ) newLinks.push( input1.value );
		if( input2.value ) newLinks.push( input2.value );
		if( input3.value ) newLinks.push( input3.value );

		if( !this.#haveYouTubeLinksChanged( newLinks ) ) {
			return true;
		}

		await this.#queueAction( () => {
			return HackSubmissionApi.updateVideosAsync( newLinks );
		});

		return !this.#haveYouTubeLinksChanged( newLinks );
	}

	#haveYouTubeLinksChanged( newLinks: string[] ) : boolean {
		if( !this.submission.media ) {
			return newLinks.length > 0;
		}

		if( this.submission.media.videos.length !== newLinks.length ) {
			return true;
		}

		for( let i = 0; i < newLinks.length; i++ ) {
			if( this.submission.media.videos[i] !== newLinks[i] ) {
				return true;
			}
		}

		return false;
	}

	#queueAction(
		action: () => Promise<void>,
		errorSummary = 'Server Error'
	) : Promise<void> {
		this.#promiseQueue = this.#nextAction( action, errorSummary );
		return this.#promiseQueue.catch( () => {} );
	}

	async #nextAction(
		action: () => Promise<void>,
		errorSummary: string
	) : Promise<void> {
		await this.#promiseQueue;

		const previousScreenshots = this.screenshots;
		try {
			await action();
		} catch( exception: unknown ) {
			this.toastError( errorSummary, getErrorMessage( exception ) );
			this.#promiseQueue = Promise.resolve();
			this.screenshots = previousScreenshots;
			this.#errored = true;
		} finally {
			await this.#refreshSession();
		}
	}

	public override async saveAsync() : Promise<void> {
		this.loading = true;
		await this.#promiseQueue;
		this.loading = false;
	}

	async #goPrev() : Promise<void> {
		await this.saveAsync();
		this.dispatchEvent( new CustomEvent( 'rhdc-submission-prev-event' ) );
	}

	async #goNext() : Promise<void> {
		this.#errored = false;
		await this.saveAsync();

		if( this.#errored ) return;
		if( !this.submission.media || this.submission.media.screenshots.length < 1 ) return;
		if( !await this.#updateYouTubeLinksAsync() ) return;

		this.dispatchEvent( new CustomEvent( 'rhdc-submission-next-event' ) );
	}

}
