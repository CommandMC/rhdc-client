import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { RhdcSubmissionComponent } from './rhdc-submission-component';
import { HackSubmissionApi, HackSubmissionSession } from '../../../apis/hack-submission-api';
import { rhdcUserContext, UserAuth } from '../../../decorators/user-context';
import { Nullable } from '../../../util/types';
import { ImmutableDate } from '../../../util/time';
import { Category } from '../../../apis/hacks-api';
import { RhdcHackSubmissionUpdatedEvent } from '../../../events/rhdc-hack-submission-updated-event';
import { RhdcTagInput } from '../../../widgets/rhdc-tag-input';
import { RhdcDateInput } from '../../../widgets/rhdc-date';
import { getErrorMessage } from '../../../util/http-error-parser';
import SlSwitch from '@shoelace-style/shoelace/dist/components/switch/switch';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import SlTextarea from '@shoelace-style/shoelace/dist/components/textarea/textarea';
import SlSelect from '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/switch/switch';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/textarea/textarea';
import '@shoelace-style/shoelace/dist/components/select/select';
import '@shoelace-style/shoelace/dist/components/option/option';
import '@shoelace-style/shoelace/dist/components/alert/alert';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/button/button';
import '../../../widgets/rhdc-tag-input';
import '../../../widgets/rhdc-date';

@customElement( 'rhdc-submission-metadata' )
export class RhdcSubmissionMetadata extends RhdcSubmissionComponent {

	@property({ attribute: false })
	submission!: HackSubmissionSession;

	@state()
	error = '';

	@state()
	loading = false;

	@state()
	isPublic = true;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	@state()
	allUsernames: Nullable<string[]> = null;

	@state()
	allTags: Nullable<string[]> = null;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-large);
			}

			:host([hidden]) {
				display: none !important;
			}

			.body {
				display: flex;
				flex-wrap: wrap;
				gap: var(--sl-spacing-large) var(--sl-spacing-3x-large);
			}

			.column {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-large);
				min-width: min( 100vw, 350px );
				flex-grow: 1;
			}

			sl-input[type="number"] {
				align-self: flex-start;
				max-width: 15ch;
			}

			label {
				display: block;
				font-size: var(--sl-input-label-font-size-medium);
				color: var(--sl-input-label-color);
				padding-bottom: var(--sl-spacing-2x-small);
			}

			sl-switch[checked]::part(label) {
				color: var(--sl-color-neutral-800);
			}

			.button-tray {
				display: flex;
			}

			.spacer {
				flex-grow: 1;
			}
		`;
	}

	override render() : unknown {
		if( !this.currentUser ) return html``;
		return html`
			<sl-alert id="error" variant="danger" ?open=${!!this.error}>
				<sl-icon slot="icon" name="exclamation-octagon"></sl-icon>
				<strong>Problem with Submission</strong><br/>
				<span>${this.error}</span>
			</sl-alert>
			<div class="body">
				<div class="column">
					<div>
						<label for="public">Visibility</label>
						<sl-switch
							id="public"
							?checked=${this.isPublic}
							@sl-change=${this.#onVisibilityChanged}
						>Public</sl-switch>
					</div>
					<sl-alert variant="primary" ?open=${!!this.submission.competition && !this.isPublic}>
						<sl-icon slot="icon" name="info-circle"></sl-icon>
						<span>Your hack will be made public once the competition is published.</span>
					</sl-alert>
					<sl-input type="text"
						id="title"
						.value=${this.submission.metadata?.title || ''}
						label="Title"
						placeholder="Enter your hack's name here."
						?disabled=${this.loading}
						required
						autocapitalize="on"
						enterkeyhint="next"
						inputmode="text"
					></sl-input>
					<sl-textarea
						id="description"
						.value=${this.submission.metadata?.description || ''}
						label="Description"
						placeholder="Enter a description of your hack here."
						rows="4"
						resize="auto"
						?disabled=${this.loading}
						required
						enterkeyhint="next"
						spellcheck
						inputmode="text"
					></sl-textarea>
					<sl-select
						id="category"
						label="Category"
						?disabled=${this.loading}
						placeholder="Select a category."
						.value="${this.submission.metadata?.category || ''}"
						required
					>
						<sl-option value="${Category.Original}">${Category.Original}</sl-option>
						<sl-option value="${Category.Kaizo}">${Category.Kaizo}</sl-option>
						<sl-option value="${Category.Retexture}">${Category.Retexture}</sl-option>
						<sl-option value="${Category.Concept}">${Category.Concept}</sl-option>
					</sl-select>
				</div>
				<div class="column">
					<rhdc-tag-input
						id="authors"
						.value=${this.submission.metadata?.authors || [ this.currentUser.username ]}
						?loading=${!this.allUsernames}
						case-sensitive
						highlight-existing
						placeholder="Enter username here. Tab to autocomplete."
						?disabled=${this.loading}
						label="Author(s)"
						required
						.autocomplete=${this.allUsernames || []}
					></rhdc-tag-input>
					<rhdc-tag-input
						id="tags"
						.value=${this.submission.metadata?.tags || []}
						?loading=${!this.allTags}
						allow-spaces
						placeholder="Enter text here and hit Enter to add a tag."
						?disabled=${this.loading}
						label="Tags"
						.autocomplete=${this.allTags || []}
					></rhdc-tag-input>
					<rhdc-date
						id="creation-date"
						label="Creation Date"
						.value=${this.submission.metadata?.creationDate ? new ImmutableDate( this.submission.metadata.creationDate, true ) : ImmutableDate.today( true )}
						utc
						?disabled=${this.loading}
					></rhdc-date>
					<sl-input type="number"
						id="stars"
						.value=${this.submission.metadata ? this.submission.metadata.stars.toString() : ''}
						label="Stars"
						placeholder="Star Count"
						?disabled=${this.loading}
						min="0"
						max="9999"
						step="1"
						required
						enterkeyhint="next"
						inputmode="numeric"
					></sl-input>
				</div>
			</div>
			<div class="button-tray">
				<sl-button variant="primary" ?disabled=${this.loading} @click=${this.#goPrev}>
					<sl-icon slot="prefix" name="chevron-left"></sl-icon>
					<span>Back</span>
				</sl-button>
				<div class="spacer"></div>
				<sl-button variant="primary" ?loading=${this.loading} @click=${this.#goNext}>
					<sl-icon slot="suffix" name="chevron-right"></sl-icon>
					<span>Next</span>
				</sl-button>
			</div>
		`;
	}

	override connectedCallback(): void {
		super.connectedCallback();
		this.#initUsernames();
		this.#initTags();
	}

	protected override willUpdate( changedProperties: PropertyValues ): void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'submission' ) ) {
			this.isPublic = this.submission.metadata ? !this.submission.metadata.private : !this.submission?.competition;
		}
	}

	async #initUsernames() : Promise<void> {
		try {
			this.allUsernames = await HackSubmissionApi.getAllUsernamesAsync();
		} catch( exception: unknown ) {
			this.allUsernames = [];
			throw exception;
		}
	}

	async #initTags() : Promise<void> {
		try {
			this.allTags = await HackSubmissionApi.getPublicTagsAsync();
		} catch( exception: unknown ) {
			this.allTags = [];
			throw exception;
		}
	}

	#onVisibilityChanged( event: Event ) {
		this.isPublic = (event.target as SlSwitch).checked;
	}

	protected override async saveAsync(): Promise<void> {
		const fPublic = this.#getField<SlSwitch>( 'public' );
		const fTitle = this.#getField<SlInput>( 'title' );
		const fDescription = this.#getField<SlTextarea>( 'description' );
		const fCategory = this.#getField<SlSelect>( 'category' );
		const fAuthors = this.#getField<RhdcTagInput>( 'authors' );
		const fTags = this.#getField<RhdcTagInput>( 'tags' );
		const fCreationDate = this.#getField<RhdcDateInput>( 'creation-date' );
		const fStars = this.#getField<SlInput>( 'stars' );

		if(
			!fTitle.reportValidity() ||
			!fDescription.reportValidity() ||
			!fCategory.reportValidity() ||
			!fAuthors.reportValidity() ||
			!fStars.reportValidity()
		) return;

		this.loading = true;
		try {
			const metadata = await HackSubmissionApi.updateMetadataAsync(
				fPublic.checked,
				fTitle.value,
				fDescription.value,
				fCategory.value as Category,
				fAuthors.value,
				fTags.value,
				fCreationDate.value.toDate(),
				fStars.valueAsNumber
			);

			this.submission = this.submission.withMetadata( metadata );
			this.dispatchEvent( RhdcHackSubmissionUpdatedEvent.create( this.submission ) );
		} finally {
			this.loading = false;
		}
	}

	async #goPrev() : Promise<void> {
		await this.trySaveAsync();
		this.dispatchEvent( new CustomEvent( 'rhdc-submission-prev-event' ) );
	}

	async #goNext() : Promise<void> {
		this.error = '';
		try {
			await this.saveAsync();
			if( !this.error ) {
				this.dispatchEvent( new CustomEvent( 'rhdc-submission-next-event' ) );
			}
		} catch( exception: unknown ) {
			this.error = getErrorMessage( exception );
			await this.updateComplete;
			this.shadowRoot?.getElementById( 'error' )?.scrollIntoView({ behavior: 'smooth' });
		}
	}

	#getField<T extends HTMLElement>( id: string ) : T {
		return this.shadowRoot!.getElementById( id ) as T;
	}

}
