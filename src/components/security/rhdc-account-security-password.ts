import { css, CSSResultGroup, html } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { Uuid } from '../../util/types';
import { Auth } from '../../auth/auth';
import { AccountSecurityApi } from '../../apis/account-security-api';
import { getErrorMessage } from '../../util/http-error-parser';
import generatePassword from '../../util/generate-password';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';

@customElement( 'rhdc-account-security-password' )
export class RhdcAccountSecurityPassword extends RhdcElement {

	@property({ attribute: false })
	accountSecurityToken!: Uuid;

	@state()
	loading = false;

	@query( '#new-password' )
	passwordInput!: SlInput;

	@query( '#verify-password' )
	verificationInput!: SlInput;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: flex-start;
				gap: var(--sl-spacing-x-small);
			}

			sl-input {
				width: 300px;
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-button variant="primary" ?disabled=${this.loading} @click=${this.#generatePassword}>Generate Random Password</sl-button>

			<sl-input type="password"
				id="new-password"
				label="New Password"
				?disabled=${this.loading}
				password-toggle
				required
				minlength="8"
				autocapitalize="off"
				autocorrect="off"
				autocomplete="new-password"
				enterkeyhint="next"
				?spellcheck=${false}
				inputmode="text"
			></sl-input>

			<sl-input type="password"
				id="verify-password"
				label="Confirm Password"
				?disabled=${this.loading}
				password-toggle
				required
				minlength="8"
				autocapitalize="off"
				autocorrect="off"
				autocomplete="new-password"
				enterkeyhint="send"
				?spellcheck=${false}
				inputmode="text"
				@keydown=${this.#onKeyDown}
			></sl-input>

			<br/>

			<sl-button variant="primary" ?loading=${this.loading} @click=${this.#submit}>Change Password</sl-button>
		`;
	}

	#generatePassword() : void {
		this.verificationInput.value = '';
		const input = this.passwordInput;
		input.value = generatePassword();
		input.passwordVisible = true;
		input.focus();
		input.select();
	}

	#onKeyDown( event: KeyboardEvent ) : void {
		if( event.key !== 'Enter' ) return;
		this.#submit();
	}

	async #submit() : Promise<void> {
		if( !this.passwordInput.reportValidity() || !this.verificationInput.reportValidity() ) {
			return;
		}

		if( this.passwordInput.value !== this.verificationInput.value ) {
			this.passwordInput.setCustomValidity( 'Passwords do not match' );
			this.verificationInput.setCustomValidity( 'Passwords do not match' );
			return;
		}

		this.loading = true;
		try {
			await AccountSecurityApi.changePasswordAsync( this.accountSecurityToken, this.passwordInput.value );
			this.toastSuccess( 'Password changed', 'You have been logged out of all other browser sessions.' );
			await Auth.initAsync();
		} catch( exception: unknown ) {
			this.toastError( 'Failed to change password', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

}
