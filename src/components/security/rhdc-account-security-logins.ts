import { css, CSSResultGroup, html, nothing, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../rhdc-element';
import { ConstArray, Nullable, Uuid } from '../../util/types';
import { UserAuth } from '../../decorators/user-context';
import { AccountSecurityApi, ActiveLogin } from '../../apis/account-security-api';
import { getErrorMessage } from '../../util/http-error-parser';
import { AlertFactory } from '../../util/alert-factory';
import { countryMap } from '../../util/countries';
import { rhdcIconButtonStyles } from '../../common-styles';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/spinner/spinner';
import '@shoelace-style/shoelace/dist/components/relative-time/relative-time';
import '@shoelace-style/shoelace/dist/components/tooltip/tooltip';
import '@shoelace-style/shoelace/dist/components/badge/badge';

@customElement( 'rhdc-account-security-logins' )
export class RhdcAccountSecurityLogins extends RhdcElement {

	@property({ attribute: false })
	accountSecurityToken!: Uuid;

	@property({ attribute: false })
	userContext!: UserAuth;

	@state()
	sessions: Nullable<ConstArray<ActiveLogin>> = null;

	@state()
	loading = false;

	static override get styles() : CSSResultGroup {
		return css`
			${rhdcIconButtonStyles}

			.logins {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				width: min-content;
			}

			.logins > div {
				display: flex;
				flex-direction: row;
				align-items: center;
				justify-content: start;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-x-small);
				border-bottom: var(--sl-panel-border-width) solid var(--sl-panel-border-color);
			}

			.logins > div:last-child {
				border-bottom: none;
			}

			.login {
				line-height: 1.25rem;
				white-space: nowrap;
			}

			img {
				display: inline;
				vertical-align: middle;
				width: 1em;
				height: 1em;
			}

			.logout {
				font-size: 2.5rem;
			}

			.badges {
				display: flex;
				flex-direction: row;
				gap: 1ch;
			}

			sl-spinner {
				font-size: 3rem;
			}

			sl-badge {
				margin: var(--sl-spacing-2x-small) 0;
			}

			sl-badge::part(base) {
				border-color: transparent;
			}

			.grey {
				color: var(--sl-color-neutral-600);
			}

			.flag {
				font-family: EmojiFallback, var(--sl-font-sans);
			}

			sl-tooltip {
				white-space: normal;
				--max-width: 30rem;
			}
		`;
	}

	override render() : unknown {
		if( !this.sessions ) return html`<sl-spinner></sl-spinner>`;
		return html`
			${AlertFactory.create({ type: 'primary', message: 'Location data is guessed from the IP address and may not be accurate, especially when using mobile or satellite internet connections.' })}
			<div class="logins">
				${map( this.sessions, this.#renderActiveLogin.bind( this ) )}
			</div>
		`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'userContext' ) ) {
			this.#getActiveLogins();
		}
	}

	async #getActiveLogins() : Promise<void> {
		this.sessions = null;
		try {
			this.sessions = await AccountSecurityApi.getActiveLoginsAsync( this.accountSecurityToken );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to fetch active logins', getErrorMessage( exception ) );
		}
	}

	#renderActiveLogin( login: Readonly<ActiveLogin> ) : unknown {
		let location: HTMLTemplateResult;
		if( login.country && countryMap.has( login.country ) ) {
			const country = countryMap.get( login.country )!;
			const locationName = login.city ? `${login.city}, ${country.name}` : country.name;
			location = html`<span class="flag">${country.flag}</span> <span>${locationName}</span>`;
		} else {
			location = html`<span class="grey">unknown location</span>`;
		}

		let browser: HTMLTemplateResult;
		if( login.browser ) {
			browser = html`<img src="/assets/icons/browser/${login.browser.toLowerCase().replace( ' ', '-' )}.svg" /><span> ${login.browser} ${login.browserVersion}</span>`;
		} else {
			browser = html`<span class="grey">unknown browser</span>`;
		}

		if( login.steamDeck ) {
			browser = html`<sl-icon name="steam"></sl-icon> ${browser}`;
		} else if( login.mobile ) {
			browser = html`<sl-icon name="phone"></sl-icon> ${browser}`;
		}

		let os: unknown = nothing;
		if( login.steamDeck ) {
			os = html`<span> (Steam Deck)</span>`;
		} else if( login.operatingSystem ) {
			let osName: string;
			switch( login.operatingSystem ) {
				case 'Linux': osName = login.mobile ? 'Android' : 'Linux'; break;
				case 'MacOS': osName = login.mobile ? 'iPhone' : 'Mac OS'; break;
				case 'Windows': osName = login.mobile ? 'Windows Phone' : 'Windows'; break;
			}

			os = html`<span> (${osName})</span>`;
		}

		let logoutButton: HTMLTemplateResult;
		if( login.canLogout ) {
			logoutButton = html`<sl-icon-button class="danger logout" name="shield-x" label="Logout" title="Logout" ?disabled=${this.loading} @click=${this.#logout.bind( this, login.sessionId )}></sl-icon-button>`;
		} else {
			logoutButton = html`<sl-icon class="logout"></sl-icon>`;
		}

		let badge: unknown = nothing;
		if( login.application !== 'Browser' || !login.canLogout ) {
			const badges: HTMLTemplateResult[] = [];
			if( !login.canLogout ) {
				badges.push( html`<sl-badge pill variant="primary">Current Session</sl-badge>` );
			}
			if( login.application !== 'Browser' ) {
				badges.push( html`<sl-tooltip content="This session is only permitted to perform a limited set of actions required by ${login.application}"><sl-badge pill variant="success">Limited Scope</sl-badge></sl-tooltip>` );
			}
			badge = html`<div class="badges">${badges}</div>`;
		}

		return html`
			<div>
				<div class="login">
					${badge}
					<div>
						<b>Initial Login: </b><sl-relative-time .date=${login.loginDate}></sl-relative-time>
					</div>
					<div>
						<b>Last Refresh: </b><sl-relative-time .date=${login.refreshData}></sl-relative-time>
					</div>
					<div>
						<span>from </span>${location}
					</div>
					<div>
						<span>using ${browser}${os}</span>
					</div>
				</div>
				${logoutButton}
			</div>
		`;
	}

	async #logout( sessionId: Uuid ) : Promise<void> {
		if( !await this.confirmAsync( 'Confirm Logout', 'Are you sure you want to logout this device?', 'Yes', 'Cancel' ) ) return;

		this.loading = true;
		try {
			await AccountSecurityApi.terminateSessionAsync( this.accountSecurityToken, sessionId );
			this.sessions = this.sessions?.filter( session => session.sessionId !== sessionId ) || null;
		} catch( exception: unknown ) {
			this.toastError( 'Failed to terminate session', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

}
