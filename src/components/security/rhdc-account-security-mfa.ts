import { css, CSSResultGroup, html } from 'lit';
import { customElement, property, state, query } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { Nullable, Uuid } from '../../util/types';
import { UserAuth } from '../../decorators/user-context';
import { AccountSecurityApi } from '../../apis/account-security-api';
import { getErrorMessage } from '../../util/http-error-parser';
import { RhdcMfaInput } from '../../widgets/rhdc-mfa-input';
import SlInput from '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/qr-code/qr-code';
import '@shoelace-style/shoelace/dist/components/icon-button/icon-button';
import '@shoelace-style/shoelace/dist/components/input/input';
import '@shoelace-style/shoelace/dist/components/button/button';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '../../widgets/rhdc-mfa-input';

@customElement( 'rhdc-account-security-mfa' )
export class RhdcAccountSecurityMfa extends RhdcElement {

	@property({ attribute: false })
	accountSecurityToken!: Uuid;

	@property({ attribute: false })
	userContext!: UserAuth;

	@state()
	key: Nullable<string> = null;

	@state()
	loading = false;

	@query( 'rhdc-mfa-input' )
	codeInput!: RhdcMfaInput;

	@query( 'sl-input' )
	mfaSecret!: SlInput;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: flex-start;
				gap: var(--sl-spacing-small);
			}

			sl-qr-code {
				color: var(--sl-color-neutral-1000);
			}

			sl-qr-code::part(base) {
				width: 300px;
				height: 300px;
			}

			.button-tray {
				align-self: stretch;
				display: flex;
				flex-direction: row;
				justify-content: space-between;
			}

			sl-input {
				max-width: 30rem;
			}
		`;
	}

	override render() : unknown {
		if( this.userContext.mfaEnabled ) {
			return html`
				<span><sl-icon name="lock"></sl-icon> Multi-Factor Authentication is <b>enabled</b>.</span>
				<sl-button variant="danger" @click=${this.#disableMfa} ?loading=${this.loading}>Disable MFA</sl-button>
			`;
		}

		if( !this.key ) {
			return html`
				<span><sl-icon name="unlock"></sl-icon> Multi-Factor Authentication is <b>disabled</b>.</span>
				<sl-button variant="primary" @click=${this.#requestMfa} ?loading=${this.loading}>Enable MFA</sl-button>
			`;
		}

		const url = `otpauth://totp/RHDC:${this.userContext.username}?secret=${this.key}&issuer=RHDC&algorithm=SHA1&digits=6&period=30`;
		return html`
			<div>
				<div>
					Use a phone app like Google Authenticator or FreeOTP to get MFA codes.
					Scan the QR code below in your authenticator app, then enter the 6 digit code it
					generates to activate multi-factor authentication on your account.
				</div>
				<sl-qr-code value="${url}" size="150" fill="currentColor" background="transparent" error-correction="Q"></sl-qr-code>
			</div>

			<div>
				<div>
					If you are unable to scan the QR code, you can enter this text code below instead.
					Store this text code in a secure location separate from your password. If you ever
					lose access to your device containing your authenticator app, you can use this code
					to re-enter it into a new device.
				</div>
				<sl-input type="text"
					value="${this.key}"
					filled
					label="Recovery Code"
					readonly
				>
					<sl-icon-button slot="suffix" name="clipboard2" label="Copy to clipboard" @click=${this.#copyKey}></sl-icon-button>
				</sl-input>
			</div>

			<rhdc-mfa-input ?disabled=${this.loading} @rhdc-submit=${this.#enableMfa}></rhdc-mfa-input>

			<div class="button-tray">
				<sl-button variant="primary" @click=${this.#enableMfa} ?loading=${this.loading}>Activate MFA</sl-button>
				<sl-button variant="neutral" @click=${() => this.key = null} ?disabled=${this.loading}>Cancel</sl-button>
			</div>
		`;
	}

	async #disableMfa() : Promise<void> {
		if( !await this.confirmAsync( 'Disable MFA?', 'Are you sure you want to disable multi-factor authentication on this account?', 'Disable MFA', 'Cancel' ) ) return;

		this.loading = true;
		try {
			this.userContext = await AccountSecurityApi.disableMfaAsync( this.accountSecurityToken );
		} catch( exception: unknown ) {
			this.toastError( 'Unexpected error disabling MFA', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #requestMfa() : Promise<void> {
		this.loading = true;
		try {
			this.key = await AccountSecurityApi.generateMfaQrCodeAsync( this.accountSecurityToken );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to generate MFA QR code', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	async #enableMfa() : Promise<void> {
		if( !this.codeInput.reportValidity() ) return;

		this.loading = true;
		try {
			this.userContext = await AccountSecurityApi.enableMfaAsync( this.accountSecurityToken, this.codeInput.value );
		} catch( exception: unknown ) {
			this.toastError( 'Incorrect MFA code', getErrorMessage( exception ) );
		} finally {
			this.loading = false;
		}
	}

	#copyKey() : void {
		window.navigator.clipboard.writeText( this.mfaSecret.value );
	}

}
