import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { User, UsersApi } from '../../apis/users-api';
import { ConstRef } from '../../util/types';
import { rhdcPageState, IRhdcPageState } from '../../decorators/page-state';
import '../../widgets/rhdc-load-more';
import '../../decorators/page-state';
import './rhdc-user-search-card';

@customElement( 'rhdc-user-search' )
export class RhdcHackSearch extends RhdcElement {

	@property({ attribute: false })
	searchTerm!: string;

	@rhdcPageState( (user: ConstRef<User>) => html`<rhdc-user-search-card .user=${user}></rhdc-user-search-card>` )
	pageState!: IRhdcPageState;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				gap: var(--sl-spacing-2x-small);
				overflow-y: auto;
				max-height: 300px;
			}

			div {
				display: flex;
				flex-direction: row;
				flex-wrap: wrap;
				align-items: stretch;
				gap: var(--sl-spacing-small);
			}

			p {
				font-size: var(--sl-font-size-large);
				color: var(--sl-color-neutral-700);
			}
		`;
	}

	override render() : unknown {
		return html`<div>${this.pageState.render({ renderError: 'Failed to get user search results' })}</div>`;
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'searchTerm' ) && this.searchTerm ) {
			this.pageState.reset( UsersApi.searchUsersAsync( this.searchTerm ) );
		}
	}

}
