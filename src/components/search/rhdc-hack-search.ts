import { css, CSSResultGroup, html, PropertyValues } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { Hack, HackSortField, MatureFilter, HacksApi } from '../../apis/hacks-api';
import { rhdcPageState, IRhdcPageState } from '../../decorators/page-state';
import '../../decorators/page-state';
import '../hacks/rhdc-hack-card';

@customElement( 'rhdc-hack-search' )
export class RhdcHackSearch extends RhdcElement {

	@property({ attribute: false })
	searchTerm!: string;

	@rhdcPageState( (hack: Hack) => html`<rhdc-hack-card .hack=${hack}></rhdc-hack-card>` )
	pageState!: IRhdcPageState;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			.items {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-2x-small);
			}

			p {
				font-size: var(--sl-font-size-large);
				color: var(--sl-color-neutral-700);
			}
		`;
	}

	override render() : unknown {
		/* eslint-disable indent */
		return html`
			<div class="items">
				${this.pageState.render({
					renderEmpty: () => html`<p>No results found.</p>`,
					renderError: 'Failed to get hack search results.'
				})}
			</div>
		`;
		/* eslint-enable indent */
	}

	protected override willUpdate( changedProperties: PropertyValues ) : void {
		super.willUpdate( changedProperties );
		if( changedProperties.has( 'searchTerm' ) ) {
			this.pageState.reset( HacksApi.searchHacksAsync( this.searchTerm, HackSortField.Popularity, true, MatureFilter.Include ) );
		}
	}

}
