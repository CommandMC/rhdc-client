import { css, CSSResultGroup, html } from 'lit';
import { customElement, query, state } from 'lit/decorators.js';
import { RhdcElement } from '../rhdc-element';
import { RhdcPromptEventDetails } from '../events/rhdc-prompt-event';
import SlDialog from '@shoelace-style/shoelace/dist/components/dialog/dialog';
import SlRequestCloseEvent from '@shoelace-style/shoelace/dist/events/sl-request-close';
import '@shoelace-style/shoelace/dist/components/dialog/dialog';
import '@shoelace-style/shoelace/dist/components/button/button';

@customElement( 'rhdc-confirm-dialog' )
export class RhdcConfirmDialog extends RhdcElement {

	@state()
	options: RhdcPromptEventDetails;

	@state()
	resolved = true;

	@query( 'sl-dialog' )
	dialog!: SlDialog;

	constructor() {
		super();
		this.options = {
			title: '',
			message: '',
			affirmText: 'Yes',
			rejectText: 'No',
			resolver: () => {}
		};
	}

	static override get styles() : CSSResultGroup {
		return css`
			.button-tray {
				display: flex;
				justify-content: flex-end;
				gap: var(--sl-spacing-x-small);
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-dialog label=${this.options.title} @sl-hide=${this.#onHide} @sl-request-close=${this.#requestClose}>
				<p>${this.options.message}</p>
				<div slot="footer" class="button-tray">
					<sl-button variant="primary" @click=${this.#affirm}>${this.options.affirmText}</sl-button>
					<sl-button variant="neutral" @click=${this.#reject}>${this.options.rejectText}</sl-button>
				</div>
			</sl-dialog>
		`;
	}

	public show( options: RhdcPromptEventDetails ) : void {
		this.options = options;
		this.resolved = false;
		this.dialog.show();
	}

	#affirm() : void {
		this.options.resolver( true );
		this.resolved = true;
		this.dialog.hide();
	}

	#reject() : void {
		this.options.resolver( false );
		this.resolved = true;
		this.dialog.hide();
	}

	#onHide() : void {
		if( this.resolved ) return;
		this.options.resolver( false );
	}

	#requestClose( event: SlRequestCloseEvent ) : void {
		if( event.detail.source === 'overlay' ) {
			event.preventDefault();
		}
	}

}
