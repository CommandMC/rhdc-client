import { css, CSSResultGroup, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { styleMap } from 'lit/directives/style-map.js';
import '@shoelace-style/shoelace/dist/components/icon/icon';

@customElement( 'rhdc-poll-result' )
export class RhdcPollResult extends RhdcElement {

	@property({ attribute: false })
	name = '';

	@property({ attribute: false })
	votes = 0;

	@property({ attribute: false })
	total = 0;

	@property({ attribute: false })
	winner = false;

	@property({ attribute: 'checked', type: Boolean })
	checked = false;

	static override get styles() : CSSResultGroup {
		return css`
			.option {
				position: relative;
				width: 100%;
				height: var(--sl-input-height-medium);
				font-size: var(--sl-button-font-size-medium);
				line-height: calc( var(--sl-input-height-medium) - var(--sl-input-border-width) * 2 );
				border: var(--sl-input-border-width) solid var(--sl-color-danger-500);
				border-radius: var(--sl-input-border-radius-medium);
				padding: 0;
				padding-left: var(--sl-spacing-x-small);
				vertical-align: middle;
				white-space: nowrap;
			}

			.background {
				position: absolute;
				top: 0;
				left: 0;
				height: 100%;
				z-index: 10;
				background-color: var(--sl-color-danger-100);
			}

			.foreground {
				position: absolute;
				z-index: 20;
				display: flex;
				align-items: center;
				gap: var(--sl-spacing-2x-small);
				color: var(--sl-color-danger-800);
			}

			sl-icon {
				font-size: 1.5rem;
			}

			.winner.option {
				border-color: var(--sl-color-success-500);
				font-weight: bold;
			}

			.winner > .background {
				background-color: var(--sl-color-success-100);
			}

			.winner > .foreground {
				color: var(--sl-color-success-800);
			}
		`;
	}

	override render() : unknown {
		const percent = `${(this.total > 0) ? Math.round( 100 * this.votes / this.total ) : 0}%`;
		const barStyle = { width: percent };

		let icon: unknown = null;
		if( this.checked ) {
			icon = html`<sl-icon name="check" label="Your vote"></sl-icon>`;
		}

		const klass = this.winner ? 'option winner' : 'option';
		return html`
			<div class="${klass}">
				<div class="background" style=${styleMap( barStyle )}></div>
				<div class="foreground">
					<span>${percent}</span>
					<span>${this.name}</span>
					${icon}
				</div>
			</div>
		`;
	}

}
