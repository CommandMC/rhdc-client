import { css, CSSResultGroup, html, HTMLTemplateResult, PropertyValues } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import { NewsApi, NewsPost } from '../../apis/news-api';
import { Nullable, Weak } from '../../util/types';
import { rhdcUserContext, UserAuth } from '../../decorators/user-context';
import { Role } from '../../auth/roles';
import { rhdcPageState, IRhdcPageState } from '../../decorators/page-state';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/skeleton/skeleton';
import '../../decorators/page-state';
import '../../widgets/rhdc-load-more';
import './rhdc-news-post';
import './rhdc-news-post-skeleton';
import './rhdc-news-post-creator';

@customElement( 'rhdc-news' )
export class RhdcNews extends RhdcElement {

	@rhdcPageState( RhdcNews.prototype._renderPost )
	pageState!: IRhdcPageState;

	@state()
	creatingPost = false;

	@rhdcUserContext()
	currentUser!: Nullable<UserAuth>;

	override connectedCallback(): void {
		super.connectedCallback();
		this.#reloadNews();
	}

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				gap: var(--sl-spacing-medium);
			}

			h2 {
				margin: 0;
				font-size: 1.75rem;
				color: var(--sl-color-primary-600);
			}

			.header {
				display: flex;
				align-items: center;
			}

			.spacer {
				flex-grow: 1;
			}
		`;
	}

	override render() : unknown {
		if( this.creatingPost ) {
			return html`
				<rhdc-news-post-creator
					@rhdc-reload-news-event=${this.#postCreated}
					@rhdc-cancel-news-post-event=${this.#createCancelled}
				></rhdc-news-post-creator>
			`;
		}

		if( this.pageState.count === 0 && !this.pageState.done ) {
			return html`
				<h2>News</h2>
				<rhdc-news-post-skeleton></rhdc-news-post-skeleton>
				<rhdc-news-post-skeleton></rhdc-news-post-skeleton>
				<rhdc-news-post-skeleton></rhdc-news-post-skeleton>
			`;
		}

		let createButton: Nullable<HTMLTemplateResult> = null;
		if( this.currentUser?.role === Role.Staff ) {
			createButton = html`<sl-button variant="primary" @click=${this.#toggleCreateMode}>Create News Post</sl-button>`;
		}

		return html`
			<div class="header">
				<h2>News</h2>
				<div class="spacer"></div>
				${createButton}
			</div>
			${this.pageState.render({ renderEmpty: null, renderError: 'Failed to load news posts.' })}
		`;
	}

	override willUpdate( changedProperties: PropertyValues ): void {
		super.willUpdate( changedProperties );
		const prevContext : Weak<UserAuth> = changedProperties.get( 'currentUser' );
		if( prevContext !== undefined && prevContext?.role !== this.currentUser?.role ) {
			this.#reloadNews();
		}
	}

	private _renderPost( post: NewsPost ) : HTMLTemplateResult {
		return html`<rhdc-news-post .post=${post} @rhdc-reload-news-event=${this.#reloadNews}></rhdc-news-post>`;
	}

	#reloadNews() : void {
		this.pageState.reset( NewsApi.getNewsPostsPagedAsync() );
	}

	#toggleCreateMode() : void {
		this.creatingPost = !this.creatingPost;
	}

	#postCreated() : void {
		this.creatingPost = false;
		this.#reloadNews();
	}

	#createCancelled() : void {
		this.creatingPost = false;
	}

}
