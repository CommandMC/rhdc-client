import { css, CSSResultGroup, html } from 'lit';
import { customElement } from 'lit/decorators.js';
import { RhdcElement } from '../../rhdc-element';
import '@shoelace-style/shoelace/dist/components/card/card';
import '@shoelace-style/shoelace/dist/components/skeleton/skeleton';

@customElement( 'rhdc-news-post-skeleton' )
export class RhdcNewsPostSkeleton extends RhdcElement {

	static override get styles() : CSSResultGroup {
		return css`
			sl-card {
				width: 100%;
			}

			sl-card::part(base) {
				background: none;
			}

			.body > sl-skeleton {
				width: 100%;
			}

			.body > sl-skeleton:last-child {
				width: 60%;
			}

			.avatar {
				float: left;
				width: 64px;
				height: 64px;
				--border-radius: 32px;
				padding: 0;
				margin-right: 1ch;
			}

			.title {
				display: inline-block;
				height: 2.25em;
				width: 50%;
				--border-radius: 1.125em;
			}

			.subtitle {
				display: inline-block;
				width: 35%;
			}

			sl-skeleton {
				padding-bottom: 0.5em;
			}
		`;
	}

	override render() : unknown {
		return html`
			<sl-card>
				<div slot="header">
					<sl-skeleton class="avatar"></sl-skeleton>
					<sl-skeleton class="title"></sl-skeleton><br/>
					<sl-skeleton class="subtitle"></sl-skeleton>
				</div>
				<div class="body">
					<sl-skeleton></sl-skeleton>
					<sl-skeleton></sl-skeleton>
					<sl-skeleton></sl-skeleton>
				</div>
			</sl-card>
		`;
	}

}
