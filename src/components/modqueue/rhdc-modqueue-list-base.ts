import { html, css, HTMLTemplateResult, CSSResultGroup } from 'lit';
import { Page } from '../../apis/pagination';
import { RhdcElement } from '../../rhdc-element';
import { rhdcPageState, IRhdcPageState } from '../../decorators/page-state';

export abstract class RhdcModqueueListBase<T> extends RhdcElement {

	@rhdcPageState(
		RhdcModqueueListBase.prototype._renderer,
		RhdcModqueueListBase.prototype._getter
	)
	private pageState!: IRhdcPageState;

	static override get styles() : CSSResultGroup {
		return css`
			:host {
				display: contents;
			}

			.items {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				width: 100%;
				box-sizing: border-box;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-small);
			}
		`;
	}

	override render() : unknown {
		return html`
			<div class="items">
				${this.pageState.render({ renderEmpty: this.#renderEmpty.bind( this ), renderError: this.loadErrorMessage })}
			</div>
		`;
	}

	#renderEmpty() : HTMLTemplateResult {
		return html`
			<sl-alert slot="no-results" variant="success" open>
				<sl-icon slot="icon" name="check2-circle"></sl-icon>
				<strong>All Caught Up!</strong><br/>
				<span>${this.emptyQueueMessage}</span>
			</sl-alert>
		`;
	}

	public loadItems() : void {
		this.pageState.reset();
	}

	protected removeItem( id: string ) : void {
		this.shadowRoot?.getElementById( id )?.remove();
		if( this.shadowRoot?.querySelector( '.items' )?.childElementCount === 0 ) {
			this.pageState.reset();
		}
	}

	protected abstract loadFirstPageAsync() : Promise<Page<T>>;
	protected abstract renderItem( item: T ) : HTMLTemplateResult;

	protected abstract get emptyQueueMessage() : string;
	protected abstract get loadErrorMessage() : string;

	private _renderer( item: T ) : HTMLTemplateResult {
		return this.renderItem( item );
	}

	private _getter() : Promise<Page<T>> {
		return this.loadFirstPageAsync();
	}

}
