import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { map } from 'lit/directives/map.js';
import { RhdcElement } from '../../rhdc-element';
import { DownloadInfo } from '../../apis/files';
import { ConstArray, ConstRef, Optional } from '../../util/types';
import { RichUserRef } from '../../apis/users-api';
import '@shoelace-style/shoelace/dist/components/format-date/format-date';
import '../common/rhdc-username';
import '../../widgets/rhdc-avatar';
import '../../widgets/rhdc-image';
import '../../widgets/rhdc-link';

@customElement( 'rhdc-modqueue-item' )
export class RhdcModqueueItem extends RhdcElement {

	@property({ attribute: false })
	thumbnail: Optional<DownloadInfo>;

	@property({ attribute: false })
	hackName!: string;

	@property({ attribute: false })
	user!: Readonly<RichUserRef> | ConstArray<RichUserRef>;

	@property({ attribute: false })
	datePrefix = 'Submitted';

	@property({ attribute: false })
	date: Optional<ConstRef<Date>>;

	@property({ attribute: false })
	href!: string;

	static override get styles() : CSSResultGroup {
		return css`
			rhdc-link {
				width: 100%;
				color: var(--rhdc-text-color) !important;
			}

			rhdc-link > div {
				display: flex;
				width: 100%;
				border: 1px solid var(--sl-color-neutral-200);
				background-color: var(--sl-panel-background-color);
				border-radius: var(--sl-border-radius-medium);
				color: var(--rhdc-text-color);
			}

			rhdc-link:hover > div, rhdc-link:focus-visible > div {
				border-color: var(--sl-input-border-color-hover);
				background-color: var(--sl-input-background-color-hover);
			}

			rhdc-image {
				width: calc( 4.8rem + 2 * var(--sl-spacing-x-small) );
				height: calc( 4.8rem + 2 * var(--sl-spacing-x-small) );
			}

			rhdc-avatar {
				--size: calc( 4.8rem + 2 * var(--sl-spacing-x-small) );
			}

			.body {
				display: flex;
				flex-direction: column;
				align-items: stretch;
				padding: var(--sl-spacing-x-small);
				flex-grow: 1;
				width: 0;
			}

			.body > * {
				font-size: 1rem;
				line-height: 1.2rem;
				overflow-x: hidden;
				text-overflow: ellipsis;
				white-space: nowrap;
				flex-grow: 1;
			}

			.users {
				display: flex;
				gap: 0.5ch;
			}

			.users > div {
				color: var(--sl-color-neutral-600);
			}

			.users > div:last-of-type {
				display: none;
			}

			slot {
				display: contents;
			}

			slot.actions {
				display: none;
			}

			slot.actions[data-visible] {
				display: flex;
				align-items: center;
				padding: var(--sl-spacing-x-small);
				gap: var(--sl-spacing-x-small);
			}
			
			.date {
				color: var(--sl-color-neutral-700);
			}
		`;
	}

	override render() : unknown {
		let userRow: HTMLTemplateResult;
		if( Array.isArray( this.user ) ) {
			userRow = html`
				<div class="users">
					${map( this.user, user => html`
						<rhdc-username .user=${user}></rhdc-username>
						<div>&bull;</div>
					`)}
				</div>
			`;
		} else {
			userRow = html`<rhdc-username .user=${this.user}></rhdc-username>`;
		}

		let thumbnail: HTMLTemplateResult;
		if( !this.thumbnail && !Array.isArray( this.user ) ) {
			thumbnail = html`<rhdc-avatar square .username=${(this.user as RichUserRef).username}></rhdc-avatar>`;
		} else {
			thumbnail = html`<rhdc-image .downloadInfo=${this.thumbnail}></rhdc-image>`;
		}

		let dateRow: HTMLTemplateResult;
		if( this.date ) {
			const timestamp = html`
				<sl-format-date
					.date=${this.date as Date}
					year="numeric"
					month="long"
					day="numeric"
					hour="numeric"
					minute="2-digit"
					hour-format="12"
				></sl-format-date>
			`;
			dateRow = html`<span class="date">${this.datePrefix} ${timestamp}</span>`;
		} else {
			dateRow = html`<span>&nbsp;</span>`;
		}

		return html`
			<rhdc-link href=${this.href}>
				<div>
					${thumbnail}
					<div class="body">
						<b>${this.hackName}</b>
						${userRow}
						<slot></slot>
						${dateRow}
					</div>
					<slot name="action" class="actions" @slotchange=${this.#slotChanged} @click=${(event: Event) => event.stopPropagation()}></slot>
				</div>
			</rhdc-link>
		`;
	}

	#slotChanged( event: Event ) : void {
		const slot = event.target as HTMLSlotElement;
		if( slot.assignedElements().length > 0 ) {
			slot.dataset.visible = '';
		} else {
			delete slot.dataset.visible;
		}
	}

}
