import { html, HTMLTemplateResult } from 'lit';
import { customElement } from 'lit/decorators.js';
import { ModqueueApi, PendingHack } from '../../apis/modqueue-api';
import { Page } from '../../apis/pagination';
import { RhdcModqueueListBase } from './rhdc-modqueue-list-base';
import { RichUserRef } from '../../apis/users-api';
import { ConstArray } from '../../util/types';
import './rhdc-modqueue-item';

@customElement( 'rhdc-modqueue-bonus-list' )
export abstract class RhdcModqueueBonusList extends RhdcModqueueListBase<PendingHack> {

	protected override get emptyQueueMessage() : string {
		return 'There are no more hacks missing a completion bonus.'
	}

	protected override get loadErrorMessage() : string {
		return 'Failed to load hacks.';
	}

	protected override loadFirstPageAsync() : Promise<Page<PendingHack>> {
		return ModqueueApi.getPendingCompletionBonusesAsync();
	}

	protected override renderItem( item: PendingHack ) : HTMLTemplateResult {
		return html`
			<rhdc-modqueue-item
				.thumbnail=${item.thumbnail}
				.hackName=${item.title}
				.user=${item.authors.filter( a => a.userId ) as ConstArray<RichUserRef>}
				.datePrefix=${'Uploaded'}
				.date=${item.uploadedDate}
				.href="/hack/${item.slug}"
			><span>No completion bonus assigned</span></rhdc-modqueue-item>
		`;
	}

	override render() : unknown {
		return html`
			<p>The following hacks have been approved and are not retextures, but they have no completion bonus assigned.</p>
			${super.render()}
		`;
	}

}
