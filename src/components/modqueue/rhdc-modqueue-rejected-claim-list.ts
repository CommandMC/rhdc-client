import { css, CSSResultGroup, html, HTMLTemplateResult } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { ModqueueApi, PendingClaim } from '../../apis/modqueue-api';
import { HacksApi } from '../../apis/hacks-api';
import { Page } from '../../apis/pagination';
import { RhdcModqueueListBase } from './rhdc-modqueue-list-base';
import { RhdcModqueueStatusChangeEvent } from '../../events/rhdc-modqueue-status-change-event';
import { getErrorMessage } from '../../util/http-error-parser';
import '@shoelace-style/shoelace/dist/components/icon/icon';
import '@shoelace-style/shoelace/dist/components/button/button';
import './rhdc-modqueue-item';

@customElement( 'rhdc-modqueue-rejected-claim-list' )
export abstract class RhdcModqueueRejectedClaimList extends RhdcModqueueListBase<PendingClaim> {

	@state()
	deleting = false;

	static override get styles() : CSSResultGroup {
		return css`
			${super.styles}

			.author {
				color: var(--rhdc-font-color);
				overflow-x: hidden;
				text-overflow: ellipsis;
				flex-grow: 1;
			}

			.explanation {
				color: var(--rhdc-font-color);
				white-space: pre-wrap;
			}
		`;
	}

	protected override get emptyQueueMessage() : string {
		return 'There are no more rejected hacks ownership claims.'
	}

	protected override get loadErrorMessage() : string {
		return 'Failed to load rejected hack ownership claims.';
	}

	protected override loadFirstPageAsync() : Promise<Page<PendingClaim>> {
		return ModqueueApi.getRejectedClaimsAsync();
	}

	protected override renderItem( item: PendingClaim ) : HTMLTemplateResult {
		let reason: unknown = null;
		if( item.explanation ) {
			reason = html`
				<br/>
				<b>Explanation: </b>
				<span class="explanation">${item.explanation}</span>
			`;
		}

		return html`
			<rhdc-modqueue-item
				id="${item.hackId}:${item.claimant.userId}"
				.thumbnail=${item.hackThumbnail}
				.hackName=${item.hackTitle}
				.user=${item.claimant}
				.datePrefix=${'Submitted'}
				.date=${item.dateSubmitted}
				.href="/hack/${item.hackSlug}"
			>
				<div>
					<b>Alias: </b>
					<span class="author">${item.targetAuthor || 'n/a'}</span>
					${reason}
				</div>
				<sl-button
					slot="action"
					variant="danger"
					size="large"
					?loading=${this.deleting}
					@click=${this.#delete.bind( this, item )}
				><sl-icon name="trash"></sl-icon></sl-button>
			</rhdc-modqueue-item>
		`;
	}

	override render() : unknown {
		return html`
			<p>The following hack ownership claims have been permanently rejected. Deleting items from this list will allow the user to submit hack ownership claims again.</p>
			${super.render()}
		`;
	}

	async #delete( claim: PendingClaim ) : Promise<void> {
		this.deleting = true;
		try {
			await HacksApi.deleteHackOwnershipClaimAsync( claim.hackId, claim.claimant.userId );
			this.#removeItemFromDOM( claim );
			this.dispatchEvent( RhdcModqueueStatusChangeEvent.createAsync() );
		} catch( exception: unknown ) {
			this.toastError( 'Failed to delete hack ownership claim', getErrorMessage( exception ) );
		} finally {
			this.deleting = false;
		}
	}

	#removeItemFromDOM( claim: PendingClaim ) : void {
		this.removeItem( `${claim.hackId}:${claim.claimant.userId}` );
	}

}
