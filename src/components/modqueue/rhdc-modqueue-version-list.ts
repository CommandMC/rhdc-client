import { html, HTMLTemplateResult } from 'lit';
import { customElement } from 'lit/decorators.js';
import { ModqueueApi, PendingHack } from '../../apis/modqueue-api';
import { Page } from '../../apis/pagination';
import { RhdcModqueueListBase } from './rhdc-modqueue-list-base';
import { RichUserRef } from '../../apis/users-api';
import { ConstArray } from '../../util/types';
import './rhdc-modqueue-item';

@customElement( 'rhdc-modqueue-version-list' )
export abstract class RhdcModqueueVersionList extends RhdcModqueueListBase<PendingHack> {

	protected override get emptyQueueMessage() : string {
		return 'There are no more hack versions pending approval.'
	}

	protected override get loadErrorMessage() : string {
		return 'Failed to load pending hack versions.';
	}

	protected override loadFirstPageAsync() : Promise<Page<PendingHack>> {
		return ModqueueApi.getPendingVersionsAsync();
	}

	protected override renderItem( item: PendingHack ) : HTMLTemplateResult {
		return html`
			<rhdc-modqueue-item
				.thumbnail=${item.thumbnail}
				.hackName=${item.title}
				.user=${item.authors.filter( a => a.userId ) as ConstArray<RichUserRef>}
				.datePrefix=${'Uploaded'}
				.date=${item.lastVersionUploadTime}
				.href="/hack/${item.slug}"
			><span>${item.pendingVersions} pending ${item.pendingVersions === 1 ? 'version' : 'versions'}</span></rhdc-modqueue-item>
		`;
	}

}
